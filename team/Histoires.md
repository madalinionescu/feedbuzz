# Histoires
Informations récapitulatives concernant les différentes histoires.

#### Quelques précisions
Un point correspond à une heure de travail par binôme (approximatif). Par itération il faut accomplir 70 points.
Risque évalué de 1 (facile) à 3 (difficile).
Les évaluation en point des tâches des Histoires sont indiquées entre parenthèse à côté de celle-ci.

Certaines histoires ont été modifiées par des remarques du client (abandon de certaine
fonctionnalités car trop gourmande en point) et sont donc marquée comme "Finie" si les tâches sont remplies en tenant compte
de ces remarques.

Les tâches bonus (pas dans l'histoire originale mais demandée en supplément par le client) sont également spécifiées en annexes des tâches

----------------------


## Pondération

| Priorité/3 | N° | Description | Risque/3 | Points | Etat |
| ------ | ------ | ------ | ------ | ------ | ------ | 
| 1 | [1](#Histoire-1) | Visualisation des articles | 2 | 0 | Finie |
|   | [3](#Histoire-3) | Récupération des articles à parti de sources extérieures | 1 | 5 | En cours |
|   | [4](#Histoire-4) | Récupération d'articles à partir de pages web | 1 | 0 | Finie |
| 2 | [2](#Histoire-2) | Recherche et filtres sur les articles stockés localement | 3 | 0 | Finie |
|   | [5](#Histoire-5) | Création d'un système de recommandations | 3 | 35 | En cours |
|   | [7](#Histoire-7) | Intégrité des données | 2 | 20 | Non assignée |
|   | [9](#Histoire-9) | Vérification de la fiabilité d'un article | 3 | 47 | Non assignée |
|   | [10](#Histoire-10) | Gestion des articles | 2 | 2 | En cours |
|   | [11](#Histoire-11) | Gestion de plusieurs utilisateurs | 1 | 0 | Finie |
|   | [12](#Histoire-12) | Support pour médias différents | 3 | 20 | En cours |
|   | [14](#Histoire-14) | Filtrage des articles sur base de la rélevance géographique | 2 | 17 | Non assignée |
| 3 | [6](#Histoire-6) | Sécurité des données | 3 | 20 | Non assignée |
|   | [8.1](#Histoire-8) | Intégration avec les réseaux sociaux (Twitter) | 2 | 0 | Finie |
|   | [8.2](#Histoire-8) | Intégration avec les réseaux sociaux (Facebook) | 2 | 10 | En cours |
|   | [13](#Histoire-13) | Continuous learning pour le système de recommendation | 3 | 30 | Non assigné |
|   | [15](#Histoire-15) | Section d'aide | 1 | 8 | En cours |

----------------------


## Description
<a name="Histoire-1"></a>
### Histoire 1 : Visualisation des articles

**Instructions originales:**   
- [x] Afficher une liste d'articles (titre, vignette, source, localisation, date de publication, mots-clés)
    (reste Système de mots-clé)
- [x] Afficher un aperçu de l'article
- [x] Possibilité d'ouvrir le lien de l'article
- [x] Possibilité de copier le lien de l'article
- [x] Possibilité de supprimer l'article
- [x] Possibilité d'afficher l'article complet

**Tâches en plus:**          
- [x] Annuler la suppresion d'un article

:question: **Question:**    
- /

**Remarque**
- pas de gestion des mots-clés

<a name="Histoire-2"></a>
### Histoire 2 : Recherche et filtres sur les articles stockés localement

**Instructions originales:**   
- [x] Effectuer des recherches sur les articles stockés localement (par mots-clés, date, source et langue)
- [x] Suggestion de recherche sur base des recherches récentes et de mots-clés possibles
- [x] Effectuer des recherches poussées (filtrer les articles sur bases de 1 ou plusieurs critères)
- [x] Sauvegarder les recherches pour créer des filtres réutilisables
- [x] Trier les résultats de recherche (par date ou relévance) de façon (dé)croissante

**Tâches en plus:**          
- /

:question: **Question:**      
- /

<a name="Histoire-3"></a>
### Histoire 3 : Récupération des articles à parti de sources extérieures

**Instructions originales:**   
- [x] Récupérer des articles de sources extérieures (présenter les dernières nouvelles dans le domaine qui l'intéresse)
- [x] Réaliser des recherches sur ces articles récupérés HORS-LIGNE
- [x] Télécharger les articles de manière automatique
- [x] Choix des sources d'intérêt de l'utilisateur
- [x] Choix du nombre d'articles sauvegarder
- [ ] Choix de la durée d'enregistrement des articles (5)
- [x] Suppression des articles par l'utilisateur

**Tâches en plus:**          
- [x] Possibilité de se désabonner d'une source extérieure

:question: **Question:**  
- /

<a name="Histoire-4"></a>
### Histoire 4 : Récupération d'articles à partir de pages web

**Instructions originales:**   
- [x] Importer des articles venant d'une page web donnée
- [x] Choix du nombre d'article à importer
- [x] Choix des informations à importer (base : titre, contenu, auteur et source)
- [x] Aperçu des articles importés avant importation

**Tâches en plus:**          
- [x] Animation de chargement pendant l'importation des articles d'une page web

:question: **Question:**  
- /

**Remarque**
- Seulement pour lesoir.be et hackaday.com

<a name="Histoire-5"></a>
### Histoire 5 : Création d'un système de recommandation
**Instructions originales:** 
- [x] Possibilité pour l'utilisateur de définir ses centres d'intérêts
- [x] Recommander des articles sur base des centres d'intérêt de l'utilisateur
- [ ] Recommander des articles sur base du flux d'actualité de l'utilisateur (20)
- [ ] Proposer des recommandations diversifiées et pertinentes (15)

**Tâches en plus:**  
- /

:question: **Question:** 
- /

<a name="Histoire-6"></a>
### Histoire 6 Sécurité des données
**Instructions originales:** 
 - [ ] Encrypter les fichiers de la database (10)
 - [ ] Vérifier que le fichier contenant les articles a été modifié ou non (10)

**Tâches en plus:**  
- /

:question: **Question:** 
- /

<a name="Histoire-7"></a>
### Histoire 7 Intégrité des données
**Instructions originales:** 
 - [ ] Vérifier si les articles sauver localement sont les mêmes que ceux en ligne (10)
 - [ ] Mettre à jour les articles s'ils sont différents (5)
 - [ ] Sauver le moment où la database à été dernièrement modifiée par l'application (1)
 - [ ] Vérifier si date de modification de la database correspond bien à la dernière écriture du fichier par l'OS (4)

**Tâches en plus:**  
- /

:question: **Question:** 
- /

<a name="Histoire-8"></a>
### Histoire 8 : Intégration avec des réseaux sociaux (Facebook et Twitter)
**Instructions originales:** 
- [x] Possibilité pour l'utilisateur de renseigner le nom d'un compte qu'il souhaite suivre
- [x] Possibilité pour l'utilisateur de renseigner le nom d'un hashtag qu'il souhaite suivre (à travers les différents réseaux sociaux)
- [x] Consulter les articles/posts des réseaux sociaux dans la liste d'article et les afficher par hashtag ou compte
- [x] Possibilité de partager des articles sur ses comptes sociaux

**Tâches en plus:**  
- /

:question: **Question:** 
- /

**Remarques sur l'histoire**
 - Seulement pour Twitter - Finie
 - Facebook  - pas fait (10)

<a name="Histoire-9"></a>
### Histoire 9 Vérification de la fiabilité d'un article
**Instructions originales:** 
 - [ ] Catégoriser les sources par fiabilité : les RSS par défaut sont fiables (10)
 - [ ] Créer une liste de sources RSS fiables (1)
 - [ ] Vérifier si les sources indiquées dans un article sont fiables (30)
 - [ ] Afficher la fiabilité d'un article (5)
 - [ ] Créer un liste de noire de sources peu/pas fiables (news parodiques,...) (1)

**Tâches en plus:**  
- /

:question: **Question:** 
- /

<a name="Histoire-10"></a>
### Histoire 10 : Gestion des articles
**Instructions originales:** 
- [x] Posssibilité de créer des dossiers, sous-dossiers
- [ ] Ajouter des articles dans un dossiers à l'importation (2)
- [x] Visualiser l'arborescence des dossiers et accéder à un dossier précis
- [x] Possibilité de créer des tags pouvant être associer aux articles
- [x] Catégorie prédéfinies "Lu", "Non lu" et "En cours de lecture"
- [x] Possibilité de modifier le dossier et/ou les tags d'un article à sa consultation

**Tâches en plus:**  
- /

:question: **Question:** 
- /

<a name="Histoire-11"></a>
### Histoire 11 : Gestion de plusieurs utilisateurs

**Instructions originales:**   
- [x] Possibilité de créer un nouveau compte au lancement du programme (avec acceptation des conditions d'utilisations, nom d'utilisateur et mot de passe)
- [x] Stocker les données relatives au comptes de manière sécurisée (chiffrement)
- [x] Possibilité de déconnexion

**Tâches en plus:**          
- [x] Restriction lors de la création de compte : mot de passe sécurisé (nombre minimum de caractères, diversités)
- [x] Limiter les tentatives de connexions consécutives (bloquer le compte pendant un certain temps)

:question: **Question:** 
- /

<a name="Histoire-12"></a>
### Histoire 12 : Support pour médias différents

**Instructions originales:**   
- [ ] Affichage d'images contenues dans l'article (dans la preview) (6)
- [ ] Sauver le lien des vidéos (2)
- [ ] Afficher les vidéos depuis leur lien (ex: API Youtube) dans la preview (4)
- [ ] Sauver la description d'un média (image/vidéo/audio) et l'afficher en dessous de celui-ci (5)
- [ ] Afficher les sources audio (preview) (3)

**Tâches en plus:**          
- /

:question: **Question:**   
- /

**Remarques sur l'histoire**
Pas de gestions des videos/sources audios localement (hors connexion)

<a name="Histoire-13"></a>
### Histoire 13 Continuous Learning pour le système de recommandation 
**Instructions originales:** 
 - [ ] Mettre en place un système de like/dislike des articles (4)
 - [ ] Déduire les préférences de ces like/dislikes (4)
 - [ ] Recommander des articles sur bases de leur tags et de l'intérêt de l'utilisateur (3)
 - [ ] Recommander des articles en se basant sur le temps passé sur certain type d'article (18)

**Tâches en plus:**  
- /

:question: **Question:** 
- /

<a name="Histoire-14"></a>
### Histoire 14 Filtrage des articles sur base de la rélevance géographique
**Instructions originales:** 
 - [ ] Créer une interface de carte (8)
 - [ ] Afficher les articles sur la carte (3)
 - [ ] Demander les préférences géographique de l'utilisateur (6)

**Tâches en plus:**  
- /

:question: **Question:** 
- zone géographique de publication de l'article ou p/r à où se passent les évènements relatés dans l'article ?

<a name="Histoire-15"></a>
### Histoire 15 : Section d'aide

**Instructions originales:**
- [ ] Tutoriel démontrant l'utilisation de certaines fonctionnalités (8)

**Tâches en plus:**
- [x] Fournir des informations détaillées sur les différentes fonctionnalités

:question: **Question:**       
- /
