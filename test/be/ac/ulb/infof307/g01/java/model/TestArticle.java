package be.ac.ulb.infof307.g01.java.model;

import org.junit.jupiter.api.Test;

import static be.ac.ulb.infof307.g01.java.ConstantsForTests.*;
import static org.junit.jupiter.api.Assertions.*;

class TestArticle {

    private final Article article1 = getArticle1();
    private final Article article2 = getArticle2();

    @Test
    void testIsDifferent(){
        assertNotEquals(article1, article2);
    }

    @Test
    void testGetTitle() {
        assertEquals(TITLE_ARTICLE_1, article1.getName());
    }

    @Test
    void testSetTitle() {
        article1.setName(TITLE_ARTICLE_2);
        assertEquals(TITLE_ARTICLE_2, article1.getName());
    }

    @Test
    void testGetContent() {
        assertEquals(CONTENT_ARTICLE_1, article1.getContent());
    }

    @Test
    void testSetContent() {
        article1.setContent(CONTENT_ARTICLE_2);
        assertEquals(CONTENT_ARTICLE_2, article1.getContent());
    }

    @Test
    void testGetAuthor() {
        assertEquals(AUTHOR_ARTICLE_1, article1.getAuthor());
    }

    @Test
    void testSetAuthor() {
        article1.setAuthor(AUTHOR_ARTICLE_2);
        assertEquals(AUTHOR_ARTICLE_2, article1.getAuthor());
    }

    @Test
    void testGetSource() {
        assertEquals(SOURCE_ARTICLE_1, article1.getSource());
    }

    @Test
    void testSetSource() {
        article1.setSource(SOURCE_ARTICLE_2);
        assertEquals(SOURCE_ARTICLE_2, article1.getSource());
    }

    @Test
    void testGetLink() {
        assertEquals(LINK_ARTICLE_1, article1.getLink());
    }

    @Test
    void testSetLink() {
        article1.setLink(LINK_ARTICLE_2);
        assertEquals(LINK_ARTICLE_2, article1.getLink());
    }

    @Test
    void testGetDelete() { assertFalse( article1.getTrash()); }

    @Test
    void testSetDelete() {
        article1.setTrash(true);
        assertTrue(article1.getTrash());
        article1.setTrash(false);
        assertFalse( article1.getTrash());
    }

    @Test
    void testGetTags(){
        assertEquals(TAGS_ARTICLE_1, article1.getTags());
    }

    @Test
    void testSetTags(){
        assertEquals(TAGS_ARTICLE_2, article2.getTags());
        article2.setTags(TAGS_ARTICLE_1);
        assertEquals(TAGS_ARTICLE_1, article2.getTags());
    }

    @Test
    void testAddTag(){
        article1.addTag(TAG_NAME_4);
        assertTrue(article1.getTags().contains(new Tag(TAG_NAME_4)));
    }

    @Test
    void testRemoveTag(){
        article1.removeTag(TAG_NAME_3);
        assertFalse(article1.getTags().contains(new Tag(TAG_NAME_3)));
    }

    @Test
    void testGetTweet() { assertFalse( article1.getTrash()); }

    @Test
    void testSetTweet() {
        article1.setTweet(true);
        assertTrue(article1.isTweet());
        article1.setTweet(false);
        assertFalse(article1.isTweet());
    }

    @Test
    void testSetTwitterHashtag() {
        article1.setTwitterHashtag(HASHTAG_TWITTER_1);
        assertEquals(HASHTAG_TWITTER_1, article1.getTwitterHashtag());
    }

    @Test
    void testGetTwitterHashtag(){
            assertEquals(HASHTAG_TWITTER_2, article1.getTwitterHashtag());
        }

}