package be.ac.ulb.infof307.g01.java.model.dao;

import be.ac.ulb.infof307.g01.java.model.Interest;
import be.ac.ulb.infof307.g01.java.model.LocalUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.ArrayList;
import java.util.List;

import static be.ac.ulb.infof307.g01.java.ConstantsForTests.*;
import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestInterestDAO {

    @BeforeEach
    void setUp(){
        DAOFactory.changeDatabasePath("./dbTest");
        DAOFactory.clearAll();

        LocalUser.singleton.create( USERNAME_USER_1,  PASSWORD_USER_1,  MAIL_USER_1);
        LocalUser.singleton.connect(USERNAME_USER_1,PASSWORD_USER_1);
    }

    @Test
    void testGetModelClass(){
        assertEquals(DAOFactory.getInterestDAO().getModelClass(), Interest.class);
    }


    @Test
    void testFindByString() {
        Interest interestValid = new Interest(INTEREST_NAME_2, RELATED_RSS_2, RELATED_TAGS_2);
        DAOFactory.getInterestDAO().create(interestValid);
        Interest find1 = DAOFactory.getInterestDAO().findByString(INTEREST_NAME_2);
        assertNotNull(find1);
        assertEquals(find1, interestValid);
        assertNull(DAOFactory.getInterestDAO().findByString(BAD_INTEREST));
        assertNull(DAOFactory.getInterestDAO().findByString(null));
    }

    @Test
    void testFindAll() {
        Interest interestValid1 = new Interest(INTEREST_NAME_1, RELATED_RSS_1, RELATED_TAGS_1);
        Interest interestValid2 = new Interest(INTEREST_NAME_2, RELATED_RSS_2, RELATED_TAGS_2);

        DAOFactory.getInterestDAO().create(interestValid1);
        DAOFactory.getInterestDAO().create(interestValid2);

        List<Interest> interests = new ArrayList<>();
        interests.add(interestValid1);
        interests.add(interestValid2);

        assertEquals(interests, DAOFactory.getInterestDAO().findAll());
    }

    @Test
    void testCreate() {
        Interest interestValid = new Interest(INTEREST_NAME_1, RELATED_RSS_1, RELATED_TAGS_1);
        assertFalse(DAOFactory.getInterestDAO().create(null));
        assertTrue(DAOFactory.getInterestDAO().create(interestValid));

        Interest find = DAOFactory.getInterestDAO().findByString(INTEREST_NAME_1);
        assertNotNull(find);
        assertEquals(find, interestValid);
    }



}