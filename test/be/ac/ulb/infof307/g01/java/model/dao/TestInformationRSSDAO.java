package be.ac.ulb.infof307.g01.java.model.dao;

import be.ac.ulb.infof307.g01.java.model.LocalUser;
import be.ac.ulb.infof307.g01.java.model.InformationRSS;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static be.ac.ulb.infof307.g01.java.ConstantsForTests.*;
import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestInformationRSSDAO {
    private static final String RSS_LINK_1 = "http://feeds.bbci.co.uk/news/rss.xml";

    @BeforeAll
    void setUp(){
        DAOFactory.changeDatabasePath(PATH_DB_TEST);
        DAOFactory.clearAll();

        LocalUser.singleton.create(USERNAME_USER_1, PASSWORD_USER_1,  MAIL_USER_1);
        LocalUser.singleton.create(USERNAME_USER_2, PASSWORD_USER_2,  MAIL_USER_2);
    }

    @Test
    void testCreate(){
        InformationRSS linkRssA = new InformationRSS(RSS_LINK_1);

        //On check que les données ne sont pas accessible d'un user à l'autre et qu'il existe
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        assertTrue(DAOFactory.getInformationRSSDAO().create(linkRssA));
        assertFalse(DAOFactory.getInformationRSSDAO().create(null));
        assertNotNull(DAOFactory.getInformationRSSDAO().findById(linkRssA.getId()));
        assertNull(DAOFactory.getInformationRSSDAO().findById(null));

        LocalUser.singleton.connect(USERNAME_USER_2, PASSWORD_USER_2);
        assertNull(DAOFactory.getInformationRSSDAO().findById(linkRssA.getId()));
    }


    @Test
    void testFindAll(){
        InformationRSS linkRssA = new InformationRSS(RSS_LINK_1);
        InformationRSS linkRssB = new InformationRSS(RSS_LINK_1);
        InformationRSS articleC = new InformationRSS(RSS_LINK_1);
        InformationRSS articleA2 = new InformationRSS(RSS_LINK_1);
        InformationRSS articleB2 = new InformationRSS(RSS_LINK_1);

        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        DAOFactory.getInformationRSSDAO().create(linkRssA);
        DAOFactory.getInformationRSSDAO().create(linkRssB);
        DAOFactory.getInformationRSSDAO().create(articleC);

        LocalUser.singleton.connect(USERNAME_USER_2, PASSWORD_USER_2);
        DAOFactory.getInformationRSSDAO().create(articleA2);
        DAOFactory.getInformationRSSDAO().create(articleB2);

        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        assertEquals(3, DAOFactory.getInformationRSSDAO().findAll().size());

        LocalUser.singleton.connect(USERNAME_USER_2, PASSWORD_USER_2);
        assertEquals(2, DAOFactory.getInformationRSSDAO().findAll().size());
    }


    @Test
    void testClear(){
        InformationRSS linkRssB = new InformationRSS(RSS_LINK_1);
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        DAOFactory.getInformationRSSDAO().create(linkRssB);
        DAOFactory.getInformationRSSDAO().clear();
        assertNull(DAOFactory.getInformationRSSDAO().findById(linkRssB.getId()));
        DAOFactory.getInformationRSSDAO().create(linkRssB);
        LocalUser.singleton.connect(USERNAME_USER_2, PASSWORD_USER_2);
        DAOFactory.getInformationRSSDAO().clear();
        assertNull(DAOFactory.getInformationRSSDAO().findById(linkRssB.getId()));
    }

    @Test
    void testGetModelClass(){
        assertEquals(DAOFactory.getInformationRSSDAO().getModelClass(), InformationRSS.class);
    }
}
