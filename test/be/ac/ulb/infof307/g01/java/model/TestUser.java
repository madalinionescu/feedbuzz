package be.ac.ulb.infof307.g01.java.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.*;

import static be.ac.ulb.infof307.g01.java.ConstantsForTests.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestUser {
    private Date dateTest;
    private Integer loginCountTest;

    private User userTest;
    private User newUser;
    private User sameUser;

    private static final ArrayList<String> INTERESTS = new ArrayList<>(Arrays.asList(INTEREST_NAME_1,INTEREST_NAME_2));
    private static final ArrayList<String> INTERESTS_2 = new ArrayList<>(Collections.singletonList(INTEREST_NAME_2));

    @BeforeEach
    void setUp() {
        this.dateTest = new Date();
        this.loginCountTest = 2;

        this.newUser = new User(USERNAME_USER_2, PASSWORD_USER_2, MAIL_USER_2, new ArrayList<>());
        this.userTest = new User(USERNAME_USER_1, PASSWORD_USER_1, MAIL_USER_1, new ArrayList<>());
        this.sameUser = new User(USERNAME_USER_1, PASSWORD_USER_1, MAIL_USER_1, new ArrayList<>());
        this.newUser.setUserTags(TAGS_ARTICLE_1);
   }

    @Test
    void testGetUsername() {
        assertEquals(USERNAME_USER_1, userTest.getUsername());
    }

    @Test
    void testGetPassword() {
        assertEquals(PASSWORD_USER_1, userTest.getHash());
    }

    @Test
    void testGetMail() {
        assertEquals(MAIL_USER_1, userTest.getMail());
    }

    @Test
    void testGetLoginCount() { assertEquals(0,this.userTest.getLoginCount() ); }

    @Test
    void testGetLockedUntil() { assertEquals(this.dateTest,this.userTest.getLockedUntil() ); }

    @Test
    void testSetUsername(){
        userTest.setUsername(USERNAME_USER_2);
        assertEquals(USERNAME_USER_2, userTest.getUsername());
    }

    @Test
    void testSetPassword(){
        userTest.setHash(PASSWORD_USER_2);
        assertEquals(PASSWORD_USER_2, userTest.getHash());
    }

    @Test
    void testSetMail(){
        userTest.setMail(MAIL_USER_2);
        assertEquals(MAIL_USER_2, userTest.getMail());
    }

    @Test
    void testEquals(){
        assertEquals(userTest, sameUser);
        assertNotEquals(userTest, newUser);
    }

    @Test
    void testSetUserTags(){
        assertEquals(userTest.getUserTags(), new ArrayList<>());
        userTest.setUserTags(TAGS_ARTICLE_1);
        assertEquals(userTest.getUserTags(), TAGS_ARTICLE_1);
    }

    @Test
    void testSetLockedUntil(){
        this.userTest.setLockedUntil(this.dateTest);
        assertEquals(this.dateTest, this.userTest.getLockedUntil());
    }

    @Test
    void testSetLoginCount(){
        this.userTest.setLoginCount(this.loginCountTest);
        assertEquals(this.loginCountTest, this.userTest.getLoginCount());
    }

    @Test
    void testGetUserTags(){
        assertEquals(newUser.getUserTags(), TAGS_ARTICLE_1);
    }

    @Test
    void testGetUserInterests() {
        this.newUser.setUserInterests(INTERESTS);
        assertEquals(newUser.getUserInterests(),INTERESTS);
    }

    @Test
    void testSetUserInterests() {
        this.newUser.setUserInterests(EMPTY_INTEREST_LIST);
        assertEquals(newUser.getUserInterests(),EMPTY_INTEREST_LIST);

        this.newUser.setUserInterests(INTERESTS);
        assertEquals(newUser.getUserInterests(),INTERESTS);
    }

    @Test
    void testDeleteUserInterest(){
        this.newUser.setUserInterests(INTERESTS);
        this.newUser.deleteUserInterest("News");

        assertEquals(newUser.getUserInterests(),INTERESTS_2);
    }
}