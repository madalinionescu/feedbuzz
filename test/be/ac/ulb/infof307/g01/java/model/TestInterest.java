package be.ac.ulb.infof307.g01.java.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static be.ac.ulb.infof307.g01.java.ConstantsForTests.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TestInterest {
    private Interest interest1;
    private Interest interest2;

    @BeforeEach
    void setUp() {
        this.interest1 = new Interest(INTEREST_NAME_1,RELATED_RSS_1,RELATED_TAGS_1);
        this.interest2 = new Interest(INTEREST_NAME_2,RELATED_RSS_2,RELATED_TAGS_2);
    }

    @Test
    void testIsRelatedToRSS() {
        assertTrue(interest1.isRelatedToRSS(RSS1));
        assertFalse(interest1.isRelatedToRSS(RSS3));
    }

    @Test
    void testIsRelatedToTag() {
        assertTrue(interest1.isRelatedToTag(TAG1));
        assertTrue(interest2.isRelatedToTag(TAG3));

        assertFalse(interest1.isRelatedToTag(TAG3));
        assertFalse(interest2.isRelatedToTag(TAG2));
    }

    @Test
    void testAddRSS(){
        interest1.addRSS(VALID_URL_2);
        assertTrue(interest1.isRelatedToRSS(RSS2));
    }

    @Test
    void testAddTag(){
        interest1.addTag(TAG3);
        assertTrue(interest1.isRelatedToTag(TAG3));
    }
}