package be.ac.ulb.infof307.g01.java.model.dao;

import be.ac.ulb.infof307.g01.java.model.LocalUser;
import be.ac.ulb.infof307.g01.java.model.Search;
import be.ac.ulb.infof307.g01.java.model.SearchFilter;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.ArrayList;
import java.util.List;

import static be.ac.ulb.infof307.g01.java.ConstantsForTests.*;
import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestSearchFilterDAO {

    private static Search search1;
    private static Search search2;

    @BeforeAll
    static void setUpSearchObjects() {
        search1 = new Search(TITLE_ARTICLE_1, CONTENT_ARTICLE_1, AUTHOR_ARTICLE_1, DATE_ARTICLE_1,
                Search.BEFORE, LOCATION_ARTICLE_1, SOURCE_ARTICLE_1, LANGUAGE_1, HASHTAGS_ARTICLE_1, TAG_NAME_1);
        search2 = new Search(TITLE_ARTICLE_2, CONTENT_ARTICLE_2, AUTHOR_ARTICLE_2, DATE_ARTICLE_2,
                Search.AFTER, LOCATION_ARTICLE_2, SOURCE_ARTICLE_2, LANGUAGE_2, HASHTAGS_ARTICLE_2, TAG_NAME_2);
    }

    @BeforeEach
    void setUp(){
        DAOFactory.changeDatabasePath("./dbTest");
        DAOFactory.clearAll();

        LocalUser.singleton.create(USERNAME_USER_1,  PASSWORD_USER_1,  MAIL_USER_1);
        LocalUser.singleton.connect(USERNAME_USER_1,PASSWORD_USER_1);
    }

    @Test
    void testGetModelClass(){
        assertEquals(DAOFactory.getSearchFilterDAO().getModelClass(), SearchFilter.class);
    }


    @Test
    void testFindByString() {
        SearchFilter validSearcHFilter = new SearchFilter(search1, FILTER_1);
        DAOFactory.getSearchFilterDAO().create(validSearcHFilter);
        SearchFilter validSearch = DAOFactory.getSearchFilterDAO().findByString(FILTER_1);
        assertNotNull(validSearch);
        assertEquals(validSearch,validSearcHFilter);
        assertNull(DAOFactory.getSearchFilterDAO().findByString(FILTER_2));
        assertNull(DAOFactory.getSearchFilterDAO().findByString(null));
    }

    @Test
    void testFindAll() {
        SearchFilter validSearcHFilter1 = new SearchFilter(search1, FILTER_1);
        SearchFilter validSearcHFilter2 = new SearchFilter(search2, FILTER_2);

        DAOFactory.getSearchFilterDAO().create(validSearcHFilter1);
        DAOFactory.getSearchFilterDAO().create(validSearcHFilter2);

        List<SearchFilter> filters = new ArrayList<>();
        filters.add(validSearcHFilter1);
        filters.add(validSearcHFilter2);

        assertEquals(filters, DAOFactory.getSearchFilterDAO().findAll());
    }

    @Test
    void testCreate() {
        SearchFilter validSearchFilter = new SearchFilter(search1, FILTER_1);
        assertFalse(DAOFactory.getSearchFilterDAO().create(null));
        assertTrue(DAOFactory.getSearchFilterDAO().create(validSearchFilter));

        SearchFilter find = DAOFactory.getSearchFilterDAO().findByString(FILTER_1);
        assertNotNull(find);
        assertEquals(find, validSearchFilter);
    }

    @Test
    void testFindNextAvailableName() {
        SearchFilter filter1 = new SearchFilter(search1, FILTER_1);
        DAOFactory.getSearchFilterDAO().create(filter1);
        String nextName = FILTER_1 + " 1";
        assertEquals(DAOFactory.getSearchFilterDAO().findNextAvailableName(filter1), nextName);
    }

}