package be.ac.ulb.infof307.g01.java.model;

import org.junit.jupiter.api.Test;

import static be.ac.ulb.infof307.g01.java.ConstantsForTests.HASHTAG_TWITTER_1;
import static org.junit.jupiter.api.Assertions.*;

class TestInformationTwitter {
    private static final int ARTICLE_COUNT_1 = 10;
    private final InformationTwitter informationTwitter = new InformationTwitter(HASHTAG_TWITTER_1,ARTICLE_COUNT_1, "hashtag");

    @Test
    void testGetNumberPosts() { assertEquals(10, informationTwitter.getNumberPosts()); }

    @Test
    void testSetNumberPosts() {
        informationTwitter.setNumberPosts(5);
        assertEquals(5, informationTwitter.getNumberPosts());
    }

    @Test
    void testSetName() {
        informationTwitter.setName("Java");
        assertEquals("Java", informationTwitter.getName());
    }

    @Test
    void testGetName() { assertEquals("Sea", informationTwitter.getName()); }
}