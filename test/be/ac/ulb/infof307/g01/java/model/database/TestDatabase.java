package be.ac.ulb.infof307.g01.java.model.database;

import be.ac.ulb.infof307.g01.java.model.dao.DAOFactory;
import be.ac.ulb.infof307.g01.java.model.Directory;
import be.ac.ulb.infof307.g01.java.model.Interest;
import org.junit.jupiter.api.*;

import static be.ac.ulb.infof307.g01.java.ConstantsForTests.*;
import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestDatabase {

    private Directory directory;
    private Directory directory2;
    private Interest interest;
    private Database db;

    @BeforeAll
    void setUp() {

        DAOFactory.changeDatabasePath(PATH_DB_TEST );
        DAOFactory.clearAll();

        db = new Database(PATH_DB_TEST);

        db.clearAll();
        directory = new Directory(DIRECTORY_ARTICLE_1);
        directory2 = new Directory(DIRECTORY_ARTICLE_2);
        interest = new Interest(INTEREST_NAME_1,RELATED_RSS_1,RELATED_TAGS_1);

    }

    @Test
    void testCreate() {
        db.create(directory);
        db.create(interest);
        assertNotNull(db.findById( directory.getId(),directory.getClass()));
        assertNotNull(db.findById(interest.getId(),interest.getClass()));

    }

    @Test
    void testUpdate() {
        db.create(directory);
        db.create(interest);

        directory.setName(DIRECTORY_ARTICLE_2);
        db.update(directory);

        directory = db.findById(directory.getId(),directory.getClass());

        assertEquals(directory.getName(),DIRECTORY_ARTICLE_2);
    }

    @Test
    void testDelete() {
        db.create(directory);
        db.create(interest);
        assertNotNull(db.findById( directory.getId(),directory.getClass()));
        assertNotNull(db.findById(interest.getId(),interest.getClass()));
        db.delete(directory);
        db.delete(interest);
        assertNull(db.findById( directory.getId(),directory.getClass()));
        assertNull(db.findById(interest.getId(),interest.getClass()));
    }

    @Test
    void testFindById() {
        db.create(directory);
        db.create(interest);
        assertNotNull(db.findById( directory.getId(),directory.getClass()));
        assertNotNull(db.findById(interest.getId(),interest.getClass()));
        assertNull(db.findById(null,interest.getClass()));
        assertThrows(Exception.class,() -> db.findById(interest.getId(),null));
    }

    @Test
    void testIsFound() {
        db.create(directory);
        db.create(interest);
        assertTrue(db.isFound(directory));
        assertTrue(db.isFound(interest));
        assertFalse(db.isFound(directory2));
        assertThrows(Exception.class,() -> db.isFound(null));
    }

    @Test
    void testClear() {
        db.create(directory);
        db.create(interest);
        db.clear(directory.getClass());
        assertNull(db.findById( directory.getId(),directory.getClass()));
        assertNotNull(db.findById(interest.getId(),interest.getClass()));
        assertThrows(Exception.class,() ->  db.clear(null));
    }

    @Test
    void testClearAll() {
        db.create(directory);
        db.create(interest);
        db.clearAll();
        assertNull(db.findById( directory.getId(),directory.getClass()));
        assertNull(db.findById(interest.getId(),interest.getClass()));
    }

    @Test
    void testGenerateUniqueId() {
        String idA,idB,idC;

        db.create(directory);

        db.generateUniqueId(directory);
        idA = directory.getId();

        db.generateUniqueId(directory);
        idB = directory.getId();

        db.generateUniqueId(directory);
        idC = directory.getId();

        assertNotEquals(idA,idB);
        assertNotEquals(idA,idC);
        assertNotEquals(idB,idC);
    }
}