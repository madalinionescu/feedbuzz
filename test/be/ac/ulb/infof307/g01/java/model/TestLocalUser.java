package be.ac.ulb.infof307.g01.java.model;

import be.ac.ulb.infof307.g01.java.model.dao.DAOFactory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static be.ac.ulb.infof307.g01.java.ConstantsForTests.*;
import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestLocalUser {
    @BeforeAll
    void setUp(){
        DAOFactory.changeDatabasePath(PATH_DB_TEST );
        DAOFactory.clearAll();
        LocalUser.singleton.create(USERNAME_USER_1, PASSWORD_USER_1, MAIL_USER_1);
    }

    @Test
    void testCreate(){
        LocalUser.singleton.create(USERNAME_USER_1, PASSWORD_USER_1, MAIL_USER_1);
        assertNotNull(DAOFactory.getUserDAO().findById(USERNAME_USER_1));
        assertFalse(LocalUser.singleton.create(null, PASSWORD_USER_2, MAIL_USER_2));
    }

    @Test
    void testConnect() {
        assertTrue( LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1));
        assertFalse( LocalUser.singleton.connect(USERNAME_USER_1, BAD_PASSWORD));
        assertFalse( LocalUser.singleton.connect(BAD_USERNAME, BAD_PASSWORD));
        assertFalse( LocalUser.singleton.connect(BAD_USERNAME, PASSWORD_USER_1));
        assertFalse( LocalUser.singleton.connect(null, BAD_PASSWORD));
        assertFalse( LocalUser.singleton.connect(BAD_USERNAME,null));
        assertFalse( LocalUser.singleton.connect(null,null));
    }

    @Test
    void testIsConnect() {
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        assertTrue(LocalUser.singleton.isConnect());
        LocalUser.singleton.connect(BAD_USERNAME, BAD_PASSWORD);
        assertFalse(LocalUser.singleton.isConnect());
        LocalUser.singleton.connect(null,null);
        assertFalse(LocalUser.singleton.isConnect());
    }

    @Test
    void testDisconnect() {
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        LocalUser.singleton.disconnect();
        assertFalse(LocalUser.singleton.isConnect());
        assertNull(LocalUser.singleton.getUser());

        LocalUser.singleton.connect(null,null);
        LocalUser.singleton.disconnect();
        assertFalse(LocalUser.singleton.isConnect());
        assertNull(LocalUser.singleton.getUser());
    }

    @Test
    void testGetUser() {
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        assertEquals(LocalUser.singleton.getUserId(), LocalUser.singleton.getUserId());
        assertNotNull(LocalUser.singleton.getUserId());
    }

    @AfterAll
    void tearDown() {
        //Détruire Db de test
        DAOFactory.clearAll();
    }
}
