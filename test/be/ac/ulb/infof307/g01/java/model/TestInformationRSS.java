package be.ac.ulb.infof307.g01.java.model;

import org.junit.jupiter.api.Test;

import static be.ac.ulb.infof307.g01.java.ConstantsForTests.*;
import static org.junit.jupiter.api.Assertions.*;

class TestInformationRSS {
    private final InformationRSS testInformationRSS = new InformationRSS(RSS_LINK_ARTICLE_1);

    @Test
    void testGetLink() {
        assertEquals(RSS_LINK_ARTICLE_1, testInformationRSS.getLink());
    }

    @Test
    void testSetLink() {
        testInformationRSS.setLink(RSS_LINK_ARTICLE_2);
        assertEquals(RSS_LINK_ARTICLE_2,testInformationRSS.getLink());
    }

    @Test
    void testGetNbArticles() {
        assertEquals(3, testInformationRSS.getNbArticles());
    }

    @Test
    void testSetNbArticles() {
        testInformationRSS.setNbArticles(5);
        assertEquals(5, testInformationRSS.getNbArticles());
    }

    @Test
    void testIsTitleSelected() {
        assertTrue(testInformationRSS.isTitleSelected());
    }

    @Test
    void testSetTitleSelected() {
        testInformationRSS.setTitleSelected(false);
        assertFalse(testInformationRSS.isTitleSelected());
    }

    @Test
    void testIsContentSelected() {
        assertTrue(testInformationRSS.isContentSelected());
    }

    @Test
    void testSetContentSelected() {
        testInformationRSS.setContentSelected(false);
        assertFalse(testInformationRSS.isContentSelected());
    }

    @Test
    void testIsAuthorSelected() {
        assertTrue(testInformationRSS.isAuthorSelected());
    }

    @Test
    void testSetAuthorSelected() {
        testInformationRSS.setAuthorSelected(false);
        assertFalse(testInformationRSS.isAuthorSelected());
    }

    @Test
    void testIsLinkSelected() {
        assertTrue(testInformationRSS.isLinkSelected());
    }

    @Test
    void testSetLinkSelected() {
        testInformationRSS.setLinkSelected(false);
        assertFalse(testInformationRSS.isLinkSelected());
    }

    @Test
    void testIsSourceSelected() {
        assertTrue(testInformationRSS.isSourceSelected());
    }

    @Test
    void testSetSourceSelected() {
        testInformationRSS.setSourceSelected(false);
        assertFalse(testInformationRSS.isSourceSelected());
    }

    @Test
    void testIsLocationSelected() {
        assertTrue(testInformationRSS.isLocationSelected());
    }

    @Test
    void testSetLocationSelected() {
        testInformationRSS.setLocationSelected(false);
        assertFalse(testInformationRSS.isLocationSelected());
    }

    @Test
    void testIsDateSelected() {
        assertTrue(testInformationRSS.isDateSelected());
    }

    @Test
    void testSetDateSelected() {
        testInformationRSS.setDateSelected(false);
        assertFalse(testInformationRSS.isDateSelected());
    }
}