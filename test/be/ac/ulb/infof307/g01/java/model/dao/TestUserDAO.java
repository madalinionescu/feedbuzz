package be.ac.ulb.infof307.g01.java.model.dao;

import be.ac.ulb.infof307.g01.java.model.LocalUser;
import be.ac.ulb.infof307.g01.java.model.Tag;
import be.ac.ulb.infof307.g01.java.model.User;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.ArrayList;
import java.util.List;

import static be.ac.ulb.infof307.g01.java.ConstantsForTests.*;
import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestUserDAO {
    @BeforeAll
    void setUp(){
        DAOFactory.changeDatabasePath(PATH_DB_TEST );
        DAOFactory.clearAll();
        LocalUser.singleton.create(USERNAME_USER_1, PASSWORD_USER_1,  MAIL_USER_1);
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
    }

    @Test
    void testCreate(){
        User testUser = new User(USERNAME_USER_2, PASSWORD_USER_2, MAIL_USER_2, new ArrayList<>());
        assertFalse(DAOFactory.getUserDAO().create(null));
        assertNull(DAOFactory.getUserDAO().create(null, null, null));
        assertNotNull(DAOFactory.getUserDAO().create(USERNAME_USER_2, PASSWORD_USER_2, MAIL_USER_2));
        User find = DAOFactory.getUserDAO().find(USERNAME_USER_2, PASSWORD_USER_2);
        assertNotNull(find);
        assertEquals(find, testUser);
    }

    @Test
    void testFind(){
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        User find1 = DAOFactory.getUserDAO().find(USERNAME_USER_1, PASSWORD_USER_1);
        assertTrue(find1 != null &&find1.equals(LocalUser.singleton.getUser()));
        assertNull(DAOFactory.getUserDAO().find(BAD_USERNAME, BAD_PASSWORD));
        assertNull(DAOFactory.getUserDAO().find(null, null));
    }

    @Test
    void testGetModelClass(){
        assertEquals(DAOFactory.getUserDAO().getModelClass(), User.class);
    }

    @Test
    void testAddTag(){
        User user1 = LocalUser.singleton.getUser();
        assertEquals(user1.getUserTags(),new ArrayList<Tag>());
        DAOFactory.getUserDAO().addTag(TAG_NAME_1);
        List<Tag> tmpTags = user1.getUserTags();
        Tag tmpTag = new Tag(TAG_NAME_1);
        assertTrue(tmpTags.contains(tmpTag));

        User user2 = DAOFactory.getUserDAO().find(USERNAME_USER_1, PASSWORD_USER_1);
        tmpTags = user2.getUserTags();
        assertTrue(tmpTags.contains(tmpTag));

    }

    @Test
    void testDeleteTag(){
        User user = LocalUser.singleton.getUser();
        DAOFactory.getUserDAO().addTag(TAG_NAME_1);
        DAOFactory.getUserDAO().addTag(TAG_NAME_2);
        List<Tag> tmpTags = user.getUserTags();
        assertEquals(tmpTags.size(),2);
        DAOFactory.getUserDAO().deleteTag(TAG_NAME_1);
        assertEquals(tmpTags.size(),1);
        Tag tmpTag = new Tag(TAG_NAME_2);
        assertTrue(tmpTags.contains(tmpTag));
        DAOFactory.getUserDAO().deleteTag(TAG_NAME_2);
        assertEquals(tmpTags.size(),0);
    }

    @AfterAll
    void tearDown(){
        DAOFactory.clearAll();
    }
}
