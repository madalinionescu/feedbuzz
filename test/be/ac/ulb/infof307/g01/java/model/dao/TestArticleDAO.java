package be.ac.ulb.infof307.g01.java.model.dao;

import be.ac.ulb.infof307.g01.java.exception.TagExistsException;
import be.ac.ulb.infof307.g01.java.model.Article;
import be.ac.ulb.infof307.g01.java.model.LocalUser;
import be.ac.ulb.infof307.g01.java.model.Tag;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.ArrayList;
import java.util.List;

import static be.ac.ulb.infof307.g01.java.ConstantsForTests.*;
import static be.ac.ulb.infof307.g01.java.Constants.*;
import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestArticleDAO {
    @BeforeEach
    void setUp(){
        DAOFactory.changeDatabasePath(PATH_DB_TEST );
        DAOFactory.clearAll();
        LocalUser.singleton.create(USERNAME_USER_1, PASSWORD_USER_1,  MAIL_USER_1);
        LocalUser.singleton.create(USERNAME_USER_2, PASSWORD_USER_2,  MAIL_USER_2);
    }

    @Test
    void testCreate(){

        Article articleA = getArticle1();

        //On check que les données ne sont pas accessible d'un user à l'autre et qu'il existe
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);

        assertTrue(DAOFactory.getArticleDAO().create(articleA));
        assertFalse(DAOFactory.getArticleDAO().create(null));

        assertNotNull(DAOFactory.getArticleDAO().findById(articleA.getId()));

        LocalUser.singleton.connect(USERNAME_USER_2, PASSWORD_USER_2);

        assertNull(DAOFactory.getArticleDAO().findById(articleA.getId()));
    }


    @Test
    void testFindAll(){
        Article articleA = getArticle1();
        Article articleB = getArticle1();
        Article articleC = getArticle1();
        Article articleA2 = getArticle1();
        Article articleB2 = getArticle1();

        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);

        DAOFactory.getArticleDAO().create(articleA);
        DAOFactory.getArticleDAO().create(articleB);
        DAOFactory.getArticleDAO().create(articleC);

        LocalUser.singleton.connect(USERNAME_USER_2, PASSWORD_USER_2);

        DAOFactory.getArticleDAO().create(articleA2);
        DAOFactory.getArticleDAO().create(articleB2);

        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);

        assertEquals(DAOFactory.getArticleDAO().findAll().size(), 3);
        LocalUser.singleton.connect(USERNAME_USER_2, PASSWORD_USER_2);
        assertEquals(DAOFactory.getArticleDAO().findAll().size(), 2);
    }


    @Test
    void testClear(){
        Article articleB = getArticle1();
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        DAOFactory.getArticleDAO().create(articleB);
        DAOFactory.getArticleDAO().clear();
        assertNull(DAOFactory.getArticleDAO().findById(articleB.getId()));
        DAOFactory.getArticleDAO().create(articleB);
        LocalUser.singleton.connect(USERNAME_USER_2, PASSWORD_USER_2);
        DAOFactory.getArticleDAO().clear();
        assertNull(DAOFactory.getArticleDAO().findById(articleB.getId()));
        DAOFactory.getArticleDAO().update(getArticle1());
    }

    @Test
    void testGetModelClass(){
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        assertEquals(DAOFactory.getArticleDAO().getModelClass(),Article.class);
    }

    @Test
    void testDeleteArticlesFromFeed(){
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        Article article = getArticle1();
        Article articleNotDelete = getArticle2();
        Article articleTweet = getArticle3();
        DAOFactory.getArticleDAO().clear();
        DAOFactory.getArticleDAO().create(article);
        DAOFactory.getArticleDAO().create(articleNotDelete);
        DAOFactory.getArticleDAO().create(articleTweet);
        assertEquals(3, DAOFactory.getArticleDAO().findAll().size());
        ArticleDAO.deleteArticlesFromFeed(RSS_LINK_ARTICLE_1);
        assertEquals(2, DAOFactory.getArticleDAO().findAll().size());
        ArticleDAO.deleteArticlesFromFeed(HASHTAG_TWITTER_1);
        assertEquals(1, DAOFactory.getArticleDAO().findAll().size());
        DAOFactory.getArticleDAO().update(getArticle1());
    }

    @Test
    void testAddTagToArticle(){
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        Article article = getArticle1();
        try {
            DAOFactory.getArticleDAO().addTagToArticle(TAG_NAME_4, article);
            assertEquals(article.getTags().size(),4);
        }catch (TagExistsException ignored){}
        // throws an exception because the tag already exists
        assertThrows(TagExistsException.class,() -> DAOFactory.getArticleDAO().addTagToArticle(TAG_NAME_4, article));

        DAOFactory.getArticleDAO().update(getArticle1());
    }

    @Test
    void testDeleteTagFromArticle(){
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        ArrayList<Tag> tmpTags = new ArrayList<>();
        tmpTags.add(new Tag(TAG_NAME_2));
        Article article = getArticle1();
        article.setTags(tmpTags);
        assertEquals(1, article.getTags().size());
        DAOFactory.getArticleDAO().deleteTagFromArticle(TAG_NAME_2,article);
        assertEquals(0, article.getTags().size());
        DAOFactory.getArticleDAO().update(getArticle1());
    }

    @Test
    void testHasTagInArticle(){
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        Article article = getArticle1(); //3 Tag par défaut
        assertTrue(DAOFactory.getArticleDAO().hasTagInArticle(TAG_NAME_1, article));
        DAOFactory.getArticleDAO().deleteTagFromArticle(TAG_NAME_1,article);
        assertFalse(DAOFactory.getArticleDAO().hasTagInArticle(TAG_NAME_1, article));
        DAOFactory.getArticleDAO().update(getArticle1());
    }

    @Test
    void testTagAsReading(){
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        Article article = getArticle2(); //0 Tag par défaut
        DAOFactory.getArticleDAO().tagAsReading(article);
        assertTrue(DAOFactory.getArticleDAO().hasTagInArticle(TAG_NAME_READING, article));
        assertFalse(DAOFactory.getArticleDAO().hasTagInArticle(TAG_NAME_FINISHED, article));
        assertFalse(DAOFactory.getArticleDAO().hasTagInArticle(TAG_NAME_NOT_READ, article));
        DAOFactory.getArticleDAO().update(getArticle1());
    }

    @Test
    void testTagAsFinished(){
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        Article article = getArticle1(); //3 Tag par défaut
        DAOFactory.getArticleDAO().tagAsFinished(article);
        assertTrue(DAOFactory.getArticleDAO().hasTagInArticle(TAG_NAME_FINISHED, article));
        assertFalse(DAOFactory.getArticleDAO().hasTagInArticle(TAG_NAME_READING, article));
        assertFalse(DAOFactory.getArticleDAO().hasTagInArticle(TAG_NAME_NOT_READ, article));
        DAOFactory.getArticleDAO().update(getArticle1());
    }

    @Test
    void testTagAsRecommended() {
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        Article article = getArticle1();
        DAOFactory.getArticleDAO().tagAsRecommended(article);
        assertTrue(DAOFactory.getArticleDAO().hasTagInArticle(TAG_NAME_RECOMMENDED, article));
        DAOFactory.getArticleDAO().update(getArticle1());
    }

    @Test
    void testGetTagsAsStrings() throws TagExistsException {
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        Article article = getArticle2(); //0 Tag par défaut
        DAOFactory.getArticleDAO().addTagToArticle(TAG_NAME_1, article);
        DAOFactory.getArticleDAO().addTagToArticle(TAG_NAME_2, article);
        List<String> tags = DAOFactory.getArticleDAO().getTagsAsStrings(article);
        assertEquals(3, tags.size());
        assertTrue(tags.contains(TAG_NAME_1));
        assertTrue(tags.contains(TAG_NAME_2));
        DAOFactory.getArticleDAO().update(getArticle1());
    }

    @Test
    void testFindByName() {
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        Article article = getArticle1();
        DAOFactory.getArticleDAO().create(article);
        assertEquals(article.getId(), DAOFactory.getArticleDAO().findByName(article.getName()).getId());
    }
}
