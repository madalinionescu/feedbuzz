package be.ac.ulb.infof307.g01.java.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestDirectory {
    private static final String DIR_HOME = "home";
    private static final String DIR_NAME_1 = "dfsdqf";
    private static final String DIR_NAME_2 = "fqfdsfqsdf";
    private static final String DIR_NAME_3 = "fqsfqsdfqsdf";
    private static final String DIR_NAME_4 = "fdfsqdfqsdfqsd";
    private static final String DIR_NAME_5 = "dfqsdfqsdfqsdf";
    private static final String ID_2 = "fdsqfdsfqs";
    @Test
    void testGetId() {
        Directory dir = new Directory(DIR_NAME_1);
        Assertions.assertNull(dir.getId());
    }

    @Test
    void testSetId() {
        Directory dir = new Directory(DIR_HOME);
        String id = DIR_NAME_2;
        dir.setId(id);
        Assertions.assertEquals(dir.getId(),id);
        dir.setId(null);
        Assertions.assertNull(dir.getId());
    }

    @Test
    void testGetName() {
        String name = DIR_NAME_3;
        Directory dir = new Directory(name);
        Assertions.assertEquals(dir.getName(),name);
    }

    @Test
    void testSetName() {
        String name = DIR_NAME_3;
        Directory dir = new Directory(DIR_NAME_4);
        dir.setName(name);
        Assertions.assertEquals(dir.getName(),name);
        dir.setName(null);
        Assertions.assertNull(dir.getName());
    }

}