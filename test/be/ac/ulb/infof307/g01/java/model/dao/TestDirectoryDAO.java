package be.ac.ulb.infof307.g01.java.model.dao;

import be.ac.ulb.infof307.g01.java.model.Article;
import be.ac.ulb.infof307.g01.java.model.Directory;
import be.ac.ulb.infof307.g01.java.model.LocalUser;
import org.junit.jupiter.api.*;

import static be.ac.ulb.infof307.g01.java.Constants.ROOT_DIRECTORY_NAME;
import static be.ac.ulb.infof307.g01.java.ConstantsForTests.*;
import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestDirectoryDAO {
    private Directory grandFather;
    private Directory father;
    private Directory son;
    private Directory sister;
    private Article news;

    @BeforeEach
    void setUp() {
        DAOFactory.changeDatabasePath(PATH_DB_TEST );
        DAOFactory.clearAll();

        LocalUser.singleton.create(USERNAME_USER_1, PASSWORD_USER_1,  MAIL_USER_1);
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);

        Directory rootDirectory = new Directory(ROOT_DIRECTORY_NAME);
        DAOFactory.getDirectoryDAO().create(rootDirectory);

        grandFather = new Directory(GRAND_FATHER);
        DAOFactory.getDirectoryDAO().create(grandFather);

        father = new Directory(FATHER);
        DAOFactory.getDirectoryDAO().create(father);

        son = new Directory(SON);
        DAOFactory.getDirectoryDAO().create(son);

        sister = new Directory(SISTER);

        news = getArticle1();
        DAOFactory.getArticleDAO().create(news);


        // Ajout d'un hiérarchie
        father.addNode(son);
        DAOFactory.getDirectoryDAO().update(father);
        grandFather.addNode(father);
        DAOFactory.getDirectoryDAO().update(grandFather);
        rootDirectory.addNode(grandFather);
        DAOFactory.getDirectoryDAO().update(rootDirectory);

    }

    @AfterAll
    void tearDown() {
        DAOFactory.clearAll();
    }

    @Test
    void testGetModelClass() {
        assertEquals(DAOFactory.getDirectoryDAO().getModelClass(), Directory.class);
    }

    @Test
    void testFindByName() {
        assertEquals(GRAND_FATHER, DAOFactory.getDirectoryDAO().findByName(GRAND_FATHER).getName());
        assertNull(DAOFactory.getDirectoryDAO().findByName(NULL_DIRECTORY));
    }

    @Test
    void testDeleteDirectory() {
        DAOFactory.getDirectoryDAO().deleteDirectory(son.getId());
        assertNull(DAOFactory.getDirectoryDAO().findByName(SON));
    }

    @Test
    void testChangeNodePath() {
        DAOFactory.getDirectoryDAO().changeNodePath(grandFather, son);
        assertTrue(grandFather.getNodes().contains(son.getId()));
    }

    @Test
    void testCreateIfNotExists() {
        assertNull(DAOFactory.getDirectoryDAO().findByName(SISTER));
        DAOFactory.getDirectoryDAO().createIfNotExists(sister);
        String sisterId = sister.getId();

        assertEquals(SISTER, DAOFactory.getDirectoryDAO().findByName(SISTER).getName());
        DAOFactory.getDirectoryDAO().createIfNotExists(sister);
        assertEquals(sisterId, DAOFactory.getDirectoryDAO().findByName(SISTER).getId());

    }

    @Test
    void testAddChildToRoot() {
        DAOFactory.getDirectoryDAO().addChildToRoot(news);
        assertEquals(news.getName(), DAOFactory.getArticleDAO().findById(news.getId()).getName());
    }


}