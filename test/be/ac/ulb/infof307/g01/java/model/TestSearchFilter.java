package be.ac.ulb.infof307.g01.java.model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static be.ac.ulb.infof307.g01.java.ConstantsForTests.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TestSearchFilter {
    private SearchFilter filter1;
    private SearchFilter filter2;

    private static Search search1;
    private static Search search2;

    @BeforeAll
    static void setUpSearchObjects() {
        search1 = new Search(TITLE_ARTICLE_1, CONTENT_ARTICLE_1, AUTHOR_ARTICLE_1, DATE_ARTICLE_1,
                Search.BEFORE, LOCATION_ARTICLE_1, SOURCE_ARTICLE_1, LANGUAGE_1, HASHTAGS_ARTICLE_1, TAG_NAME_1);
        search2 = new Search(TITLE_ARTICLE_2, CONTENT_ARTICLE_2, AUTHOR_ARTICLE_2, DATE_ARTICLE_2,
                Search.AFTER, LOCATION_ARTICLE_2, SOURCE_ARTICLE_2, LANGUAGE_2, HASHTAGS_ARTICLE_2, TAG_NAME_2);
    }

    @BeforeEach
    void setUp() {
        this.filter1 = new SearchFilter(search1, FILTER_1);
        this.filter2 = new SearchFilter(search2, FILTER_2);
    }

    @Test
    void testGetName() {
        assertEquals(filter1.getName(), FILTER_1);
        assertEquals(filter2.getName(), FILTER_2);
    }

    @Test
    void testSetName() {
        filter1.setName(FILTER_2);
        assertEquals(filter1.getName(), FILTER_2);
        filter2.setName(FILTER_1);
        assertEquals(filter2.getName(), FILTER_1);
        filter1.setName(FILTER_1);
        assertEquals(filter1.getName(), FILTER_1);
    }
}