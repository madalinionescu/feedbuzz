package be.ac.ulb.infof307.g01.java.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TestTag {
    private static final String STRING_TAG = "monTag";
    private static final Tag TAG = new Tag(STRING_TAG);


    @Test
    void testGetName() {
        assertEquals(STRING_TAG, TAG.getName());
    }
}