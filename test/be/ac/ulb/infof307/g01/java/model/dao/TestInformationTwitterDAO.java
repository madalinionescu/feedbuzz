package be.ac.ulb.infof307.g01.java.model.dao;

import be.ac.ulb.infof307.g01.java.model.InformationTwitter;
import be.ac.ulb.infof307.g01.java.model.LocalUser;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static be.ac.ulb.infof307.g01.java.ConstantsForTests.*;
import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestInformationTwitterDAO {
    private static final String HASHTAG_NAME_1 = "Sea";
    private static final int ARTICLES_NUMBER_1 = 10;
    private static final String HASHTAG_TYPE_1 = "hashtag";

    @BeforeAll
    void setUp(){
        DAOFactory.changeDatabasePath(PATH_DB_TEST);
        DAOFactory.clearAll();

        LocalUser.singleton.create(USERNAME_USER_1, PASSWORD_USER_1,  MAIL_USER_1);
        LocalUser.singleton.create(USERNAME_USER_2, PASSWORD_USER_2,  MAIL_USER_2);
    }

    @Test
    void testCreate(){
        InformationTwitter informationTwitter1 = new InformationTwitter(HASHTAG_NAME_1,ARTICLES_NUMBER_1, HASHTAG_TYPE_1);

        //On verifie que les données ne sont pas accessible d'un user à l'autre et qu'il existe
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        assertTrue(DAOFactory.getInformationTwitterDAO().create(informationTwitter1));
        assertFalse(DAOFactory.getInformationTwitterDAO().create(null));
        assertNotNull(DAOFactory.getInformationTwitterDAO().findById(informationTwitter1.getId()));
        assertNull(DAOFactory.getInformationTwitterDAO().findById(null));

        LocalUser.singleton.connect(USERNAME_USER_2, PASSWORD_USER_2);
        assertNull(DAOFactory.getInformationTwitterDAO().findById(informationTwitter1.getId()));
    }


    @Test
    void testFindAll(){
        InformationTwitter infoHashtag1 = new InformationTwitter(HASHTAG_NAME_1,ARTICLES_NUMBER_1, HASHTAG_TYPE_1);
        InformationTwitter infoHashtag2 = new InformationTwitter(HASHTAG_NAME_1,ARTICLES_NUMBER_1, HASHTAG_TYPE_1);
        InformationTwitter infoHashtag3 = new InformationTwitter(HASHTAG_NAME_1,ARTICLES_NUMBER_1, HASHTAG_TYPE_1);
        InformationTwitter infoHashtag4 = new InformationTwitter(HASHTAG_NAME_1,ARTICLES_NUMBER_1, HASHTAG_TYPE_1);
        InformationTwitter infoHashtag5 = new InformationTwitter(HASHTAG_NAME_1,ARTICLES_NUMBER_1, HASHTAG_TYPE_1);

        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        DAOFactory.getInformationTwitterDAO().create(infoHashtag1);
        DAOFactory.getInformationTwitterDAO().create(infoHashtag2);
        DAOFactory.getInformationTwitterDAO().create(infoHashtag3);

        LocalUser.singleton.connect(USERNAME_USER_2, PASSWORD_USER_2);
        DAOFactory.getInformationTwitterDAO().create(infoHashtag4);
        DAOFactory.getInformationTwitterDAO().create(infoHashtag5);

        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        assertEquals(3, DAOFactory.getInformationTwitterDAO().findAll().size());

        LocalUser.singleton.connect(USERNAME_USER_2, PASSWORD_USER_2);
        assertEquals(2, DAOFactory.getInformationTwitterDAO().findAll().size());
    }


    @Test
    void testClear(){
        InformationTwitter informationTwitter1 = new InformationTwitter(HASHTAG_NAME_1,ARTICLES_NUMBER_1, HASHTAG_TYPE_1);
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        DAOFactory.getInformationTwitterDAO().create(informationTwitter1);
        DAOFactory.getInformationTwitterDAO().clear();
        assertNull(DAOFactory.getInformationTwitterDAO().findById(informationTwitter1.getId()));
        DAOFactory.getInformationTwitterDAO().create(informationTwitter1);
        LocalUser.singleton.connect(USERNAME_USER_2, PASSWORD_USER_2);
        DAOFactory.getInformationTwitterDAO().clear();
        assertNull(DAOFactory.getInformationTwitterDAO().findById(informationTwitter1.getId()));
    }

    @Test
    void testGetModelClass() {
        assertEquals(DAOFactory.getInformationTwitterDAO().getModelClass(), InformationTwitter.class);
    }
}