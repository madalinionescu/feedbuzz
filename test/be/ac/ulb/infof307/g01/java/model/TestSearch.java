package be.ac.ulb.infof307.g01.java.model;

import be.ac.ulb.infof307.g01.java.model.dao.DAOFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static be.ac.ulb.infof307.g01.java.ConstantsForTests.*;

class TestSearch {
    private static final String TITLE = "test";
    private static final String CONTENT = "test";
    private static final String AUTHOR = "Person";
    private static final String DATE = "1970/01/01";
    private static final String DATECOMP = "After";
    private static final String DATEBEFORE = "Before", DATEAFTER = "After", DATEON = "On Date";
    private static final String LOCATION = "Brussels";
    private static final String SOURCE = "The moon";
    private static final String LANG = "EN";
    private static final List<String> HASHTAGS = new ArrayList<>();
    private static final String TAG = "Tag", TAG2 = "Tag2";

    private final Search search = new Search("temp", "temp", "temp", "temp",
            DATEBEFORE, "temp", "temp", "temp", HASHTAGS, TAG);

    @BeforeEach
    void setUp(){
        DAOFactory.changeDatabasePath(PATH_DB_TEST );
        DAOFactory.clearAll();
        LocalUser.singleton.create(USERNAME_USER_1, PASSWORD_USER_1,  MAIL_USER_1);
        LocalUser.singleton.create(USERNAME_USER_2, PASSWORD_USER_2,  MAIL_USER_2);
    }

    @Test
    void testGetTitle(){assertEquals("temp", search.getTitle());}

    @Test
    void testSetTitle(){
        search.setTitle(TITLE);
        assertEquals(TITLE, search.getTitle());
    }

    @Test
    void testGetContent(){assertEquals("temp", search.getTitle());}

    @Test
    void testSetContent(){
        search.setContent(CONTENT);
        assertEquals(TITLE, search.getContent());
    }

    @Test
    void testGetAuthor() {
        assertEquals("temp", search.getAuthor());
    }

    @Test
    void testSetAuthor() {
        search.setAuthor(AUTHOR);
        assertEquals(AUTHOR, search.getAuthor());
    }

    @Test
    void testGetDate() {
        assertEquals("temp", search.getDate());
    }

    @Test
    void testSetDate() {
        search.setDate(DATE);
        assertEquals(DATE, search.getDate());
    }

    @Test
    void testGetLocation() {
        assertEquals("temp", search.getLocation());
    }

    @Test
    void testSetLocation() {
        search.setLocation(LOCATION);
        assertEquals(LOCATION, search.getLocation());
    }

    @Test
    void testGetSource() {
        assertEquals("temp", search.getSource());
    }

    @Test
    void testSetSource() {
        search.setSource(SOURCE);
        assertEquals(SOURCE, search.getSource());
    }

    @Test
    void testGetLanguage() {
        assertEquals("temp", search.getLanguage());
    }

    @Test
    void testSetLanguage() {
        search.setLanguage(LANG);
        assertEquals(LANG, search.getLanguage());
    }

    @Test
    void testGetDateComparison() {
        assertEquals(DATEBEFORE, search.getDateComparison());
    }

    @Test
    void testSetDateComparison() {
        search.setDateComparison(DATECOMP);
        assertEquals(DATECOMP, search.getDateComparison());
        search.setDateComparison(DATEBEFORE);
        assertEquals(DATEBEFORE, search.getDateComparison());
        search.setDateComparison(DATEAFTER);
        assertEquals(DATEAFTER, search.getDateComparison());
        search.setDateComparison(DATEON);
        assertEquals(DATEON, search.getDateComparison());
    }

    @Test
    void testGetHashtags() {
        assertEquals(HASHTAGS, search.getHashtags());
    }

    @Test
    void testGetTag() {
        assertEquals(TAG, search.getTag());
    }

    @Test
    void testSetTag() {
        search.setTag(TAG2);
        assertEquals(TAG2, search.getTag());
    }

    @Test
    void testSearch() {
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        Article article1 = getArticle1();
        DAOFactory.getArticleDAO().create(article1);

        ArrayList<Article> expected_search = new ArrayList<>();
        expected_search.add(article1);

        Search search = new Search(TITLE_ARTICLE_1);
        search.setId(LocalUser.singleton.getUserId());
        assertEquals(expected_search, search.search());
    }
}
