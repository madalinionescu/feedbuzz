package be.ac.ulb.infof307.g01.java;

import be.ac.ulb.infof307.g01.java.model.builder.ArticleBuilder;
import be.ac.ulb.infof307.g01.java.model.Article;
import be.ac.ulb.infof307.g01.java.model.InformationRSS;
import be.ac.ulb.infof307.g01.java.model.Tag;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ConstantsForTests {
    public static final String PATH_DB_TEST = "./dbTest" ;

    //Constantes pour les articles
    public static final String TITLE_ARTICLE_1 = "Title", TITLE_ARTICLE_2 = "Title2";
    public static final String CONTENT_ARTICLE_1 = "Content", CONTENT_ARTICLE_2 = "Content2";
    public static final String AUTHOR_ARTICLE_1 = "Author", AUTHOR_ARTICLE_2 = "Author2";
    public static final String SOURCE_ARTICLE_1 = "Source", SOURCE_ARTICLE_2 = "Source2";
    public static final String LINK_ARTICLE_1 = "Link", LINK_ARTICLE_2 = "Link2";
    public static final String LOCATION_ARTICLE_1 = "location", LOCATION_ARTICLE_2 = "location2";
    public static final String DATE_ARTICLE_1 = "date", DATE_ARTICLE_2 = "date2";
    public static final List<String> HASHTAGS_ARTICLE_1 = new ArrayList<>(), HASHTAGS_ARTICLE_2 = new ArrayList<>();
    private static final String THUMBNAIL_ARTICLE_1 = "thumbnail";
    private static final String THUMBNAIL_ARTICLE_2 = "thumbnail2";
    public static final String RSS_LINK_ARTICLE_1 = "rsslink", RSS_LINK_ARTICLE_2 = "rsslink2";
    public static final String TAG_NAME_1 = "Tag1";
    public static final String TAG_NAME_2 = "Tag2";
    public static final String TAG_NAME_3 = "Tag3";
    public static final String TAG_NAME_4 = "Tag4";
    public static final Tag TAG1 = new Tag(TAG_NAME_1);
    public static final Tag TAG2 = new Tag(TAG_NAME_2);
    public static final Tag TAG3 = new Tag(TAG_NAME_3);
    public static final List<Tag> TAGS_ARTICLE_1 = new ArrayList<>(Arrays.asList(TAG1, TAG2, TAG3));
    public static final List<Tag> TAGS_ARTICLE_2 = new ArrayList<>(); //empty
    public static final String DIRECTORY_ARTICLE_1 = "directory", DIRECTORY_ARTICLE_2 = "directory2";
    public static final String FILTER_1 = "filter 1", FILTER_2 = "filter 2";
    public static final String LANGUAGE_1 = "EN", LANGUAGE_2 = "FR";
    private static final boolean TWEET_1 = true;
    private static final boolean TWEET_2 = false;
    public static final String HASHTAG_TWITTER_1 = "Sea", HASHTAG_TWITTER_2 = "";



    /**
     * @return un nouvel article avec les constantes 1. On fait cela pour éviter
     * de devoir créer à chaque fois un nouvel article en ayant 20 lignes à écrire.
     */
    public static Article getArticle1() {
        return new ArticleBuilder()
                .setTitle(TITLE_ARTICLE_1)
                .setContent(CONTENT_ARTICLE_1)
                .setAuthor(AUTHOR_ARTICLE_1)
                .setSource(SOURCE_ARTICLE_1)
                .setLink(LINK_ARTICLE_1)
                .setLocation(LOCATION_ARTICLE_1)
                .setDate(DATE_ARTICLE_1)
                .setHashtags(HASHTAGS_ARTICLE_1)
                .setThumbnail(THUMBNAIL_ARTICLE_1)
                .setRssLink(RSS_LINK_ARTICLE_1)
                .setTweet(TWEET_2)
                .setTwitterHashtag(HASHTAG_TWITTER_2)
                .setTags(TAGS_ARTICLE_1)
                .build();
    }

    /**
     * @return un nouvel article avec les constantes 1. On fait cela pour éviter
     * de devoir créer à chaque fois un nouvel article en ayant 20 lignes à écrire.
     */
    public static Article getArticle2() {
        return new ArticleBuilder()
                .setTitle(TITLE_ARTICLE_2)
                .setContent(CONTENT_ARTICLE_2)
                .setAuthor(AUTHOR_ARTICLE_2)
                .setSource(SOURCE_ARTICLE_2)
                .setLink(LINK_ARTICLE_2)
                .setLocation(LOCATION_ARTICLE_2)
                .setDate(DATE_ARTICLE_2)
                .setHashtags(HASHTAGS_ARTICLE_2)
                .setThumbnail(THUMBNAIL_ARTICLE_2)
                .setRssLink(RSS_LINK_ARTICLE_2)
                .setTweet(TWEET_2)
                .setTwitterHashtag(HASHTAG_TWITTER_2)
                .setTags(TAGS_ARTICLE_2)
                .build();
    }

    public static Article getArticle3() {
        return new ArticleBuilder()
                .setTitle(TITLE_ARTICLE_1)
                .setContent(CONTENT_ARTICLE_1)
                .setAuthor(AUTHOR_ARTICLE_1)
                .setSource(SOURCE_ARTICLE_1)
                .setLink(LINK_ARTICLE_1)
                .setLocation(LOCATION_ARTICLE_1)
                .setDate(DATE_ARTICLE_1)
                .setHashtags(HASHTAGS_ARTICLE_1)
                .setThumbnail(THUMBNAIL_ARTICLE_1)
                .setRssLink(RSS_LINK_ARTICLE_1)
                .setTweet(TWEET_1)
                .setTwitterHashtag(HASHTAG_TWITTER_1)
                .setTags(TAGS_ARTICLE_1)
                .build();
    }

    //Constantes pour User
    public static final String USERNAME_USER_1 = "User", USERNAME_USER_2 = "User2";
    public static final String PASSWORD_USER_1 = "Password@1", PASSWORD_USER_2 = "Password@2";
    public static final String BAD_USERNAME = "User3", BAD_PASSWORD = "Password@3";
    public static final String MAIL_USER_1 = "email1@gmail.com", MAIL_USER_2 = "email2@gmail.com";


    // Constantes pour Interests
    private static final String VALID_URL_1 = "http://feeds.bbci.co.uk/news/science_and_environment/rss.xml?edition=uk"; //science
    public static final String VALID_URL_2 = "https://www.sciencedaily.com/rss/top/science.xml"; //science
    private static final String VALID_URL_3 = "https://www.lalibre.be/rss.xml";                   //news

    public static final InformationRSS RSS1 = new InformationRSS(VALID_URL_1, 1, false, false, false);
    public static final InformationRSS RSS2 = new InformationRSS(VALID_URL_2, 1, false, false, false);
    public static final InformationRSS RSS3 = new InformationRSS(VALID_URL_3, 1, false, false, false);

    public static final ArrayList<String> RELATED_RSS_1 = new ArrayList<>(Collections.singletonList(VALID_URL_1));
    public static final ArrayList<String> RELATED_RSS_2 = new ArrayList<>(Collections.singletonList(VALID_URL_3));

    public static final ArrayList<Tag> RELATED_TAGS_1 = new ArrayList<>(Arrays.asList(TAG1, TAG2));
    public static final ArrayList<Tag> RELATED_TAGS_2 = new ArrayList<>(Collections.singletonList(TAG3));

    public static final String INTEREST_NAME_1 = "News";
    public static final String INTEREST_NAME_2 = "Science";
    public static final String BAD_INTEREST = "BadInterest";


    public  static final ArrayList<String> EMPTY_INTEREST_LIST = new ArrayList<>();


    //Constante pour test DirectoryDAO

    public static final String GRAND_FATHER = "grandFather";
    public static final String FATHER = "father";
    public static final String SON = "son";
    public static final String NULL_DIRECTORY = "null dir";
    public static final String SISTER = "sister";
    public static final String ARTICLE_NEWS = "news";

}