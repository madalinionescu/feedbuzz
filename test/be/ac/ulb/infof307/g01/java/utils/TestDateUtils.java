package be.ac.ulb.infof307.g01.java.utils;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestDateUtils {
    private static final String DATE_WRONG_FORMAT = "wrong";

    private static final String DATE_STRING_1 = "Sun, 28 Apr 2019 16:29:46 GMT"; //BBC
    private static final String DATE_STRING_2 = "Sun, 28 Apr 2019 18:39:44 +0100"; //LaLibre
    private static final String DATE_STRING_3 = "Sun, 28 Apr 2019 12:23:02 EDT"; //ScienceDaily
    private static final String DATE_STRING_4 = "Sun, 28 Apr 2019 16:33:36 +0000"; //ScienceDaily

    private static final Date defaultDate = new Date(); //heure actuelle

    @Test
    void testStringToDate() {
        //Test mauvais format, donc renvoie la date actuelle. Attention, il se peut que defaultDate soit 1 seconde
        //plus ancien que le deuxième membre. Dans ce cas, il faut relancer le test. Cela est dû au décalage
        //en milliseconds qu'il y entre la création de defaultDate et celle du second membre.
        assertEquals(defaultDate.toString(), DateUtils.stringToDate(DATE_WRONG_FORMAT).toString());

        //Attention pour les tests suivant, le PC doit être sur l'heure d'été de Bruxelles pour fonctionner
        assertEquals("Sun Apr 28 18:29:46 CEST 2019", DateUtils.stringToDate(DATE_STRING_1).toString()); //+2h que GMT
        assertEquals("Sun Apr 28 19:39:44 CEST 2019", DateUtils.stringToDate(DATE_STRING_2).toString()); //+1h que 0100
        assertEquals("Sun Apr 28 18:23:02 CEST 2019", DateUtils.stringToDate(DATE_STRING_3).toString()); //+2h que EDT
        assertEquals("Sun Apr 28 18:33:36 CEST 2019", DateUtils.stringToDate(DATE_STRING_4).toString()); //+2h que 0000
    }
}