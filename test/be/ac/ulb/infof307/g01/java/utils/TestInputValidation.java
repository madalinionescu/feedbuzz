package be.ac.ulb.infof307.g01.java.utils;

import be.ac.ulb.infof307.g01.java.model.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TestInputValidation {
    private static final String CORRECT_EMAIL_1="test@ulb.ac.be", CORRECT_EMAIL_2="a_b.c@yahoo.fr",
            INCORRECT_EMAIL_1="test@ulb", INCORRECT_EMAIL_2="te@st@ulb.ac.be",
            INCORRECT_EMAIL_3="te st@ulb.ac.be", INCORRECT_EMAIL_4="test@ulb ac.be",
            INCORRECT_EMAIL_5="test@.ulb.ac.be";

    private static final String CORRECT_PASSWORD_1="AaBb12@#", PASS_MISSING_DIGIT="AaBb1234",
            PASS_TOO_SHORT="AB1@", PASS_MISSING_CAPITAL="abcd12@#",
            PASS_TOO_LONG="ABCDabcd12345!@#$", PASS_MISSING_LOWERCASE="ABCD12@#",
            PASS_MISSING_SYMBOL="AaBb1234";

    private static final String PASSWORD1_VALID="AaBb12@#",
                    PASSWORD1_INVALID="Aurevoir";

    @Test
    void testEmailValidation() {
        assertTrue(User.validateEmail(CORRECT_EMAIL_2));
        assertFalse(User.validateEmail(INCORRECT_EMAIL_1));
        assertFalse(User.validateEmail(INCORRECT_EMAIL_2));
        assertFalse(User.validateEmail(INCORRECT_EMAIL_3));
        assertFalse(User.validateEmail(INCORRECT_EMAIL_4));
        assertFalse(User.validateEmail(INCORRECT_EMAIL_5));
    }

    @Test
    void testPasswordValidation() {
        assertTrue(User.validatePassword(CORRECT_PASSWORD_1));
        assertFalse(User.validatePassword(PASS_MISSING_CAPITAL));
        assertFalse(User.validatePassword(PASS_MISSING_DIGIT));
        assertFalse(User.validatePassword(PASS_MISSING_LOWERCASE));
        assertFalse(User.validatePassword(PASS_MISSING_SYMBOL));
        assertFalse(User.validatePassword(PASS_TOO_LONG));
        assertFalse(User.validatePassword(PASS_TOO_SHORT));
    }

    @Test
    void testPasswordDiffUsername() {
        assertFalse(User.validatePassword(CORRECT_PASSWORD_1,CORRECT_PASSWORD_1));
        assertTrue(User.validatePassword(CORRECT_EMAIL_1,CORRECT_PASSWORD_1));
    }

    @Test
    void testPasswordConfirmation() {
        assertTrue(User.confirmPassword(CORRECT_PASSWORD_1, PASSWORD1_VALID));
        assertFalse(User.confirmPassword(CORRECT_PASSWORD_1, PASSWORD1_INVALID));
    }
}
