package be.ac.ulb.infof307.g01.java.utils;

import be.ac.ulb.infof307.g01.java.utils.TwitterShare;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestTwitterShare {

    private static final String ARTICLE_TITLE = "hdfghfdh";
    private static final String ARTICLE_LINK = "https://hghdfghdfg.com";
    private static final String NETWORK_NAME = "Twitter";
    private final TwitterShare twitterShare = new TwitterShare(ARTICLE_TITLE, ARTICLE_LINK);

    @Test
    void testGetArticleLink() {
        assertEquals(twitterShare.getArticleLink(), ARTICLE_LINK);
    }

    @Test
    void testGetArticleTitle() {
        assertEquals(twitterShare.getArticleTitle(), ARTICLE_TITLE);
    }

    @Test
    void testGetNetworkName() {
        assertEquals(twitterShare.getNetworkName(),NETWORK_NAME);
    }
}