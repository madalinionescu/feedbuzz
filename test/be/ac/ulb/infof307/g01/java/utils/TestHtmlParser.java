package be.ac.ulb.infof307.g01.java.utils;

import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.*;

class TestHtmlParser {

    @Test
    void testAbsoluteLink() throws MalformedURLException {
        URL urlA = new URL("http://www.article27.be/gdfgdsfgsdfgsdfg");
        URL urlB = new URL("http://www.article27.be/gdfgdsfgsdfgsdfg/");
        URL urlC = null;


        String image = "/squelettes/img/linktosite-wall.png";
        String result = "http://www.article27.be/squelettes/img/linktosite-wall.png";
        assertEquals(HtmlParser.absoluteLink(image,urlA), result);
        String imageC = "";
        assertEquals(HtmlParser.absoluteLink(imageC,urlA), imageC);

        assertEquals(HtmlParser.absoluteLink(image,urlB), result);
        assertEquals(HtmlParser.absoluteLink(imageC,urlB), imageC);

        assertEquals(HtmlParser.absoluteLink(image,urlC), imageC);
        assertEquals(HtmlParser.absoluteLink(imageC,urlC), imageC);
    }
}