package be.ac.ulb.infof307.g01.java.utils;

import be.ac.ulb.infof307.g01.java.utils.FacebookShare;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestFacebookShare {

    private static final String ARTICLE_TITLE = "gfgfdsggdsfg";
    private static final String ARTICLE_LINK = "https://gdfgdfgdgdfgd.com";
    private static final String NETWORK_NAME = "Facebook";
    private final FacebookShare facebookShare = new FacebookShare(ARTICLE_TITLE, ARTICLE_LINK);


    @Test
    void testGetArticleLink() {
        assertEquals(facebookShare.getArticleLink(), ARTICLE_LINK);
    }

    @Test
    void testGetArticleTitle() {
        assertEquals(facebookShare.getArticleTitle(), ARTICLE_TITLE);
    }

    @Test
    void testGetNetworkName() {
        assertEquals(facebookShare.getNetworkName(),NETWORK_NAME);
    }
}