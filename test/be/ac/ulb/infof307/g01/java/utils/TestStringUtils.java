package be.ac.ulb.infof307.g01.java.utils;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class TestStringUtils {
    private static final String WORD1 = "Bonjour";
    private static final String WORD2 = "Bonsoir";
    private static final String WORD3 = "Aurevoir";
    private static final ArrayList<String> LIST_WORDS = new ArrayList<>(Arrays.asList(WORD1, WORD2, WORD3));
    private static final ArrayList<String> LIST_WORDS2 = new ArrayList<>(Collections.singletonList(WORD1));
    private static final String[] LIST_WORDS3 = {WORD1, WORD2, WORD3};
    private static final String SENTENCE = "Bonjour Bonsoir Aurevoir Bonjour Aurevor bonsoir Bonsoir Salut Test Bonj aurevoir";

    @Test
    void testCountKeywordOccurences() {
        assertEquals(5, StringUtils.countKeywordOccurences(LIST_WORDS3, SENTENCE));
        assertEquals(5, StringUtils.countKeywordOccurences(LIST_WORDS, SENTENCE));
        assertEquals(2, StringUtils.countKeywordOccurences(LIST_WORDS2, SENTENCE));
    }
}