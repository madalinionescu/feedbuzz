package be.ac.ulb.infof307.g01.java.utils;

import be.ac.ulb.infof307.g01.java.model.dao.DAOFactory;
import be.ac.ulb.infof307.g01.java.model.InformationTwitter;
import be.ac.ulb.infof307.g01.java.model.LocalUser;
import be.ac.ulb.infof307.g01.java.model.ParserTwitter;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static be.ac.ulb.infof307.g01.java.ConstantsForTests.*;
import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestParserTwitter {
    private static final String HASHTAG_NAME_1 = "ULB";
    private static final int ARTICLES_COUNT_1 = 3;
    private static final String HASHTAG_TYPE_1 = "hashtag";

    private static final InformationTwitter InformationTwitter1 =
                                                    new InformationTwitter(HASHTAG_NAME_1,ARTICLES_COUNT_1, HASHTAG_TYPE_1);

    @BeforeAll
    void setUp(){
        DAOFactory.changeDatabasePath(PATH_DB_TEST );
        DAOFactory.clearAll();
        LocalUser.singleton.create(USERNAME_USER_1, PASSWORD_USER_1,  MAIL_USER_1);
    }

    @Test
    void testGetInfoTwitterHashtag() {
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);
        ParserTwitter parserTwitter = new ParserTwitter(InformationTwitter1);
        assertEquals(InformationTwitter1,parserTwitter.getInfoTwitter());
    }

    @Test
    void testGetTweetsList() {
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);

        ParserTwitter parserTwitter = new ParserTwitter(InformationTwitter1);
        assertFalse(parserTwitter.getTweetsList().isEmpty());
        assertEquals(3, parserTwitter.getTweetsList().size());
    }
}