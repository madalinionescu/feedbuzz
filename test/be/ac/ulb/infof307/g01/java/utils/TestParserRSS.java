package be.ac.ulb.infof307.g01.java.utils;

import be.ac.ulb.infof307.g01.java.model.dao.DAOFactory;
import be.ac.ulb.infof307.g01.java.model.InformationRSS;
import be.ac.ulb.infof307.g01.java.model.LocalUser;
import be.ac.ulb.infof307.g01.java.model.ParserRSS;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static be.ac.ulb.infof307.g01.java.ConstantsForTests.*;
import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestParserRSS {
    private static final String VALID_LINK = "http://feeds.bbci.co.uk/news/rss.xml";
    private static final String INVALID_LINK = "http://blabla.com";

    private static final InformationRSS VALID_INFO_RSS = new InformationRSS(VALID_LINK, 3, false, false, false);
    private static final InformationRSS INVALID_INFO_RSS = new InformationRSS(INVALID_LINK, 3, false, false, false);

    @BeforeAll
    void setUp(){
        DAOFactory.changeDatabasePath(PATH_DB_TEST );
        DAOFactory.clearAll();
        LocalUser.singleton.create(USERNAME_USER_1, PASSWORD_USER_1,  MAIL_USER_1);
    }

    @Test
    void testGetArticles() {
        LocalUser.singleton.connect(USERNAME_USER_1, PASSWORD_USER_1);

        ParserRSS parserRSS = new ParserRSS(VALID_INFO_RSS, false);
        assertFalse(parserRSS.getArticles().isEmpty());
        assertEquals(3, parserRSS.getArticles().size());

        ParserRSS parserRSS2 = new ParserRSS(INVALID_INFO_RSS, false);
        assertTrue(parserRSS2.getArticles().isEmpty());
    }
}