package be.ac.ulb.infof307.g01.java.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;

import static be.ac.ulb.infof307.g01.java.utils.FileUtils.fileExists;
import static be.ac.ulb.infof307.g01.java.utils.FileUtils.writeInputStreamToFile;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestFileUtils {
    private static final String PATH1 = "test/file1.txt";
    private static final String PATH2 = "test/file2.txt";

    private static final String STRING1 = "String1";
    private static final String STRING2 = "String2";

    private static final InputStream INPUT_STREAM1 = new ByteArrayInputStream(STRING1.getBytes());
    private static final InputStream INPUT_STREAM2 = new ByteArrayInputStream(STRING2.getBytes());


    @BeforeEach
    void setUp(){
        File filePath = new File(PATH1);
        filePath.delete();
        filePath = new File(PATH2);
        filePath.delete();
    }

    @Test
    void testWriteInputStreamToFile() {
        assertFalse(fileExists(PATH1));
        assertFalse(fileExists(PATH2));

        writeInputStreamToFile(PATH1, INPUT_STREAM1);
        writeInputStreamToFile(PATH2, INPUT_STREAM2);

        assertTrue(fileExists(PATH1));
        assertTrue(fileExists(PATH2));
    }

    @Test
    void testFileExists() {
        assertFalse(fileExists(PATH1));
        assertFalse(fileExists(PATH2));

        writeInputStreamToFile(PATH1, INPUT_STREAM1);
        writeInputStreamToFile(PATH2, INPUT_STREAM2);

        assertTrue(fileExists(PATH1));
        assertTrue(fileExists(PATH2));
    }
}