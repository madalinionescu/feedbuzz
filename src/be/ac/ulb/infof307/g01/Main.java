package be.ac.ulb.infof307.g01;

import be.ac.ulb.infof307.g01.java.controller.MainController;
import be.ac.ulb.infof307.g01.java.view.window.LauncherWindow;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        MainController listener = new MainController();
        LauncherWindow launcher = new LauncherWindow(listener);
        launcher.display();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
