package be.ac.ulb.infof307.g01.java.controller;

public class ControllerConstants {
    // INTERESTS
    public static final String DEFAULT_INTERESTS_PATH = "/txt/defaultInterests.txt";

    // WEBSITES
    public static final String NEWS_BBC_WEBSITE = "bbc.com";
    public static final String TECHNOLOGY_TECHRADAR_WEBSITE = "techradar.com";
    public static final String NEWS_LALIBRE_WEBSITE = "lalibre.be";
    public static final String SCIENCE_SCIENCEDAILY_WEBSITE = "sciencedaily.com";
    public static final String NEWS_LESOIR_WEBSITE = "lesoir.be";
    public static final String NEWS_HACKADAY_WEBSITE = "hackaday.com";


    // RSS LINKS
    public static final String NEWS_BBC = "http://feeds.bbci.co.uk/news/rss.xml";
    public static final String TECHNOLOGY_TECHRADAR = "https://www.techradar.com/rss";
    public static final String NEWS_LALIBRE = "https://www.lalibre.be/rss.xml";
    public static final String SCIENCE_SCIENCEDAILY = "https://www.sciencedaily.com/rss/top/science.xml";
    public static final String NEWS_LESOIR = "https://www.lesoir.be/rss/81853/cible_principale_gratuit";
    public static final String NEWS_HACKADAY = "https://hackaday.com/blog/feed/";

    // DEFAULT NUMBERS
    public static final int DEFAULT_NUMBER_ARTICLES = 5;
}
