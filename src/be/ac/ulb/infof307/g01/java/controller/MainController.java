package be.ac.ulb.infof307.g01.java.controller;

import be.ac.ulb.infof307.g01.java.model.dao.ArticleDAO;
import be.ac.ulb.infof307.g01.java.model.dao.DAOFactory;
import be.ac.ulb.infof307.g01.java.exception.UserExistsException;
import be.ac.ulb.infof307.g01.java.exception.UserNotFoundException;
import be.ac.ulb.infof307.g01.java.model.*;
import be.ac.ulb.infof307.g01.java.view.listener.MainListener;
import be.ac.ulb.infof307.g01.java.view.window.*;

import java.io.IOException;
import java.net.URL;
import java.util.*;

import static be.ac.ulb.infof307.g01.java.controller.ControllerConstants.DEFAULT_INTERESTS_PATH;
import static be.ac.ulb.infof307.g01.java.controller.ControllerConstants.DEFAULT_NUMBER_ARTICLES;
import static be.ac.ulb.infof307.g01.java.Constants.TAG_NAME_RECOMMENDED;

/**
 * The main controller is a intermediate component
 * between the view controllers and the DAO
 */
public class MainController implements MainListener {
    private static final int MAX_LOGIN_ATTEMPTS = 3;
    private static final int LOCKOUT_PERIOD = 3;

    /**
     * @param username pseudo entré par l'user
     * @param password mdp entré par l'user
     * @throws UserNotFoundException user pas trouvé ou mauvais mdp
     */
    @Override
    public void signIn(String username, String password) throws UserNotFoundException {
        User user = DAOFactory.getUserDAO().findById(username);
        if (user == null) throw new UserNotFoundException();
        Date date = new Date();
        if (date.before(user.getLockedUntil())) throw new UserNotFoundException();

        if (LocalUser.singleton.connect(username, password)) {
            showDashboard();
            showDefaultInterest();
        } else{
            wrongPassword(user);
        }
    }

    /**
     * Gère le cas d'un mauvais password entré (ou mauvais user)
     * @param user user qui a essayé de se connecter
     * @throws UserNotFoundException si l'user s'est trompé MAXLOGINATTEMPTS fois
     */
    private void wrongPassword(User user) throws UserNotFoundException {
        int loginCount = user.getLoginCount();
        if (loginCount < MAX_LOGIN_ATTEMPTS) {
            user.setLoginCount(loginCount + 1);
        } else {
            Date currentDate = new Date();
            Date lockoutDate = new Date(currentDate.getTime() + (LOCKOUT_PERIOD * 60 * 1000));
            user.setLoginCount(0);
            user.setLockedUntil(lockoutDate);
        }
        DAOFactory.getUserDAO().update(user);
        throw new UserNotFoundException();
    }

    /**
     * Show le Dashboard après la connexion réussie
     */
    private void showDashboard(){
        try {
            DashboardWindow dashboard = new DashboardWindow(this);
            dashboard.display();
        } catch (Exception e) {
            // fail to find app files -> close the app
            showErrorWindowMissingFile();
        }
    }

    /**
     * Si l'user a pas encore d'intérêts, on lui propose quelques defaults
     */
    private void showDefaultInterest(){
        try {
            setupDefaultInterestsDB();
            if (LocalUser.singleton.getUser().getUserInterests() == null) {
                InterestsWindow interests = new InterestsWindow(this);
                interests.display();
            }
        } catch (Exception e) {
            //
        }
    }

    private void showErrorWindowMissingFile() {
        ErrorWindow window = new ErrorWindow();
        window.display("Error", "File not found", "File is missing, please reinstall");
    }

    /**
     * Crée l'utilisateur demandé en vérifiant que le pseudo ne soit pas déjà pris
     * @param username username entré
     * @param email email entrée
     * @param password mdp entré
     * @throws UserExistsException si user existe déjà
     */
    @Override
    public void signUp(String username, String email, String password) throws UserExistsException {
        if (!LocalUser.singleton.create(username, password, email)) {
            throw new UserExistsException();
        }
    }

    /**
     * Déconnexion d'un utilisateur connecté et retour à la Launcherwindow
     */
    @Override
    public void signOut() {
        LocalUser.singleton.disconnect();
        // closes everything and open back the login window
        try {
            LauncherWindow launcher = new LauncherWindow(this);
            launcher.display();
        } catch (Exception e) {
            ErrorWindow error = new ErrorWindow();
            error.display("Error","An error occurred while signing out","Please restart the application");
        }
    }

    @Override
    public boolean validatePassword(String username, String password) {
        return User.validatePassword(username, password);
    }

    @Override
    public boolean validateEmail(String email) {
        return User.validateEmail(email);
    }

    @Override
    public boolean confirmPassword(String password, String confirmation) {
        return User.confirmPassword(password, confirmation);
    }

    @Override
    public void displayTermCond() {
        new TermCondWindow();
        try {
            TermCondWindow.display();
        } catch (IOException e) {
            showErrorWindowMissingFile();
        }
    }

    /**
     * Ajoute l'interet choisi par l'user à son compte
     * @param interestName interet choisi par l'user
     */
    @Override
    public void setInterest(String interestName) {
        Interest interest = DAOFactory.getInterestDAO().findByString(interestName);
        User usr = LocalUser.singleton.getUser();
        usr.addUserInterest(interest.getId());
        DAOFactory.getUserDAO().update(usr);
    }



    @Override
    public ArrayList<Article> getArticles() {
        List<Article> articles = DAOFactory.getArticleDAO().findAll();
        return (ArrayList<Article>) articles;
    }

    /**
     * @return les articles supprimés précédemment par l'user
     */
    @Override
    public List<Article> getRecycleBinArticles() {
        return DAOFactory.getArticleDAO().findAll(true);
    }

    @Override
    public void addToSearchHistory(String search) {
        User usr = LocalUser.singleton.getUser();
        usr.addSearchHistory(search);
        DAOFactory.getUserDAO().update(usr);
    }

    @Override
    public ArrayList<String> getSearchHistory() {
        return LocalUser.singleton.getUser().getSearchHistory();
    }

    /**
     * créer la base de donnée des centres d'intéret par défaut
     * (ceux proposé dans la fenêtre popup)
     */
    @Override
    public void setupDefaultInterestsDB() {
        URL url = this.getClass().getResource(DEFAULT_INTERESTS_PATH);
        Scanner scanner;
        try {
            scanner = new Scanner(url.openStream());
        } catch (IOException e) {
            WarningWindow warning = new WarningWindow();
            warning.display("Warning","Cannot read/open the file",
                    "Interests file cannot be read/openned");
            scanner = null;
        }
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] parameters = line.split("\\|");
            String name = parameters[0];
            ArrayList<String> links = new ArrayList<>(Arrays.asList(parameters[1].split(";")));
            ArrayList<Tag> tags = new ArrayList<>();
            for(String tag: parameters[2].split(";")){
                tags.add(new Tag(tag));
            }
            Interest interest = new Interest(name,links,tags);
            DAOFactory.getInterestDAO().create(interest);

        }
    }

    @Override
    public void deleteArticle(Article article){
        if (!(article ==null)) {
            article.setTrash(true);
            DAOFactory.getArticleDAO().update(article);
        }
    }

    @Override
    public Article getArticle(String title) {
        return DAOFactory.getArticleDAO().findByName(title);
    }

    @Override
    public void deleteArticleFromTrash(Article article) {
        DAOFactory.getArticleDAO().delete(article);
    }


    @Override
    public void changeArticlePath(Directory parent, Article article) {
        DAOFactory.getDirectoryDAO().changeNodePath(parent, article);
    }

    /**
     * Travail effectué dans un Thread qui va lire les différents feeds
     * auxquels l'user est abonné et va les parser pour avoir les articles.
     */
    @Override
    public void refreshRSSInBackground(){
        List<InformationRSS> links = DAOFactory.getInformationRSSDAO().findAll();
        for (InformationRSS link : links) {
            ParserRSS parserRSS = new ParserRSS(link, true);
            for (Article currentArticle : parserRSS.getArticles()){
                DAOFactory.getArticleDAO().createOrUpdate(currentArticle);
            }
        }
    }

    /**
     * Travail effectué dans un Thread qui va lire les différents hashtags Twitter
     * auxquels l'user est abonné et va les parser pour avoir les tweets.
     */
    @Override
    public void refreshTwitterInBackground() {
        List<InformationTwitter> informationsTwitter = DAOFactory.getInformationTwitterDAO().findAll();
        for (InformationTwitter informationTwitter : informationsTwitter) {
            ParserTwitter parserTwitter = new ParserTwitter(informationTwitter);
            for (Article currentArticle : parserTwitter.getTweetsList()){
                DAOFactory.getArticleDAO().createOrUpdate(currentArticle);
            }
        }
    }

    /**
     * Marque l'article en reading si l'ouvre dans la webview ou en finished
     * si l'utilisateur a fini de le lire
     * @param article article selectionnée
     * @param reading si on passe l'article en reading ou en finished
     */
    @Override
    public void updateReadingStatus(Article article, boolean reading) {
        if (reading) {
            DAOFactory.getArticleDAO().tagAsReading(article);
        }
        else {
            DAOFactory.getArticleDAO().tagAsFinished(article);
        }
    }

    @Override
    public void markAsRecommended(Article article, boolean status){
        if (status)
            DAOFactory.getArticleDAO().tagAsRecommended(article);
        else
            article.removeTag(TAG_NAME_RECOMMENDED);
            DAOFactory.getArticleDAO().update(article);
    }

    @Override
    public void updateInformationRSS(ParserRSS parser) {
        DAOFactory.getInformationRSSDAO().update(parser.getInformationRSS());
    }

    @Override
    public List<InformationRSS> getInformationRSS() {
        return DAOFactory.getInformationRSSDAO().findAll();
    }

    /**
     * Supprime un feed auquel l'user est abonné et tous les articles liés à ce feed
     * @param infoRssLink feed à supprimer
     */
    @Override
    public void deleteInformationRSS(String infoRssLink) {
        List<InformationRSS> list = getInformationRSS();
        for (InformationRSS info : list){
            if (info.getLink().equals(infoRssLink)){
                DAOFactory.getInformationRSSDAO().delete(info);
                break;
            }
        }

        ArticleDAO.deleteArticlesFromFeed(infoRssLink);
    }

    /**
     * Update (ou ajoute) les article parsés à la DB
     * @param parser contenant les articles parsés
     */
    @Override
    public void updateRSSArticles(ParserRSS parser) {
        List<Article> articlesList = parser.getArticles();
        for (Article article : articlesList) {
            DAOFactory.getArticleDAO().update(article);
        }
    }

    /**
     * Fonction qui obtient l'interet qui est definit par un rss donnée.
     * Cet interet est cherché dans la base de données.
     * @param link : link d'un rss donné
     * @return l'objet de type Interest qui est trouvé dans la base de données.
     *         Renvoie null si le link ne definit aucun interet
     */
    @Override
    public Interest getInterest(String link) {
        ArrayList<Interest> interests = new ArrayList(DAOFactory.getInterestDAO().findAll());
        for(Interest interest:interests){
            for(String rssLink: interest.getRelatedRSSLinks()){
                if (rssLink.equals(link)){
                    return interest;
                }
            }
        }
        return null;
    }

    /**
     * Fonction qui ajoute un interet dans la base de données de l'utilisateur
     * @param interest un ubjet de type Interest
     */
    @Override
    public void addInterest(Interest interest) {
        if(interest!=null){
            User usr = LocalUser.singleton.getUser();
            usr.addUserInterest(interest.getId());
            DAOFactory.getUserDAO().update(usr);
        }
    }

    @Override
    public void updateArticle (Article article){
        DAOFactory.getArticleDAO().update(article);
    }

    @Override
    public void updateUser(User user){DAOFactory.getUserDAO().update(user);}

    /**
     * Fonction qui aide à créer ou à mettre à jour un article dans la base de donneés
     * @param informationRSS: est objet de type InformationRSS
     */
    @Override
    public void createIfNotExist(InformationRSS informationRSS) {
        DAOFactory.getInformationRSSDAO().createIfNotExist(informationRSS);
    }

    /**
     * Ajoute un feed RSS à ceux du user
     * @param link lien d'un feed RSS à suivre
     */
    @Override
    public void addRSS(String link) {
        if (link.equals("")) return;
        InformationRSS informationRSS = new InformationRSS(
                link,
                DEFAULT_NUMBER_ARTICLES,
                true,
                true,
                true);
        createIfNotExist(informationRSS);
    }

    @Override
    public void createDirectory(String dirName, Directory father) {
        //Directory father = getDirectory(parentDirName);
        Directory newDir = new Directory(dirName);
        DAOFactory.getDirectoryDAO().create(newDir);
        father.addNode(newDir);
        DAOFactory.getDirectoryDAO().update(father);
    }

    @Override
    public void createRootDirectory(Directory root){
        DAOFactory.getDirectoryDAO().createIfNotExists(root);
    }

    @Override
    public void deleteDirectory(String directoryID) {
        DAOFactory.getDirectoryDAO().deleteDirectory(directoryID);
    }


    @Override
    public Directory getDirectory(String dirName){
        return DAOFactory.getDirectoryDAO().findByName(dirName);
    }

    @Override
    public void saveInformationRSS(InformationRSS infRss) {
        DAOFactory.getInformationRSSDAO().createIfNotExist(infRss);
    }

    @Override
    public List<String> getTags(Article article) {
        return DAOFactory.getArticleDAO().getTagsAsStrings(article);
    }

    @Override
    public List<SearchFilter> getSearchFilters() {
        return DAOFactory.getSearchFilterDAO().findAll();
    }

    @Override
    public void addSearchFilter(SearchFilter searchFilter) {
        DAOFactory.getSearchFilterDAO().create(searchFilter);
    }

    @Override
    public SearchFilter getSearchFilterByName(String name) {
        return DAOFactory.getSearchFilterDAO().findByString(name);
    }


    /**
     * Fonction qui obtient une liste de tous les hashtags auxquels l'utilisateur est abonné
     * et qui sont stoqués dans la base de donées
     * @return
     */
    @Override
    public List<InformationTwitter> getInformationTwitterHashtags() {
        return DAOFactory.getInformationTwitterDAO().getHashtags();
    }

    /**
     * Fonction qui obtient une liste de tous les hashtags auxquels l'utilisateur est abonné
     * et qui sont stoqués dans la base de donées
     * @return
     */
    @Override
    public List<InformationTwitter> getInformationTwitterAccounts() {
        return DAOFactory.getInformationTwitterDAO().getAccounts();
    }

    /**
     * Fonction qui enregistre un hashtag dans la base de données
     * @param informationTwitter
     */
    @Override
    public void saveInformationTwitter(InformationTwitter informationTwitter) {
        DAOFactory.getInformationTwitterDAO().createIfNotExist(informationTwitter);
    }

    /**
     * Finction qui supprime les informations de la base de données pour un hashtag donné.
     * Elle supprime aussi tous les tweets liés à ce hashtag.
     * @param hashtagName: le nom du hashtag (en String)
     */
    @Override
    public void deleteTwitterHashtag(String hashtagName) {
            List<InformationTwitter> list = getInformationTwitterHashtags();
            for (InformationTwitter info : list){
                if (info.getName().equals(hashtagName)) {
                    DAOFactory.getInformationTwitterDAO().delete(info);
                    break;
                }
            }

            ArticleDAO.deleteArticlesFromFeed(hashtagName);
    }


    @Override
    public void deleteFilter(String name) {
        List<SearchFilter> filterList = DAOFactory.getSearchFilterDAO().findAll();
        for (SearchFilter filter : filterList) {
            if (filter.getName().equals(name)) {
                DAOFactory.getSearchFilterDAO().delete(filter);
            }
        }
    }

    /**
     * Finction qui supprime les informations de la base de données pour un hashtag donné.
     * Elle supprime aussi tous les tweets liés à ce hashtag.
     * @param accountName: le nom du hashtag (en String)
     */
    @Override
    public void deleteTwitterAccount(String accountName) {
        List<InformationTwitter> list = getInformationTwitterAccounts();
        for (InformationTwitter info : list){
            if (info.getName().equals(accountName)){
                DAOFactory.getInformationTwitterDAO().delete(info);
                break;
            }
        }

        ArticleDAO.deleteArticlesFromFeed(accountName);
    }

}
