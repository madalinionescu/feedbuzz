package be.ac.ulb.infof307.g01.java.utils;

/**
 * Class qui permet de partager des articles sur facebook
 */
public class FacebookShare extends NetworkShare {
    private static final String NETWORK_NAME = "Facebook";
    private static final String FORMAT_SHARE = "https://www.facebook.com/sharer/sharer.php?u=%s";

    /**
     * Constructor
     * @param articleTitle title
     * @param articleLink link
     */
    public FacebookShare(String articleTitle, String articleLink) {
        super(articleTitle, articleLink);
    }

    @Override
    public String getNetworkName() {
        return NETWORK_NAME;
    }

    @Override
    public String getShareLink() {
        return String.format(FORMAT_SHARE, articleLink);
    }
}
