package be.ac.ulb.infof307.g01.java.utils;

import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

import java.util.concurrent.CountDownLatch;

/**
 * Classe représentant un thread permettant de faire une tâche
 * en parallèle à une autre (tel que le chargement de l'importation
 * d'articles)
 * Permet de mettre à jour javafx avec un thread qui ne fait
 * pas partie de ceux intégrés à celui-ci.
 */
public abstract class ThreadJavaFx {

    /**
     * Le travail en arrière plan (le thread) pour une
     * certaine tâche
     */
    protected abstract void backgroundWork();

    /**
     * Travail final effectué après avoir fini le background work
     */
    protected abstract void finalWork();

    /**
     * Lance le final work du thread dans javafx
     * @throws InterruptedException
     */
    private void finalWorkLatch() throws InterruptedException{
        final CountDownLatch latch = new CountDownLatch(1);
        Platform.runLater(() -> {
            try{
                finalWork();
            } catch (Exception e) {
                //Ne rien faire, problème de thread. On rentre qd mm dans le finally
            } finally{
                latch.countDown();
            }
        });
        latch.await();
    }

    /**
     * Lance le thread dédié à une certaine tâche (par
     * exemple, le chargement lors de l'importation
     * d'articles)
     */
    public void run() {
        Service<Void> service = new Service<Void>() {
            @Override
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        backgroundWork();  //Background work
                        finalWorkLatch();       // final work
                        return null;
                    }
                };
            }
        };
        service.start();
    }

}
