package be.ac.ulb.infof307.g01.java.utils;

import be.ac.ulb.infof307.g01.java.view.window.ErrorWindow;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * HtmlParser est une classe qui va convertir et parser une fichier
 * html pour y introduire un maximum de code source. HtmlParser va
 * importer tout le css directement dans le code et va convertir  dans les
 * balises images la source en code de base64 directement intégré.
 *
 * Permet de pouvoir sauvegarder une site avec son contenue css et la plus
 * part des images dans un seul et unique String.
 *
 * Le résultat peut être montré dans une webEngine.
 *
 * Example:
 * webEngine.loadContent(sourceHtml);
 */
public class HtmlParser {

    private static final String CHARSET_NAME = "UTF-8";

    /**
     * Récupère le code html d'un url et intégré les importes css et
     * les sources des balises images directement dans le code.
     * @param url Url de la page html
     * @param takeCSS boolean indiquant si l'on prend les CSS
     * @param takeIMG boolean indiquant si l'on prend les images
     * @return Un string de la source html
     */
    public static String htmlWithSource(String url, boolean takeCSS, boolean takeIMG){
        URL aURL;
        try {
            aURL = new URL(url);
            Document doc = Jsoup.connect(url).get();
            if(takeCSS){
                replaceCss(doc,aURL);
            }
            if (takeIMG){
                replaceImg(doc,aURL);
            }
            return doc.outerHtml();
        } catch (Exception e) {
            ErrorWindow error = new ErrorWindow();
            error.display("Error","Error in HtmlParser!", e.toString());
        }
        return "";
    }

    /**
     * Remplace les sources des balises images avec le code en Base64.
     * @param doc Jsoup Document
     * @param aURL URL de la resource
     * @throws Exception
     */
    private static void replaceImg(Document doc, URL aURL) {
        for (Element element : doc.select("img")) {
            String realLink,filePath;
            byte[] cssByte,imageInB64;

            filePath = element.attr("src");

            if(filePath == null || filePath.isEmpty() || filePath.startsWith("data:")) continue;

            realLink = absoluteLink(filePath,aURL);


            try{
                cssByte = Jsoup.connect(realLink).ignoreContentType(true).execute().bodyAsBytes();

                if(cssByte != null){
                    imageInB64 = Base64.getEncoder().encode(cssByte);
                    element.attr("src", "data:image/png;base64," + new String(imageInB64, CHARSET_NAME));
                    element.attr("srcset", "");

                }
            }
            catch (Exception e){
                //Problème pour récupéré le file
            }
        }
    }

    /**
     * Remplace les importes css pour les intégrers directement dans le code.
     * @param doc Jsoup Document
     * @param aURL Url de la resource
     */
    private static void replaceCss(Document doc,URL aURL) {
        for (Element link : doc.select("link[rel=stylesheet]")) {
            String cssText,filePath,realLink;
            byte[] cssByte;

            filePath = link.attr("href");

            if(filePath == null || filePath.isEmpty()) continue;

            realLink = absoluteLink(filePath,aURL);

            try{
                cssByte = Jsoup.connect(realLink).ignoreContentType(true).execute().bodyAsBytes();
                cssText = new String(cssByte, CHARSET_NAME);

                Element style = new Element(Tag.valueOf("style"), "");
                style.appendText(cssText);
                link.replaceWith(style);
            }
            catch (Exception e){
                //Problème pour récupéré le file
            }

        }
    }

    /**
     * Permet de récupérer le lien absolue d'un fichier.
     * example :
     * base : squelettes/img/linktosite-wall.png
     * result : http://www.article27.be/squelettes/img/linktosite-wall.png
     * @param filePath Url de la resource
     * @param aURL Url de l'article
     * @return absolute link
     */
    public static String absoluteLink(String filePath, URL aURL) {

        if(filePath.isEmpty() || aURL ==null) return "";

        String protocol = aURL.getProtocol();
        String domain = aURL.getHost();

        String pathResolve = "//";
        if(filePath.startsWith(pathResolve)){
            return filePath.replaceFirst(pathResolve,"http://");
        }

        String pathSeparator = "/";
        if(filePath.startsWith(pathSeparator)){
            String protocolSeparator = "://";
            return protocol + protocolSeparator + domain + filePath;
        }
        return filePath;
    }


    /**
     * Renvoi un string contenant l'html de l'article
     * @param path Path du fichier HTML
     * @return Renvoi un string contenant l'html
     */
    public static String readHTML(String path) throws IOException{
        File file = new File(path);
        return FileUtils.readFileToString(file, StandardCharsets.UTF_8);
    }
}
