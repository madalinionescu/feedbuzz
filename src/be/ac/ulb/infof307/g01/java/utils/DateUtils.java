package be.ac.ulb.infof307.g01.java.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Classe regroupant une série de fonctions utiles pour manager des dates
 */
public class DateUtils {

    /**
     * Convertis une date en string vers une Date java.
     * @param string la date en string (format : EEE, dd MMM yyyy HH:mm:ss Z)
     * @return la date dans un objet java Date
     */
    public static Date stringToDate(String string){
        DateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH);
        Date date;
        try {
            date = formatter.parse(string);
        } catch (ParseException e) {
            date = new Date();
        }
        return date;
    }
}
