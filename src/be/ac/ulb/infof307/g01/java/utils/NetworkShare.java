package be.ac.ulb.infof307.g01.java.utils;

/**
 * Class qui permet d'implémenter des réseaux pour partager des articles
 */
public abstract class NetworkShare {
    private static final String FORMAT_SHARE_TITLE = "share on %s | %s";

    private final String articleTitle;
    final String articleLink;

    /**
     * Constructor
     * @param articleTitle title
     * @param articleLink link
     */
    NetworkShare(String articleTitle,String articleLink){
        this.articleTitle = articleTitle;
        this.articleLink = articleLink;
    }

    String getArticleLink() {
        return articleLink;
    }

    String getArticleTitle(){
        return articleTitle;
    }

    /**
     * Récupère le titre du partage
     * @return
     */
    public String getShareTitle(){
        return String.format(FORMAT_SHARE_TITLE, getNetworkName(),articleTitle);
    }

    /**
     * Récupère le nom du network
     * @return
     */
    public abstract String getNetworkName();


    /**
     * Récupère le lien de partage de l'article
     * @return lien
     */
    public abstract String getShareLink();
}
