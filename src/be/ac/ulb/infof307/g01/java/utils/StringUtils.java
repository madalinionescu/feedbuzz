package be.ac.ulb.infof307.g01.java.utils;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {
    /**
     * @param keywords liste de mots clés extraits de la bare de recherche
     * @param text le contenu ou le titre d'un article
     * @return le nombre de fois qu'on trouve les mots clés dans le text
     */
    public static long countKeywordOccurences(String[] keywords, String text){
        long numberOccurrences = 0;
        Pattern toFind;
        Matcher matcher;
        for (String word : keywords) {
            toFind = Pattern.compile(word);
            matcher = toFind.matcher(text);
            while (matcher.find()) {
                numberOccurrences++;
            }
        }
        return numberOccurrences;
    }

    /**
     * @param keywords ArrayList des mots quon veut chercher
     * @param text text  dans lequel on cherche les mots
     * @return nbre d'occurence des mots dans le texte
     */
    public static long countKeywordOccurences(ArrayList<String> keywords, String text){
        String[] array = new String[keywords.size()];
        array = keywords.toArray(array);
        return countKeywordOccurences(array, text);
    }
}
