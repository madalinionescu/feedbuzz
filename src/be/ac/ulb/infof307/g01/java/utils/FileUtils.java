package be.ac.ulb.infof307.g01.java.utils;

import java.io.File;
import java.io.InputStream;

/**
 * Utils functions when dealing with files
 */
public class FileUtils {
    /**
     * Ecrit l'inputstream dans le fichier
     * @param path Path du fichier
     * @param is InputStream
     */
    public static void writeInputStreamToFile(String path, InputStream is){
        try {
            File targetFile = new File(path);
            org.apache.commons.io.FileUtils.copyInputStreamToFile(is, targetFile);
        }
        catch (Exception e) {
            //Fichier n'existe pas
        }
    }

    /**
     * @param path path du fichier
     * @return true si le fichier existe
     */
    public static boolean fileExists(String path){
        File f = new File(path);
        return (f.exists() && !f.isDirectory());
    }
}
