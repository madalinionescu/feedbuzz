package be.ac.ulb.infof307.g01.java.utils;

/**
 * Class qui permet de partager des articles sur twitter
 */
public class TwitterShare extends NetworkShare {
    private static final String NETWORK_NAME = "Twitter";
    private static final String FORMAT_SHARE = "https://twitter.com/intent/tweet?text=%s";

    /**
     * Constructor
     * @param articleTitle title
     * @param articleLink link
     */
    public TwitterShare(String articleTitle, String articleLink) {
        super(articleTitle, articleLink);
    }

    @Override
    public String getNetworkName() {
        return NETWORK_NAME;
    }

    @Override
    public String getShareLink() {
        return String.format(FORMAT_SHARE, articleLink);
    }
}
