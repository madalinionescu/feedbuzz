package be.ac.ulb.infof307.g01.java;

public final class Constants {

    // FXML PATHS
    public static final String FXML_LAUNCHER_PATH = "/fxml/LauncherWindow.fxml";
    public static final String FXML_DASHBOARD_PATH = "/fxml/DashboardWindow.fxml";
    public static final String FXML_IMPORTRSS_PATH = "/fxml/ManagerRSSWindow.fxml";
    public static final String FXML_ADVANCEDFILTER_PATH = "/fxml/AdvancedFilterWindow.fxml";
    public static final String FXML_TRASH_PATH = "/fxml/TrashWindow.fxml";
    public static final String FXML_INTERESTS_PATH = "/fxml/InterestsWindow.fxml";
    public static final String FXML_USERTAG_PATH = "/fxml/ManagerUserTagWindow.fxml";
    public static final String FXML_ARTICLETAG_PATH = "/fxml/ManagerArticleTagWindow.fxml";
    public static final String FXML_PROFILE_PATH = "/fxml/ProfileWindow.fxml";
    public static final String FXML_MANAGER_TWITTER_PATH = "/fxml/ManagerTwitterWindow.fxml";

    // IMAGES PATHS
    public static final String IMG_LOGO_PATH = "/img/feedBuzzLogo.png";
    public static final String IMG_UNAVAILABLE_PATH = "/img/imgUnavailable.png";
    public static final String IMG_TWITTER_HASHTAG_PATH = "/img/hashtagTwitter.png";


    // HTML PATHS
    public static final String HTML_ABOUT_PATH = "/html/aboutText.html";
    public static final String HTML_HELP_PATH = "/html/helpText.html";
    public static final String HTML_TERMS_PATH = "/html/termCondText.html";

    // COMBOBOX ORDER VALUES
    public static final String ORDER_MOST_RECENT = "Most recent";
    public static final String ORDER_LEAST_RECENT = "Least recent";
    public static final String ORDER_MOST_RELEVANT = "Most relevant";
    public static final String ORDER_LEAST_RELEVANT = "Least relevant";

    // TAGS
    public static final String TAG_NAME_NOT_READ = "Not read";
    public static final String TAG_NAME_READING = "Reading";
    public static final String TAG_NAME_FINISHED = "Finished";
    public static final String TAG_NAME_RECOMMENDED = "Recommended";
    public static final String STRING_UNKNOWN = "Unknown";

    // ICONS
    public static final String DIR_ICON_PATH = "/img/directoryIcon.png";
    public static final String ART_ICON_PATH = "/img/newspaper.png";

    public static final String HASHTAG = "hashtag";
    public static final String ACCOUNT = "account";

    //Root directory name
    public static final String ROOT_DIRECTORY_NAME = "/";

}
