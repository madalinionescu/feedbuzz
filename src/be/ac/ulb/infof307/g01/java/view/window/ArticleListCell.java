package be.ac.ulb.infof307.g01.java.view.window;

import be.ac.ulb.infof307.g01.java.model.Article;
import be.ac.ulb.infof307.g01.java.model.Tag;
import be.ac.ulb.infof307.g01.java.model.ParserRSS;
import be.ac.ulb.infof307.g01.java.Constants;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.List;

import static be.ac.ulb.infof307.g01.java.Constants.IMG_UNAVAILABLE_PATH;

/**
 * Utilisé pour afficher la liste des articles
 * contenant toutes les informations requises
 */
public class ArticleListCell extends ListCell<Article> {
    private final HBox content;

    private final Text title;
    private final Text date;
    private final Text source;
    private final Text location;
    private final List<Text> hashtags;

    private final HBox footer;
    private final ImageView imageView;

    /**
     * Constructeur de l'objet qui va établir l'organisation des
     * informations : pour chaque article on doit créer une
     * cellule dans la liste qui contient son titre, son auteur,
     * la date de publication, la localisation et une vignette.
     */
    public ArticleListCell() {
        super();
        title = new Text();
        date = new Text();
        source = new Text();
        location = new Text();
        hashtags = new ArrayList<>();

        // layout variables
        HBox header = new HBox(source, date, location);
        header.setSpacing(10);
        footer = new HBox();

        title.wrappingWidthProperty().bind(super.widthProperty().subtract(120));

        VBox vBox = new VBox(header, title, footer);

        Image thumbnail = new Image(getClass().getResource(IMG_UNAVAILABLE_PATH).toString());
        imageView = new ImageView(thumbnail);
        imageView.setFitWidth(100);
        imageView.setPreserveRatio(true);
        imageView.setSmooth(true);
        imageView.setCache(true);
        imageView.setImage(thumbnail);

        content = new HBox(imageView, vBox);
        content.setSpacing(10);
    }

    @Override
    public void updateItem(Article item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null && !empty) {
            title.setText(item.getName());
            title.setStyle("-fx-font-size: 16pt;");
            date.setText(item.getDate());
            date.setStyle("-fx-fill: black;");
            source.setText(item.getSource());
            source.setStyle("-fx-fill: grey;");
            location.setText(item.getLocation());
            location.setStyle("-fx-fill: grey;");

            List<String> tempHashtags = item.getHashtags();
            for (String tempHashtag : tempHashtags) {
                hashtags.add(new Text(tempHashtag));
            }

            imageView.setImage(ParserRSS.thumbnailStringToImage(item.getThumbnail()));


            footer.getChildren().addAll(hashtags);

            footer.setSpacing(10);

            setGraphic(content);
            changeItemColor(item); // change the color of the cell if recommended
        } else {
            setGraphic(null);
        }

    }

    private void changeItemColor(Article item) {
        for (Tag tag : item.getTags()) {
            if (tag.getName().equals(Constants.TAG_NAME_RECOMMENDED)) {
                this.setStyle("-fx-control-inner-background: #fdcb6e");
                break;
            }
            else {
                this.setStyle("-fx-control-inner-background: #ffffff");
            }
        }
        if(item.isTweet()){
            this.setStyle("-fx-control-inner-background: #c0deed");
        }

    }
}