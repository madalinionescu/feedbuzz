package be.ac.ulb.infof307.g01.java.view.controller;

public abstract class ViewController<PrimeListener> {

    PrimeListener listener;

    public void setListener(PrimeListener listener) {
        this.listener = listener;
    }
}
