package be.ac.ulb.infof307.g01.java.view.controller;

import be.ac.ulb.infof307.g01.Main;
import be.ac.ulb.infof307.g01.java.model.dao.DAOFactory;
import be.ac.ulb.infof307.g01.java.model.*;
import be.ac.ulb.infof307.g01.java.utils.*;
import be.ac.ulb.infof307.g01.java.view.listener.DashboardViewListener;
import be.ac.ulb.infof307.g01.java.view.listener.MainListener;
import be.ac.ulb.infof307.g01.java.view.window.*;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Callback;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;
import org.controlsfx.control.textfield.TextFields;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.io.IOException;
import java.net.URI;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.net.URL;
import java.net.URLConnection;

import static be.ac.ulb.infof307.g01.java.utils.HtmlParser.readHTML;
import static be.ac.ulb.infof307.g01.java.Constants.*;


public class DashboardWindowController extends ViewController<MainListener> implements DashboardViewListener  {

    @FXML private TextField searchFieldText;
    @FXML private Button searchButton;
    @FXML private Button advancedButton;
    @FXML private ListView mainArticlesList;
    @FXML private VBox previewBox;
    @FXML private Button mainRefreshFeedButton;
    @FXML private Label mainProgressBarText;
    @FXML private Label readingStatus;
    @FXML private ProgressBar mainProgressBar;
    @FXML private Button previewFullButton;
    @FXML private Button previewOpenButton;
    @FXML private Button previewCopyButton;
    @FXML private Button previewDeleteButton;
    @FXML private Button previewFinishedButton;
    @FXML private Button previewTagButton;
    @FXML private ImageView twitterImg;
    @FXML private ImageView facebookImg;

    @FXML private MenuBar menuBar;
    @FXML protected ComboBox sortComboBox;

    @FXML public TreeView<Node> treeDirectory;
    @FXML public TextField folderNameText;
    @FXML public Button createFolderButton;
    @FXML public ComboBox<Directory> folderList;
    @FXML public Button deleteFolderButton;

    private Directory rootDirectory;

    ArrayList<Article> searchedArticles;
    private Search search;

    private ArrayList<String> folderName;
    private ArrayList<String> folderId;


    /**
     * Ajoute les articles déjà existants dans la base de données
     * dans la liste des articles.
     */
    public void initializeDashboard() {
        // quand l’application se charge, les articles sont déjà chargés
        refreshArticleList();
        // vérifier si les articles déjà existants ont des mises à jour
        refreshButtonClicked();
        // ajouter la possibilité de choisir un tri personnalisé
        sortComboBox.getItems().addAll(ORDER_MOST_RECENT,ORDER_LEAST_RECENT,ORDER_MOST_RELEVANT,ORDER_LEAST_RELEVANT);
        sortComboBox.setValue(ORDER_MOST_RECENT);

        updateSearchSuggestions();

        // Create root of directory structure
        folderName = new ArrayList<>();
        folderId = new ArrayList<>();

        rootDirectory = listener.getDirectory(ROOT_DIRECTORY_NAME);

        if (rootDirectory == null) {
            rootDirectory = new Directory(ROOT_DIRECTORY_NAME);
            listener.createRootDirectory(rootDirectory);
        }

        showDirectoryTree();
    }


    /**
     * Ajoute la recherche (search) dans l'historique de recherche
     * @param search contient le texte sur lequel on effectue la recherche
     */
    void addToSearchHistory(String search) {
        listener.addToSearchHistory(search);
        updateSearchSuggestions();
    }

    /**
     * Met à jour les suggestions de la barre de recherche simple
     */
    private void updateSearchSuggestions() {
        ArrayList<String> suggestions = listener.getSearchHistory();
        String[] suggestionsList = suggestions.toArray(new String[0]);
        TextFields.bindAutoCompletion(searchFieldText, suggestionsList);
    }

    /**
     * Appelée lorsque l'ordre dans lequel les articles sont affichés est
     * sélecté par l'utilisateur. Les articles sont afichées dans l'ordre selectioné
     */
    public void comboBoxItemSelected() {
        String output = sortComboBox.getSelectionModel().getSelectedItem().toString();
        switch (output) {
            case ORDER_MOST_RECENT:
                displayOrderedList(orderArticlesDate(descendingOrder()));
                break;
            case ORDER_LEAST_RECENT:
                displayOrderedList(orderArticlesDate(ascendingOrder()));
                break;
            case ORDER_MOST_RELEVANT:
                displayOrderedList(orderArticlesRelevance(descendingOrder()));
                break;
            case ORDER_LEAST_RELEVANT:
                displayOrderedList(orderArticlesRelevance(ascendingOrder()));
                break;
        }
    }

    /**
     * Utilisé pour créer une collection avec une direction de tri spécifique.
     * @return une collection vide dans laquelle on ajoute les articles pour
     * les trier
     */
    private TreeMap<Long, ArrayList<Article> > ascendingOrder() {
        return (new TreeMap<>());
    }

    /**
     * Utilisé pour créer une collection avec une direction de tri spécifique.
     * @return une collection vide dans laquelle on ajoute les articles pour
     * les trier
     */
    private TreeMap<Long, ArrayList<Article> > descendingOrder() {
        return (new TreeMap<>(Comparator.reverseOrder()));
    }

    /**
     * Affiche les articles dans l'ordre imposé
     * @param orderedArticles liste d'articles ordonnée par date ou relevance
     */
    private void displayOrderedList(ArrayList<Article> orderedArticles){
        clearArticlesFromListView();
        putArticlesInListView(orderedArticles);

        // changer la position de la barre de déroulement au début de la liste
        mainArticlesList.scrollTo(0);
    }

    /**
     * Appele la focntion qui ordonne les articles et renvoie une liste triée d'articles
     * @param sortedArticleDict est une liste triée d'articles
     * @return
     */
    private ArrayList<Article> orderArticlesDate(Map<Long, ArrayList<Article>> sortedArticleDict) {
        return orderArticles(createArticleMapDate(sortedArticleDict));
    }

    /**
     * Appelée pour ordonnér les articles en fonction du temps de la publication
     * @param sortedArticleDict  un dictionnaire ordonné qui contient comme clé: le nombre d'occurences
     * d' un/plusieurs mot(s) recherché(s), chaque clé en ayant comme valeur, une liste d'articles
     * qui ont la meme relevance
     * @return orderedArticles: la liste trié des articles d'un utlisateur, mise à jour
     */
    private ArrayList<Article> orderArticlesRelevance(Map<Long, ArrayList<Article>> sortedArticleDict) {
        return orderArticles(createArticleMapRelevance(sortedArticleDict));
    }

    /**
     * Fonction qui crée la liste triée d'articles (par date ou relevance)
     * @param sortedArticleDict est un dictionnaire ordonné
     * @return la liste trié des articles d'un utlisateur
     */
    private ArrayList<Article> orderArticles(Map<Long, ArrayList<Article>> sortedArticleDict) {
        ArrayList<Article> orderedArticles = new ArrayList<>();
        for(Map.Entry<Long, ArrayList<Article> > entry : sortedArticleDict.entrySet()) {
            orderedArticles.addAll(entry.getValue());
        }
        return orderedArticles;
    }

    /**
     * Appelée pour créer un dictionnaire ordonné d'articles
     * @param sortedArticleDict un dictionnaire ordonné qui contient comme clé: la date en
     *                "UNIX Epoch time" , ayant comme valeur, une liste d'articles
     *                qui ont la même date de publication
     * @return le sortedArticleDict mis à jour
     */
    private Map<Long, ArrayList<Article>> createArticleMapDate(Map<Long, ArrayList<Article>> sortedArticleDict){
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        for(Article article : searchedArticles) {
            Date date;
            try {
                date = formatter.parse(article.getDate());
            } catch (ParseException e) {
                WarningWindow warning = new WarningWindow();
                warning.display("Warning","Article date not parsed correctly",
                        "Some article may not be displayed in the correct order");
                /*
                 si la date de l’article ne peut être determineé correctement, ils
                 sont ajoutés à la fin (ils sont considérés comme publiés le 01-01-1970)
                */
                date = new Date(0);
            }
            ArrayList<Article> articlesList = sortedArticleDict.computeIfAbsent(date.toInstant().toEpochMilli(), k -> new ArrayList<>());
            /*
                 Convertit la date en temps d’époque et la met comme une clé dans
                 le dictionnaire. Ajoute la liste des articles (comme une valeur clé)
                 qui ont été publiés à la même date et heure (si est le cas)
                */
            articlesList.add(article);
        }
        return sortedArticleDict;
    }

    /**
     * Fonction qui ajoute les articles et leur niveau de relevance au dictionnaire ordonné
     * @param sortedArticleDict dictionnaire ordonné, dont les clés representent le niveau de relevance et
     *                les valeurs de chaque clé representant une liste d'articles appartenant à
     *                un niveau de relevance
     * @param article article
     * @param numberOccurrences nbre d'occurences
     * @return le dictionnaire trié
     */
    private Map<Long, ArrayList<Article>> addToTreeMap(Map<Long, ArrayList<Article>> sortedArticleDict,
                                                       Article article, long numberOccurrences){
        ArrayList<Article> articlesList = sortedArticleDict.computeIfAbsent(numberOccurrences, k -> new ArrayList<>());
        articlesList.add(article);

        return sortedArticleDict;
    }

    /**
     * Fonction qui traite les resulats de la recherche simple et renvoie le dictionnaire ordonné
     * mis à jour
     * @param sortedArticleDict
     * @return le dictionnaire trié
     */
    private Map<Long, ArrayList<Article>> processSimpleSearchResults(Map<Long, ArrayList<Article>> sortedArticleDict){
        long numberOccurrences;
        String[] keywords;
        String text;
        keywords = searchFieldText.getText().toLowerCase().split(" ");
        for (Article article : searchedArticles) {
            text = article.getContent().toLowerCase() + " " + article.getName().toLowerCase();
            numberOccurrences = StringUtils.countKeywordOccurences(keywords,text);
            sortedArticleDict = addToTreeMap(sortedArticleDict,article,numberOccurrences);
        }
        return sortedArticleDict;
    }

    /**
     * Fonction qui traite les resulats de la recherche avancée
     *
     * @param sortedArticleDict
     * @return le dictionnaire trié contenant les resultats d'une recherche avancée
     */
    private Map<Long, ArrayList<Article>> processAdvancedSearchResults(Map<Long, ArrayList<Article>> sortedArticleDict) {
        String[] keywords = {""};
        String text = "";
        int flag = -1;
        long numberOccurrences;

        if (search.getContent() != null) {
            keywords = search.getContent().toLowerCase().split(" ");
            flag += 1;
        }
        if (search.getTitle() != null) {
            keywords = search.getTitle().toLowerCase().split(" ");
            flag += 2;
        }
        for (Article article : searchedArticles) {
            switch (flag) {
                case 0:
                    text = article.getContent().toLowerCase();
                    break;
                case 1:
                    text = article.getName().toLowerCase();
                    break;
                case 2:
                    text = article.getContent().toLowerCase() + " " + article.getName().toLowerCase();
                    break;
            }
            numberOccurrences = StringUtils.countKeywordOccurences(keywords, text);
            sortedArticleDict = addToTreeMap(sortedArticleDict, article, numberOccurrences);
        }
        return sortedArticleDict;
    }

    /**
     * Appelée pour créer un dictionnaire ordonné d'articles
     * @param sortedArticleDict un dictionnaire ordonné qui contient comme clé: le nombre d'occurences
     * d' un/plusieurs mot(s) recherché(s). Chaque clé en ayant comme valeur, une liste d'articles
     * qui ont la meme relevance
     * @return le sortedArticleDict mis à jour
     */
    private Map<Long, ArrayList<Article>> createArticleMapRelevance(Map<Long, ArrayList<Article>> sortedArticleDict){
        if (searchFieldText.getText().length() > 0) {
            return processSimpleSearchResults(sortedArticleDict);
        }
        else if (search != null){
            return processAdvancedSearchResults(sortedArticleDict);
        }
        return sortedArticleDict;
    }

    /**
     * Ferme la fenêtre et l'application.
     */
    @FXML private void quitButtonClicked() {
        Stage stage = (Stage) menuBar.getScene().getWindow();
        stage.close();
    }

    /**
     * Affiche la fenêtre d'aide.
     */
    public void helpButtonClicked() {
        try {
            HelpWindow.display();
        } catch (Exception e) {
            ErrorWindow error = new ErrorWindow();
            error.display("Error","An error occurred while opening the Help window", e.getMessage());
        }
    }

    /**
     * Permet d'afficher la fenetre About
     */
    public void aboutButtonClicked(){
        try {
            AboutWindow.display();
        } catch (Exception e) {
            ErrorWindow error = new ErrorWindow();
            error.display("Error","An error occurred while opening the About window", e.getMessage());
        }
    }

    /**
     * Permet d'afficher la fenêtre Terms and Conditions
     * @throws Exception
     */
    public void termsConditionsButtonClicked() {
        try {
            TermCondWindow.display();
        } catch (IOException e) {
            ErrorWindow error = new ErrorWindow();
            error.display("Error","An error occurred while opening the Terms and Conditions window", e.getMessage());
        }
    }

    /**
     * Lorsque le bouton SignOut du menu est cliqué, l'utilisateur est
     * deconnecté et la fenetre de SignIn est lancée. La fenêtre de l'application est fermée aprés.
     */
    public void signOutButtonClicked() {
        // Disconnect user
        listener.signOut();
        // Launch SignIn Window
        // take into account the listener
        //new LauncherWindow().display();
        // Quit Dashboard Window
        quitButtonClicked();
    }

    /**
     * Affiche la fenêtre de consultation des informations liées
     * au profile utilisateur
     * @throws Exception
     */
    public void menuProfileClicked() throws Exception {
        ProfileWindow profileWindow = new ProfileWindow(listener);
        profileWindow.display();
        displayRefreshedArticles();
        displayRefreshedArticles();
    }


    /**
     * Lorsque l'utilisateur clique sur un article de la liste, la fenêtre d'aperçu est mise à jour
     * et elle affiche l'aperçu de l'article
     */
    public void mainListItemClicked() {
        updatePreview();
    }

    /**
     * Gère la visibilité et activation des boutons afin que l'user
     * ne clique pas sur des boutons improbables lorsque les articles
     * se chargent
     * @param status true si on désactive (donc on est en loading), false sinon.
     */
    private void disableCenterButtons(boolean status){
        mainRefreshFeedButton.setDisable(status);
        mainProgressBar.setVisible(status);
        mainProgressBarText.setVisible(status);
        searchButton.setDisable(status);
        advancedButton.setDisable(status);
        sortComboBox.setDisable(status);
    }

    /**
     * Affiche l'appercu de l'article
     */
    private void showPreview() {
        // récupérer l'article choisi
        Article article = (Article) mainArticlesList.getSelectionModel().getSelectedItem();

        // effacer le contenu actuel de la fenêtre
        previewBox.getChildren().clear();

        ImageView image;
        if (!article.isTweet()) {
            image = new ImageView(ParserRSS.thumbnailStringToImage(article.getThumbnail()));
        }
        else {
            image = new ImageView(new Image(getClass().getResource(IMG_TWITTER_HASHTAG_PATH).toString()));
        }
        image.setPreserveRatio(true);
        image.fitWidthProperty().bind(previewBox.widthProperty());

        // afficher les tags de l'article
        List<String> tagList = listener.getTags(article);
        updateReadingStatus(tagList);
        Text tags = new Text("Tags : " + tagList.toString());

        // afficher l’aperçu de l’article actuellement sélectionné
        Text formattedTitle = new Text(article.getName() + "\n");
        formattedTitle.setStyle("-fx-font-size: 20pt;");
        formattedTitle.setTextAlignment(TextAlignment.CENTER);
        formattedTitle.wrappingWidthProperty().bind(previewBox.widthProperty());

        String summary = article.getContent();
        // Le résumé doit contenir au plus 200 caractères.
        if (summary.length() > 200) {
            summary = summary.substring(0, 200);
            summary = summary + "[...]";
        }
        WebView formattedSummary = new WebView();
        formattedSummary.getEngine().loadContent(summary);

        previewBox.getChildren().addAll(tags, image, formattedTitle, formattedSummary);
    }

    private void updateReadingStatus(List<String> tagList) {
        if (tagList.contains(TAG_NAME_FINISHED)) {
            readingStatus.setText(TAG_NAME_FINISHED);
        } else if (tagList.contains(TAG_NAME_READING)) {
            readingStatus.setText(TAG_NAME_READING);
        } else {
            readingStatus.setText(TAG_NAME_NOT_READ);
        }
    }

    /**
     * Met à jour le contenu de la fenêtre d'apperçu
     */
    private void updatePreview() {
        // Mettre à jour l’aperçu
        if (mainArticlesList.getSelectionModel().getSelectedItem() != null) {
            showPreview();
            enablePreviewButtons();
        } else { // aucun article selectionné
            clearPreview();
            disablePreviewButtons();
        }
    }

    /**
     * Efface les articles de la ListView
     */
    void clearArticlesFromListView(){
        mainArticlesList.getItems().clear();
    }


    /**
     * Vérifie si l'user a internet (ou si le site hebergeant l'article est up)
     * @param article article pour être sûr que le site soit up aussi
     * @return true if user has internet
     */
    private boolean isConnected(Article article){
        boolean isConnected = true;
        try {
            URL url = new URL(article.getLink());
            URLConnection connection = url.openConnection();
            connection.connect();
        } catch (Exception e){
            isConnected = false;
        }
        return isConnected;
    }

    /**
     * Lorsque le bouton "Read the full article" est cliqué, la fenêtre contenant les informations
     * de l'entier article est affichée
     */
    public void readFullArticleButtonClicked() {
        try {
            Article article = (Article) mainArticlesList.getSelectionModel().getSelectedItem();
            boolean isConnected = isConnected(article);
            if(isConnected){
                ViewFullArticleWindow.display(article.getName(), article.getLink());
            }else{
                ViewFullArticleWindow.display(article.getName(), readHTML(article.getHTMLPath()));
            }

            listener.updateReadingStatus(article, true);
            updatePreview();

        } catch (IOException e) {
            ErrorWindow error = new ErrorWindow();
            error.display("Error", "Error", "An error occurred while opening the article");
        } catch (NullPointerException e) {
            ErrorWindow error = new ErrorWindow();
            error.display("Error","No article selected in the list", e.getMessage());
        }
    }

    /**
     * Marque l'article comme étant lu.
     */
    public void previewFinishedButtonClicked(){
        try {
            Article article = (Article) mainArticlesList.getSelectionModel().getSelectedItem();
            listener.updateReadingStatus(article, false);
            showPreview();
        }
        catch (NullPointerException e) {
            ErrorWindow error = new ErrorWindow();
            error.display("Error","No article selected in the list", e.getMessage());
        }
    }

    /**
     * Lorsque le bouton Open est cliqué, le navigateur web s'ouvre avec la page web de l'article
     */
    public void openButtonClicked() {
        // ouvrir l’article dans le navigateur par défaut
        Article article = (Article) mainArticlesList.getSelectionModel().getSelectedItem();
        if (article != null) {
            if (Desktop.isDesktopSupported()) {
                new Thread(() -> {
                    try {
                        Desktop.getDesktop().browse(new URI(article.getLink()));
                    } catch (Exception e) {
                        // Peut pas gérer l'erreur avec une fenêtre d'erreur car
                        // pas possible d'ouvrir une fenètre dans un thread
                    }
                }).start();
            }
        }

        else {
            ErrorWindow error = new ErrorWindow();
            error.display("Error","No article selected in the list", "");
        }
    }

    /**
     * Lorsque le bouton "Copy" est cliqué, le lien de l'article est copié dans presse-papier
     */
    public void copyButtonClicked() {
        try {
            // copier le lien de l’article au presse-papiers
            Article article = (Article) mainArticlesList.getSelectionModel().getSelectedItem();
            Toolkit.getDefaultToolkit()
                    .getSystemClipboard()
                    .setContents(
                            new StringSelection(article.getLink()),
                            null
                    );
        }
        catch (NullPointerException e) {
            ErrorWindow error = new ErrorWindow();
            error.display("Error","No article selected in the list", e.getMessage());
        }
    }

    /**
     * Place les articles dans la liste de l'interface graphique
     * permettant d'afficher ceux-ci par la suite.
     * @param articles une liste d'Articles d'un utilisateur
     */
    void putArticlesInListView(ArrayList<Article> articles) {
        mainArticlesList.scrollTo(0);
        mainArticlesList.getItems().addAll(articles);
        mainArticlesList.setCellFactory((Callback<ListView<Article>, ListCell<Article>>) listView -> new ArticleListCell(){
            {
                prefWidthProperty().bind(mainArticlesList.widthProperty().subtract(20));
                // soustraire la largeur de la barre de défilement
                setMaxWidth(Control.USE_PREF_SIZE);
            }
        });
    }

    /**
     * Change l'état des boutons Full view, Open, Copy, Delete
     * @param status boolean qui décide si les butons doivent être désactivés ou pas
     */
    private void switchPreviewButtons(boolean status) {
        previewFullButton.setDisable(status);
        previewOpenButton.setDisable(status);
        previewCopyButton.setDisable(status);
        previewDeleteButton.setDisable(status);
        previewFinishedButton.setDisable(status);
        previewTagButton.setDisable(status);
        twitterImg.setDisable(status);
        facebookImg.setDisable(status);
        folderList.setDisable(status);

        if(status){
            facebookImg.setOpacity(0.2);
            twitterImg.setOpacity(0.2);
        }
        else{
            facebookImg.setOpacity(1.0);
            twitterImg.setOpacity(1.0);
        }

    }

    /**
     * Fonction qui désactive les boutons de la fenêtre d'apperçu
     */
    private void disablePreviewButtons() {
        switchPreviewButtons(true);
    }

    /**
     * Fonction qui active les boutons de la fenêtre d'apperçu
     */
    private void enablePreviewButtons() {
        switchPreviewButtons(false);
    }

    /**
     * Fonction qui efface le contenu de la fenêtre d'apperçu
     */
    void clearPreview() {
        previewBox.getChildren().clear();
        previewBox.getChildren().addAll(new Text("Select an article to preview it."));
        readingStatus.setText(null);
    }

    /**
     * Lorsque le bouton "Delete" est cliqué, l'article sélectionné est mis dans la corbeille
     */
    public void previewDeleteButtonClicked() {
        Article article = (Article) mainArticlesList.getSelectionModel().getSelectedItem();

        try {
            listener.deleteArticle(article);
            mainArticlesList.getItems().remove(article);
            mainArticlesList.getSelectionModel().clearSelection();
            updatePreview();
        } catch (NullPointerException e) {
            ErrorWindow error = new ErrorWindow();
            error.display("Error","No article selected in the list", e.getMessage());
        }
    }

    /**
     * Prend les articles recherchés et les trie. Puis rafraichit la fenêtre et les
     * articles.
     */
    private void displayRefreshedArticles() {
        refreshArticleList();
        updateRecommendations();
        disableCenterButtons(false);
        mainRefreshFeedButton.setText("Refresh Feed");
    }

    @Override
    public MainListener getMainListener() {
        return listener;
    }

    private void refreshArticleList() {
        searchedArticles = listener.getArticles();
        displayOrderedList(orderArticlesDate(descendingOrder()));
    }

    /**
     * Update les articles d'un tag recommendation s'ils sont intéressants
     * pour l'utilisateur càd si l'article contient au moins une fois un
     * des mots qu'il a ajouté à ses intérêts.
     */
    private void updateRecommendations() {
        ArrayList<Article> articles = listener.getArticles();
        ArrayList<String> interests = LocalUser.singleton.getUser().getUserInterests();
        if (interests == null) return;

        for (Article article : articles) {
            listener.markAsRecommended(article, false);
            int numberOfOccurences = 0;
            numberOfOccurences += StringUtils.countKeywordOccurences(interests, article.getName());
            numberOfOccurences += StringUtils.countKeywordOccurences(interests, article.getContent());
            if (numberOfOccurences >= 1){
                listener.markAsRecommended(article, true);
            }
        }
    }

    /**
     *  Refresh la liste des articles. On lance le Parser de RSS dans un Thread.
     *  Ainsi, la GUI reste "active" (non frozen).
     */
    @FXML private void refreshButtonClicked(){
        updateArticleList();
    }

    @Override
    public void updateArticleList(){
        disableCenterButtons(true);
        mainRefreshFeedButton.setText("Loading...");
        ThreadJavaFx thread = new ThreadJavaFx() {
            @Override
            protected void backgroundWork() {
                listener.refreshRSSInBackground();
                listener.refreshTwitterInBackground();
            }
            @Override
            protected void finalWork() {
                displayRefreshedArticles();
                showDirectoryTree();
            }


        };
        thread.run();
    }

    /**
     * Lorsque le bouton "Deleted articles" est cliqué, la fenêtre contenat les articles mises dans la
     * corbeille est affichée.
     */
    public void openTrash() {
        try {
            TrashWindow trashWindow = new TrashWindow(listener);
            trashWindow.display();
        } catch (Exception e) {
            ErrorWindow error = new ErrorWindow();
            error.display("Error","Error openning the recycle bin",
                    "An error occurred while openning the recycle bin");
        }
        refreshArticleList();
    }


    /**
     * Lorsque le bouton RSS Manager est cliqué, la fenêtre qui gère les RSS est affichée et la liste
     * d'articles est mise à jour.
     * @throws Exception si problème d'ouverte de la manager rss windows
     */
    public void addRssButtonClicked() throws Exception{
        ManagerRSSWindow managerRSSWindow = new ManagerRSSWindow(this);
        managerRSSWindow.display();
    }

    /**
     * Recherche parmi les articles les attributs contenant le texte
     * entré dans la barre de recherche
     * @return searchedArticles une liste d'Articles correspondant à
     * la recherche
     */
    private Search readSearchBarContent() {
        String searchText = searchFieldText.getText();
        return new Search(searchText);
    }

    /**
     * Partage l'article sur facebook via une webView
     */
    public void shareOnFacebook(){
        try {
            Article article = (Article) mainArticlesList.getSelectionModel().getSelectedItem();
            share(new FacebookShare(article.getName(), article.getLink()));
        }

        catch (NullPointerException e) {
            ErrorWindow error = new ErrorWindow();
            error.display("Error","No article selected in the list", e.getMessage());
        }
    }

    /**
     * Partage l'article sur facebook via une webView
     */
    public void shareOnTwitter(){
        try {
            Article article = (Article) mainArticlesList.getSelectionModel().getSelectedItem();
            share(new TwitterShare(article.getName(), article.getLink()));
        }

        catch (NullPointerException e) {
            ErrorWindow error = new ErrorWindow();
            error.display("Error","No article selected in the list", e.getMessage());
        }
    }

    /**
     * Partage l'article sur un réseau via une webView
     */
    private void share(NetworkShare network){
        try {

            ViewFullArticleWindow.display(network.getShareTitle(),network.getShareLink());

        } catch (IOException e) {
            ErrorWindow error = new ErrorWindow();
            error.display("Error", "Error", "An error occurred while openning the article");
        }
    }

    /**
     * Lorsque le bouton de recherche est cliqué, la liste des articles est
     * nettoyée et les articles correspondant à la recherche
     * y sont affichés
     */
    public void searchButtonClicked() {
        // Ajoute la recherche à l'historique de recherche
        addToSearchHistory(searchFieldText.getText());
        sortComboBox.setValue("Most recent");
        search = readSearchBarContent();
        search.setId(LocalUser.singleton.getUserId());
        searchedArticles = search.search();
        clearArticlesFromListView();
        clearPreview();
        putArticlesInListView(searchedArticles);
    }

    /**
     * Lorsque le bouton de recherche avancée est cliqué, une fenêtre contenant
     * toutes les informations spécifique sur lesquelles l'utilisateur pour faire
     * une recherche, ainsi certaines actions spécifiques s'affiche
     */
    public void advancedButtonClicked() throws Exception {
        searchFieldText.clear();
        AdvancedFilterWindow advancedSearchWindow = new AdvancedFilterWindow(listener);
        advancedSearchWindow.setParentWindowController(this);
        advancedSearchWindow.display();
    }

    void setSearchedArticles(ArrayList<Article> newSearchedArticles) {
        searchedArticles = newSearchedArticles;
    }

    void setSearch(Search newSearch) {
        search = newSearch;
    }

    public void folderTreeItemClicked() {
        updateFolderView();
    }

    private void updateFolderView() {
        TreeItem<Node> item = treeDirectory.getSelectionModel().getSelectedItem();
        if (item != null) {
            String itemName = item.getValue().getName();
            ArrayList<Article> results = new ArrayList<>();

            if (!item.getValue().nodeIsFolder()) {
                // item est un article
                Article article = listener.getArticle(itemName);
                results.add(article);
            }

            else {
                Directory directory = listener.getDirectory(itemName);
                // item est un dossier
                if (directory.getName() == null) {
                    // item est Home folder
                    directory = rootDirectory;
                }
                Search search = new Search();
                results = search.searchInDir(directory);
            }

            changeArticleView(results);
            disablePreviewButtons();
        }
    }

    private void changeArticleView(ArrayList<Article> articles) {
        clearPreview();
        clearArticlesFromListView();
        putArticlesInListView(articles);
    }

    /***
     * Le root directory est modifié sans passer par rootDirect (la variable) donc désynchronisation ...
     * Si on veut supprimer cette ligne, il faut que les modifications soit faites directement
     * sur cette variable et non par un id dans le dao
     */
    public void reloadRootDirectory(){
        rootDirectory = DAOFactory.getDirectoryDAO().findById(rootDirectory.getId());
    }

    /**
     * Fonction qui sert à afficher la structire hiéarchique des dossiers et des articles
     */
    private void showDirectoryTree(){
        reloadRootDirectory();
        displayFolder(rootDirectory, null);
        updateComboBox();
    }

    private void displayFolder(Directory node, TreeItem<Node> parentDir) {
        TreeItem<Node> newNode = new TreeItem<>(node);
        ImageView dirIcon = new ImageView(new Image(Main.class.getResourceAsStream(DIR_ICON_PATH)));
        newNode.setGraphic(dirIcon);

        if (parentDir == null) {
            treeDirectory.setRoot(newNode);
        } else {
            parentDir.getChildren().add(newNode); //On se rattache à son parent
        }

        for (Node tmpNode : node.nodeList()) {
            if (tmpNode.nodeIsFolder()) {
                displayFolder((Directory) tmpNode, newNode);
            }
            else {
                TreeItem<Node> newArticleNode = new TreeItem<>(tmpNode);
                ImageView articleIcon = new ImageView(new Image(Main.class.getResourceAsStream(ART_ICON_PATH)));
                newArticleNode.setGraphic(articleIcon);
                newNode.getChildren().add(newArticleNode);
            }
        }

        newNode.setExpanded(true);
    }

    /**
     * Fonction appelée lors de la création d'un dossier
     */
    @FXML
    private void createFolderButtonClicked(){
        //Créer le dossier
        Node parent;
        String name = folderNameText.getText().trim();

        if (name.isEmpty() || name.equals(ROOT_DIRECTORY_NAME)) return;

        TreeItem<Node> item = treeDirectory.getSelectionModel().getSelectedItem();
        parent = rootDirectory;
        if(item != null){
            parent = treeDirectory.getSelectionModel().getSelectedItem().getValue();
        }

        //add folder to db
        if (parent.getClass() == Directory.class) {
            listener.createDirectory(name, (Directory) parent);
        }

        //refresh
        showDirectoryTree();
    }

    public void tagButtonClicked() throws Exception{
        try {
            Article article = (Article) mainArticlesList.getSelectionModel().getSelectedItem();
            ManagerArticleTagWindow tagWindow = new ManagerArticleTagWindow(this);
            tagWindow.display(article);
            updatePreview();
        }

        catch (NullPointerException e) {
            ErrorWindow error = new ErrorWindow();
            error.display("Error","No article selected in the list", e.getMessage());
        }
    }

    /**
     * Ajoute un dossier au ComboBox des articles afin de l'associer à ceux-ci.
     */
    private void updateComboBox(){
        List<Directory> tmpList = DAOFactory.getDirectoryDAO().findAll();
        folderList.getItems().clear();

        for (Directory tmpDir : tmpList) {
            folderList.getItems().add(tmpDir);
        }
    }

    /**
     * Fonction qui mets à jour le dossier auquel appartient un article.
     *
     */
    public void updateFolderOfArticle(){
        try{
            Article article = (Article) mainArticlesList.getSelectionModel().getSelectedItem();
            Directory parentDir = folderList.getSelectionModel().getSelectedItem();

            if(parentDir == null || article == null) return;

            listener.changeArticlePath(parentDir, article);

            //Ce thread permet des erreurs d'index dû à la suppression et l'accès
            //qui peuvent poser problème puisque cette fonction tourne dans un thread différent pour JavaFX
            Platform.runLater(this::showDirectoryTree);

        }
        catch (NullPointerException e) {
            ErrorWindow error = new ErrorWindow();
            error.display("Error","No article selected in the list", e.getMessage());
        }
    }

    /**
     * Renvoir le path complet d'un dossier dans l'arborescence.
     * @param tmp Le TreeItem dont on veut le path
     * @return Un string de la forme /chemin/to/dossier/
     */
    private String getPath(TreeItem<String> tmp){
        StringBuilder path = new StringBuilder(); //On va créer le path pas à pas
        for (TreeItem<String> item = tmp; item!=null; item = item.getParent()){ //On remonte l'arborescence
                                                                                //de parent en parent
            path.insert(0, item.getValue());
            path.insert(0, "/");
        }
        return path.toString();
    }

    /**
     * Fonction qui supprime le dossier sélectionner de la base de donnée
     */
    @FXML
    private void deleteFolderButtonClicked(){
        TreeItem<Node> parent;
        try{
            parent = treeDirectory.getSelectionModel().getSelectedItem();

            if (Objects.equals(parent.getValue(), rootDirectory) || !parent.getValue().nodeIsFolder())
                throw new ValueException("You can't delete this");

            listener.deleteDirectory(parent.getValue().getId());
            showDirectoryTree(); //Mise à jour de l'affichage

        }
        catch (NullPointerException e){
            showErrorWindow("No folder selected","You need to select a folder in order to delete it");
        }
        catch (ValueException e){
            showErrorWindow("Can't delete this folder","You can't delete this folder");
        }
    }

    /**
     * Affiche un fenetre d'erreur
     * @param title Titre de l'erreur
     * @param message Message d'erreur
     */
    private void showErrorWindow(String title, String message) {
        ErrorWindow window = new ErrorWindow();
        window.display("Error", title, message);
    }
}

