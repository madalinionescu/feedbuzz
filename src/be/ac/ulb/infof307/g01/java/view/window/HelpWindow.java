package be.ac.ulb.infof307.g01.java.view.window;

import java.io.IOException;

import static be.ac.ulb.infof307.g01.java.Constants.HTML_HELP_PATH;

/**
 * Cette classe représente la fenêtre d'aide.
 * Elle contient le texte d'aide et des explications
 * de l'utilisation des fonctionnalités présentes 
 * dans l'application.
 */
public class HelpWindow extends PopUpWindow {

    public static void display() throws IOException {
        display("Help", HTML_HELP_PATH);
    }
}
