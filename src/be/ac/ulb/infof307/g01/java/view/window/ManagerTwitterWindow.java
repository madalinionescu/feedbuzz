package be.ac.ulb.infof307.g01.java.view.window;

import be.ac.ulb.infof307.g01.java.view.controller.ManagerTwitterWindowController;
import be.ac.ulb.infof307.g01.java.view.listener.DashboardViewListener;
import javafx.stage.Stage;

import static be.ac.ulb.infof307.g01.java.Constants.FXML_MANAGER_TWITTER_PATH;

/**
 * Fenêtre qui gère les accounts et hastags twitter à follow/unfollow
 */
public class ManagerTwitterWindow extends Window<DashboardViewListener> {

    private final DashboardViewListener listener;

    public ManagerTwitterWindow(DashboardViewListener listener) {this.listener = listener;}

    public void display() throws Exception {
        super.display(FXML_MANAGER_TWITTER_PATH, "Twitter manager", 430, 350, false);
        Stage managerTwitterWindow = super.primaryStage;
        setListener(listener);
        super.getLoader().<ManagerTwitterWindowController>getController().initializeTwitterWindow();
        managerTwitterWindow.showAndWait();
    }
}
