package be.ac.ulb.infof307.g01.java.view.controller;

import be.ac.ulb.infof307.g01.java.model.LocalUser;
import be.ac.ulb.infof307.g01.java.model.Tag;
import be.ac.ulb.infof307.g01.java.model.User;
import be.ac.ulb.infof307.g01.java.view.listener.DashboardViewListener;
import be.ac.ulb.infof307.g01.java.view.listener.MainListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import java.util.ArrayList;
import java.util.List;


/**
 * Classe représentant le controlleur de la fenêtre de gestion des Tags
 * d'un Article
 */
public class ManagerUserTagWindowController extends ViewController<DashboardViewListener>{
    @FXML private ListView userTagsListView;
    @FXML private TextField tagNameTextField;

    private MainListener mainListener;
    private User user;
    private List<Tag> userTags = new ArrayList<>();

    /**
     * Méthode éxecutée lors du lancement de la fenetre
     * Declaration du listener + refresh des différents elements de la fenetre
     */
    public void initializeUserTagWindow() {
        mainListener = listener.getMainListener();
        user = LocalUser.singleton.getUser();
        userTags = user.getUserTags();
        refreshUserTagsListView();
    }

    /**
     * Rafraichit la ListView contenant tous les tags de l'utilisateur
     * connecté
     */
    private void refreshUserTagsListView(){
        List<Tag> userTags = user.getUserTags();
        List<String> userTagsAsString = new ArrayList<>();
        for (Tag tag: userTags){
            userTagsAsString.add(tag.getName());
        }
        ObservableList<String> items = FXCollections.observableArrayList (userTagsAsString);
        userTagsListView.setItems(items);
    }

    /**
     * Methode executée lors de la pression du boutton Delete
     * Supprime le Tag selectionné dans la ListView, et met
     * à jour l'utilisateur
     */
    public void deleteTagButtonClick(){
        Tag toRemove = new Tag((String) userTagsListView.getSelectionModel().getSelectedItem());
        userTags.remove(toRemove);
        user.setUserTags(userTags);
        mainListener.updateUser(user);
        refreshUserTagsListView();
    }

    /**
     * Methode executée lors de la pression sur le boutton Add
     * Cree un nouveau Tag pour l'utilisateur connecté
     */
    public void addTagButtonClick(){
        if(tagNameTextField.getText().trim().isEmpty())
            return;
        Tag toAdd = new Tag(tagNameTextField.getText());
        tagNameTextField.clear();
        if (userTags.contains(toAdd)) return;
        userTags.add(toAdd);
        user.setUserTags(userTags);
        mainListener.updateUser(user);
        refreshUserTagsListView();
    }
}
