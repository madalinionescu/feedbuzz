package be.ac.ulb.infof307.g01.java.view.window;

import be.ac.ulb.infof307.g01.java.exception.DisplayableException;
import javafx.scene.control.Alert;

/**
 * Fenêtre d'alerte (généralement en popup quand il y a une erreur)
 */
public abstract class AlertWindow {
    Alert alert;

    public void display(String windowTitle, String title, String message) {
        alert.setTitle(windowTitle);
        alert.setHeaderText(title);
        alert.setContentText(message);

        alert.showAndWait();
    }

    public void display(DisplayableException e) {
        display(e.getTitle(), e.getTitle(), e.getMessage());
    }
}
