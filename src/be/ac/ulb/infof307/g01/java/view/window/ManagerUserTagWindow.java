package be.ac.ulb.infof307.g01.java.view.window;


import be.ac.ulb.infof307.g01.java.view.controller.ManagerUserTagWindowController;
import be.ac.ulb.infof307.g01.java.view.listener.DashboardViewListener;
import javafx.stage.Stage;

import static be.ac.ulb.infof307.g01.java.Constants.*;


/**
 * Fene^tre qui gère les tags de l'user
 */
public class ManagerUserTagWindow extends Window<DashboardViewListener> {
    private final DashboardViewListener listener;

    public ManagerUserTagWindow(DashboardViewListener listener) {
        this.listener = listener;
    }

    /**

     */
    public void display() throws Exception {
        super.display(FXML_USERTAG_PATH, "User Tag Manager", 663, 400, false);
        Stage articleTagWindow = super.primaryStage;
        setListener(listener);
        super.getLoader().<ManagerUserTagWindowController>getController().initializeUserTagWindow();
        articleTagWindow.showAndWait();
    }
}
