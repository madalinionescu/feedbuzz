package be.ac.ulb.infof307.g01.java.view.controller;

import be.ac.ulb.infof307.g01.java.controller.MainController;
import be.ac.ulb.infof307.g01.java.model.Article;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe représentant un controlleur pour la corbeille d'articles supprimés
 */
public class TrashWindowController extends ViewController<MainController>{

    @FXML
    private ListView<Article> listView;
    private List<Article> articles;
    private List<Article> selectArticles;

    @FXML
    public void initializeTrash() {
        reloadListView();
    }

    /**
     * Met à jour l'affichage de la liste d'articles
     */
    private void reloadListView(){
        selectArticles = new ArrayList<>();
        listView.getItems().clear();
        articles = listener.getRecycleBinArticles();
        listView.getItems().addAll(articles);

        listView.setCellFactory(lv -> new ListCell<Article>()
        {
            final Label label = new Label();
            final CheckBox checkBox = new CheckBox();


            @Override
            public void updateItem(Article item, boolean empty) {
                super.updateItem(item, empty);
                if(item!=null && !empty){
                    boolean selected = selectArticles.contains(item);

                    if(selected) {
                        label.setTextFill(Color.BLUE);
                    }
                    else {
                        label.setTextFill(Color.BLACK);
                    }

                    label.setText(item.getDate() + " | " + item.getName() +" | " + item.getLink());
                    HBox container = new HBox();
                    container.setMouseTransparent(true);
                    checkBox.setDisable(true);
                    checkBox.setSelected(selected);
                    container.getChildren().add(checkBox);
                    container.getChildren().add(label);
                    setGraphic(container);
                }
                else{
                    setGraphic(null);
                }
            }
        });

    }

    /**
     * Selection d'un article supprimé
     */
    public void itemClick(){
        if (listView.getSelectionModel().getSelectedItem() != null) {
            Article selectArticle = listView.getSelectionModel().getSelectedItem();

            if(selectArticles.contains(selectArticle)){
                selectArticles.remove(selectArticle);
            }
            else{
                selectArticles.add(selectArticle);
            }
            listView.getSelectionModel().clearSelection();
            listView.getItems().clear();
            listView.getItems().addAll(articles);
        }
    }

    public void pressSelectAll(){
        for(Article a: articles){
            if(!selectArticles.contains(a))
                selectArticles.add(a);
        }
        listView.getItems().clear();
        listView.getItems().addAll(articles);
    }

    public void pressUndo(){
        for(Article article: selectArticles){
            article.setTrash(false);
            listener.updateArticle(article);
        }
        reloadListView();
    }

    public void pressDelete(){
        for(Article article: selectArticles){
            listener.deleteArticleFromTrash(article);
        }
        reloadListView();
    }
}
