package be.ac.ulb.infof307.g01.java.view.controller;

import be.ac.ulb.infof307.g01.java.controller.ControllerConstants;
import be.ac.ulb.infof307.g01.java.view.listener.MainListener;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Controller pour la feneêtre d'interets
 */
public class InterestsWindowController extends ViewController<MainListener> {
    @FXML private ToggleButton newsButton, sportButton, techButton, businessButton, politicsButton,
            gamingButton, artButton, scienceButton, othersButton;
    @FXML private ToggleButton n1, n2, n3, n4, n5, n6, sp1, t1, t2, b1, p1, g1, g2, a1, sc1, o1;
    @FXML private Button closeButton;

    private final HashMap<ToggleButton,List<ToggleButton>> categories = new HashMap<>();
    private final HashMap<ToggleButton, String> RSSs = new HashMap<>();

    /**
     * Initialise la fenêtre de choix d'intéret
     */
    @FXML @SuppressWarnings("Duplicates")
    public void initialize() {
        List<ToggleButton> news = new ArrayList<>(Arrays.asList(n1,n2,n3,n4,n5,n6));
        categories.put(newsButton,news);

        List<ToggleButton> sport = new ArrayList<>(Arrays.asList(sp1));
        categories.put(sportButton,sport);

        List<ToggleButton> tech = new ArrayList<>(Arrays.asList(t1,t2));
        categories.put(techButton,tech);

        List<ToggleButton> business = new ArrayList<>(Arrays.asList(b1));
        categories.put(businessButton,business);

        List<ToggleButton> politics = new ArrayList<>(Arrays.asList(p1));
        categories.put(politicsButton,politics);

        List<ToggleButton> gaming = new ArrayList<>(Arrays.asList(g1,g2));
        categories.put(gamingButton,gaming);

        List<ToggleButton> art = new ArrayList<>(Arrays.asList(a1));
        categories.put(artButton,art);

        List<ToggleButton> science = new ArrayList<>(Arrays.asList(sc1));
        categories.put(scienceButton,science);

        List<ToggleButton> others = new ArrayList<>(Arrays.asList(o1));
        categories.put(othersButton,others);

        RSSs.put(n1, ControllerConstants.NEWS_BBC);
        RSSs.put(t1, ControllerConstants.TECHNOLOGY_TECHRADAR);
        RSSs.put(sc1, ControllerConstants.SCIENCE_SCIENCEDAILY);
        RSSs.put(n2, ControllerConstants.NEWS_LALIBRE);
        RSSs.put(o1,"");
        RSSs.put(n3,"");
        RSSs.put(n4,"");
        RSSs.put(sp1,"");
        RSSs.put(a1,"");
        RSSs.put(n5,"");
        RSSs.put(t2,"");
        RSSs.put(b1,"");
        RSSs.put(p1,"");
        RSSs.put(n6,"");
        RSSs.put(g1,"");
        RSSs.put(g2,"");
    }

    /**
     * Selectionne/déselectionne automatiquement les sources associées à cette catégorie
     * @param category Categorie séléctionnée
     */
    private void toggleCategory(ToggleButton category) {
        List<ToggleButton> sources = categories.get(category);
        for (ToggleButton source : sources) {
            source.setSelected(category.isSelected());
        }
    }

    /**
     * Séléctionne/déselectionne automatiquement les catégories associés à cette source
     * @param source Source séléctionnée
     */
    private void toggleSource(ToggleButton source) {
        ToggleButton category = newsButton;
        for (ToggleButton cat : categories.keySet()) {
            for (ToggleButton src : categories.get(cat)) {
                if (src == source) {
                    category = cat;
                }
            }
        }
        boolean availableSource = false;
        for (ToggleButton src : categories.get(category)) {
            availableSource = availableSource || src.isSelected();
        }
        category.setSelected(availableSource);
    }

    /**
     * Action lors du clic sur une catégorie
     * @param event evenement appelant la méthode
     */
    public void categoryButtonPressed(Event event) {
        toggleCategory((ToggleButton) event.getSource());
    }

    /**
     * Action lors du clic sur une source
     * @param event
     */
    public void sourceButtonPressed(Event event) {
        toggleSource((ToggleButton) event.getSource());
    }

    /**
     * Lors de la confirmation du choix des interests ceux-ci sont ajouter à l'utilisateur local
     */
    public void setInterests() {
        for (ToggleButton interest : categories.keySet()) {
            if (interest.isSelected()) {
                String name = interest.getText();
                listener.setInterest(name);
            }

            for(ToggleButton rss: categories.get(interest)){
                if(rss.isSelected()){
                    String rssLink = RSSs.get(rss);
                    listener.addRSS(rssLink);
                }
            }
        }
        handleCloseButtonAction();
    }

    /**
     * Fermeture de la fenetre d'interet
     */
    @FXML private void handleCloseButtonAction() {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
}
