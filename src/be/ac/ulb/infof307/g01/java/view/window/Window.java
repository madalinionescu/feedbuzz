package be.ac.ulb.infof307.g01.java.view.window;

import be.ac.ulb.infof307.g01.java.view.controller.ViewController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

import static be.ac.ulb.infof307.g01.java.Constants.IMG_LOGO_PATH;

/**
 * Class abstraite, mère de toutes les windows
 * The sub views have two listeners
 * The prime listener is the one though which they connect to the models
 * @param <PrimeListener>
 */
public abstract class Window<PrimeListener> {
    Stage primaryStage;
    FXMLLoader loader;

    void display(String fxmlPath, String title,
                 Integer minWidth, Integer minHeight, boolean resizable) throws Exception {

        loader = new FXMLLoader(getClass().getResource(fxmlPath));

        Parent root = loader.load();

        primaryStage = new Stage();
        primaryStage.initModality(Modality.APPLICATION_MODAL);
        primaryStage.setTitle(title);

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);

        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream(IMG_LOGO_PATH)));

        primaryStage.setResizable(resizable);

        primaryStage.setMinWidth(minWidth);
        primaryStage.setMinHeight(minHeight);
    }

    FXMLLoader getLoader() {
        return loader;
    }

    void setListener(PrimeListener listener) {
        getLoader().<ViewController>getController().setListener(listener);
    }
}
