package be.ac.ulb.infof307.g01.java.view.controller;

import be.ac.ulb.infof307.g01.java.model.LocalUser;
import be.ac.ulb.infof307.g01.java.model.User;
import be.ac.ulb.infof307.g01.java.view.listener.MainListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import java.util.List;

public class ProfileWindowController  extends ViewController<MainListener> {
    @FXML private Label usernameLabel;
    @FXML private Label emailLabel;
    @FXML private ListView interestsListView;
    @FXML private TextField addInterestTextField;

    public void initialize() {
        updateInfos();
    }

    public void addInterestButtonClicked() {
        if (addInterestTextField.getText().trim().isEmpty()) return;
        User usr = LocalUser.singleton.getUser();
        usr.addUserInterest(addInterestTextField.getText());
        listener.updateUser(usr);
        updateInfos();
        addInterestTextField.setText("");
    }

    public void deleteInterestButtonClicked() {
        User usr = LocalUser.singleton.getUser();
        String interest = (String) interestsListView.getSelectionModel().getSelectedItem();
        usr.deleteUserInterest(interest);
        listener.updateUser(usr);

        updateInfos();
    }

    /**
     * Màj des infos (ajout/delete d'un interest)
     */
    private void updateInfos() {
        User usr = LocalUser.singleton.getUser();

        usernameLabel.setText(usr.getUsername());
        emailLabel.setText(usr.getMail());

        List<String> interests = usr.getUserInterests();

        if (interests != null) {
            ObservableList<String> items = FXCollections.observableArrayList (interests);
            interestsListView.setItems(items);
        }
    }




}
