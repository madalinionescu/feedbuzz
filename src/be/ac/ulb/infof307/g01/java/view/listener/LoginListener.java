package be.ac.ulb.infof307.g01.java.view.listener;

import be.ac.ulb.infof307.g01.java.exception.UserExistsException;
import be.ac.ulb.infof307.g01.java.exception.UserNotFoundException;

/**
 * Listener pour les fonctions liées à la connexion
 */
public interface LoginListener {

    void signIn(String username, String password) throws UserNotFoundException;

    void signUp(String username, String email, String password) throws UserExistsException;

    void signOut();

    boolean validatePassword(String username, String password);

    boolean validateEmail(String email);

    boolean confirmPassword(String password, String confirmation);

    void displayTermCond();

    void setInterest(String interestName);
}