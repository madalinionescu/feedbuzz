package be.ac.ulb.infof307.g01.java.view.window;

import be.ac.ulb.infof307.g01.java.view.controller.DashboardWindowController;
import be.ac.ulb.infof307.g01.java.view.listener.MainListener;
import javafx.stage.Stage;

import static be.ac.ulb.infof307.g01.java.Constants.FXML_DASHBOARD_PATH;

/**
 * Dashboard de l'application
 */
public class DashboardWindow extends Window<MainListener> {
    private final MainListener listener;

    public DashboardWindow(MainListener listener) {
        this.listener = listener;
    }

    public void display() throws Exception {
        super.display(FXML_DASHBOARD_PATH, "FeedBuzz Home", 1000, 700, true);
        Stage windowDashboard = super.primaryStage;
        setListener(listener);
        super.getLoader().<DashboardWindowController>getController().initializeDashboard();
        windowDashboard.show();
    }
}
