package be.ac.ulb.infof307.g01.java.view.controller;

import be.ac.ulb.infof307.g01.java.model.Article;
import be.ac.ulb.infof307.g01.java.model.LocalUser;
import be.ac.ulb.infof307.g01.java.model.Tag;
import be.ac.ulb.infof307.g01.java.model.User;
import be.ac.ulb.infof307.g01.java.view.listener.DashboardViewListener;
import be.ac.ulb.infof307.g01.java.view.listener.MainListener;
import be.ac.ulb.infof307.g01.java.view.window.ManagerUserTagWindow;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static be.ac.ulb.infof307.g01.java.Constants.TAG_NAME_FINISHED;
import static be.ac.ulb.infof307.g01.java.Constants.TAG_NAME_NOT_READ;
import static be.ac.ulb.infof307.g01.java.Constants.TAG_NAME_READING;


/**
 * Classe représentant le controlleur de la fenêtre de gestion des Tags
 * d'un Article
 */
public class ManagerArticleTagWindowController extends ViewController<DashboardViewListener>{
    @FXML private ListView articleTagsListView;
    @FXML private ComboBox userTagsComboBox;

    private MainListener mainListener;
    private Article currentArticle;
    private final List<String> blacklistedTags = Arrays.asList(TAG_NAME_NOT_READ, TAG_NAME_READING, TAG_NAME_FINISHED);

    /**
     * Méthode éxecutée lors du lancement de la fenetre
     * Declaration du listener et de l'article courrant + refresh des différents elements de la fenetre
     */
    public void initializeArticleTagWindow(Article article) {
        mainListener = listener.getMainListener();
        currentArticle = article;

        refreshAll();
    }

    /**
     * Rafraichit la ListView contenant tous les Tags
     * de l'article courrant
     */
    private void refreshArticleTagsListView(){
        List<Tag> articleTags = currentArticle.getTags();
        List<String> articleTagsAsString = new ArrayList<>();
        for (Tag tag : articleTags){
            if(!blacklistedTags.contains(tag.getName())) {
                articleTagsAsString.add(tag.getName());
            }
        }
        ObservableList<String> items = FXCollections.observableArrayList (articleTagsAsString);
        articleTagsListView.setItems(items);
    }

    /**
     * Rafraichit la ComboBox contenant tous les Tags
     * de l'utilisateur courrant
     */
    private void refreshUserTagComboBox(){
        User user = LocalUser.singleton.getUser();
        List<Tag> userTags = user.getUserTags();
        List<Tag> articleTags = currentArticle.getTags();
        List<String> userTagsAsString = new ArrayList<>();
        for (Tag tag: userTags){
            if(!articleTags.contains(tag)){
                userTagsAsString.add(tag.getName());
            }
        }
        ObservableList<String> items = FXCollections.observableArrayList (userTagsAsString);
        userTagsComboBox.setItems(items);
        userTagsComboBox.getSelectionModel().selectFirst();

    }

    /**
     * Méthode executée lors de la pression sur le boutton Add
     * Ajoute un des tags de l'user à l'article et
     * rafraichit les éléments
     */
    public void addTagButtonClick(){
        if (!userTagsComboBox.getSelectionModel().isEmpty()){
            this.currentArticle.addTag((String) userTagsComboBox.getValue());
        }

        refreshAll();

        mainListener.updateArticle(currentArticle);
    }

    /**
     * Méthode executée lors de la pression sur le boutton Remove
     * Retire un Tag de l'article et rafraichit
     * les éléments
     */
    public void removeTagButtonClick(){
        this.currentArticle.removeTag((String) articleTagsListView.getSelectionModel().getSelectedItem());

        refreshAll();

        mainListener.updateArticle(currentArticle);
    }

    /**
     * Méthode executée lors de la pression sur le boutton Create New ..
     *  Ouvre la fenetre de gestion des tags de l'utilisateur courrant
     */
    public void createNewTagsButtonClick() throws Exception {
        ManagerUserTagWindow managerUserTagWindow = new ManagerUserTagWindow(super.listener);
        managerUserTagWindow.display();

        refreshAll();
    }

    /**
     * Rafraichit tous les éléments de la fentre courrante
     */
    private void refreshAll(){
        refreshArticleTagsListView();
        refreshUserTagComboBox();
    }
}
