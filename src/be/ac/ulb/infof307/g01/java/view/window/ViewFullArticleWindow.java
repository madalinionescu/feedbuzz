package be.ac.ulb.infof307.g01.java.view.window;

import java.io.IOException;

/**
 * Fenêtre où on peut lire l'article en entier (en ligne ou hors ligne)
 */
public class ViewFullArticleWindow extends PopUpWindow{

    public static void display(String title, String url) throws IOException {
        PopUpWindow.display(title, url);
    }
}