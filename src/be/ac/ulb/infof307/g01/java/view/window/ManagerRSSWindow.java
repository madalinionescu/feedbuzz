package be.ac.ulb.infof307.g01.java.view.window;


import be.ac.ulb.infof307.g01.java.view.controller.ManagerRSSWindowController;
import be.ac.ulb.infof307.g01.java.view.listener.DashboardViewListener;
import javafx.stage.Stage;

import static be.ac.ulb.infof307.g01.java.Constants.*;


/**
 * Cette classe représente la fenêtre d'importation d'articles via des flux RSS
 * Elle est appelée en cliquant sur 'add RSS' dans l'interface principale.
 * Une fois l'url d'un flux RSS indiqué, des options s'offrent à nous :
 * choisir le nombre d'articles et les informations voulues.
 * Un aperçu des articles est proposé puis le bouton confirmer va placer
 * les articles choisis dans la database.
 */
public class ManagerRSSWindow extends Window<DashboardViewListener> {
    private final DashboardViewListener listener;

    public ManagerRSSWindow(DashboardViewListener listener) {
        this.listener = listener;
    }

    /**
     * Affiche la fenêtre d'importation de flux RSS
     * La fenêtre contient un comboBox pour l'url,
     * des checkBox pour les choix des informations,
     * un spinner pour le nombre d'articles,
     * un textarea pour la preview et des boutons pour
     * confirmer, submit le lien
     */
    public void display() throws Exception {
        super.display(FXML_IMPORTRSS_PATH, "RSS Manager", 670, 600, false);
        Stage rssWindow = super.primaryStage;
        setListener(listener);
        super.getLoader().<ManagerRSSWindowController>getController().initializeRssWindow();
        rssWindow.showAndWait();
    }
}
