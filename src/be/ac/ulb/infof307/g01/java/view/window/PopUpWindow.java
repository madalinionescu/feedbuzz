package be.ac.ulb.infof307.g01.java.view.window;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.net.URL;

import java.io.IOException;

import static be.ac.ulb.infof307.g01.java.Constants.IMG_LOGO_PATH;

/**
 * Classe abstraite, mère de toutes les windows popup
 * qui s'affichent au-dessus du dashboard
 */
public abstract class PopUpWindow {

    private static WebView webView;

    /**
     * Affiche la fenêtre pop-up qui peut représenter par exemple
     * une fenêtre d'aide ou de "à propos"
     * @param title un title (string)
     * @param filePath un PATH vers le fichier contenant le texte de la fenetre (string)
     * @throws IOException si l'affichage de la fenêtre ne s'est pas bien passé
     */
    public static void display(String title, String filePath) throws IOException {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(500);
        window.setHeight(600);
        window.getIcons().add(new Image(PopUpWindow.class.getResourceAsStream(IMG_LOGO_PATH)));

        createWebView(filePath);

        Scene scene = new Scene(webView);
        window.setScene(scene);
        window.showAndWait();
    }

    /**
     * Crée un objet textarea dans lequel se trouvera du texte lié
     * au contenu de la fenêtre
     */
    private static void createWebView(String filePath) {
        webView = new WebView();
        webView.setPrefWidth(480);
        webView.setPrefHeight(550);
        if (filePath.contains("http")){
            if (isValidURL((filePath))){
                webView.getEngine().load(filePath);
            }
            else {
                webView.getEngine().loadContent(filePath);
            }
        }
        else {
            webView.getEngine().load(PopUpWindow.class.getResource(filePath).toString());
        }
    }

    private static boolean isValidURL(String urlString) {
        try {
            URL url = new URL(urlString);
            url.toURI();
            return true;
        } catch (Exception exception) {
            return false;
        }
    }
}
