package be.ac.ulb.infof307.g01.java.view.window;

import be.ac.ulb.infof307.g01.java.view.listener.MainListener;
import javafx.stage.Stage;

import static be.ac.ulb.infof307.g01.java.Constants.FXML_INTERESTS_PATH;

/**
 * Fenêtre de choix d'interets
 */
public class InterestsWindow extends Window<MainListener> {
    private final MainListener listener;

    public InterestsWindow(MainListener listener) {
        this.listener = listener;
    }

    public void display() throws Exception {
        super.display(FXML_INTERESTS_PATH, "Interests", 600, 440, false);
        Stage interestsStage = super.primaryStage;
        setListener(listener);
        interestsStage.showAndWait();
    }
}
