package be.ac.ulb.infof307.g01.java.view.window;

import be.ac.ulb.infof307.g01.java.view.controller.TrashWindowController;
import be.ac.ulb.infof307.g01.java.view.listener.MainListener;
import javafx.stage.Stage;

import static be.ac.ulb.infof307.g01.java.Constants.FXML_TRASH_PATH;

/**
 * Fene^tre où on peut récupérer (ou supprier pour toujours) des articles supprimés
 */
public class TrashWindow extends Window<MainListener> {
    private final MainListener listener;


    public TrashWindow(MainListener listener){
        this.listener = listener;
    }

    public void display() throws Exception {
        super.display(FXML_TRASH_PATH, "FeedBuzz trash", 600, 400, true);
        Stage trashWindow = super.primaryStage;
        setListener(listener);
        super.getLoader().<TrashWindowController>getController().initializeTrash();
        trashWindow.showAndWait();
    }
}
