package be.ac.ulb.infof307.g01.java.view.window;

import be.ac.ulb.infof307.g01.java.view.listener.MainListener;
import javafx.stage.Stage;

import static be.ac.ulb.infof307.g01.java.Constants.FXML_PROFILE_PATH;


/**
 * Cette classe représente la fenêtre de consultation de profile
 * Elle contient les informations relatives à l'utilisateurs
 * et le laisse en modifier certaines
 */


public class ProfileWindow extends Window{

    private final MainListener listener;

    public ProfileWindow(MainListener listener) {this.listener = listener;}

    public void display() throws Exception {
        super.display(FXML_PROFILE_PATH, "Profile", 430,350, true);
        Stage profileWindow = super.primaryStage;
        setListener(listener);
        profileWindow.showAndWait();
    }
}
