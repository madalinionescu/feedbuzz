package be.ac.ulb.infof307.g01.java.view.window;

import be.ac.ulb.infof307.g01.java.view.listener.MainListener;
import javafx.stage.Stage;

import static be.ac.ulb.infof307.g01.java.Constants.FXML_LAUNCHER_PATH;

/**
 * Fene^tre de connexion/inscription
 */
public class LauncherWindow extends Window<MainListener> {
    private final MainListener listener;

    public LauncherWindow(MainListener listener) {
        this.listener = listener;
    }

    public void display() throws Exception {
        super.display(FXML_LAUNCHER_PATH,"Welcome to FeedBuzz",
                700,600,false);
        Stage launcherStage = super.primaryStage;
        setListener(listener);
        launcherStage.show();
    }
}
