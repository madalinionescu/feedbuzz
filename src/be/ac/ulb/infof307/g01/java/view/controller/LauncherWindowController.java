package be.ac.ulb.infof307.g01.java.view.controller;

import be.ac.ulb.infof307.g01.java.exception.UserExistsException;
import be.ac.ulb.infof307.g01.java.exception.UserNotFoundException;
import be.ac.ulb.infof307.g01.java.view.listener.MainListener;
import be.ac.ulb.infof307.g01.java.view.window.ErrorWindow;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * Classe représentant le controlleur du launcher de la fenêtre principale
 */
public class LauncherWindowController extends ViewController<MainListener> {

    @FXML private Pane signInArea, signUpArea;
    @FXML private TextField usernameId, emailId, passwordId, confirmPasswordId;
    @FXML private TextField emailLoginId, passwordLoginId;
    @FXML private Button signUpButton, signInButton;
    @FXML private Label usernameChecker, passwordChecker, emailChecker, confirmPasswordChecker;
    @FXML private CheckBox termsAgreeButton;

    private boolean emailOk = false;
    private boolean passOK = false;

    /**
     * Change l'aspect d'un textField pour indiquer si son contenu est valide.
     * @param textField l'élément qui doit être changé
     * @param valid: true si le contenu e valide, false sinon
     */
    private void validateTextField(TextField textField, boolean valid) {
        if (valid) {
            textField.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(2), new BorderWidths(0))));
        } else {
            textField.setBorder(new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID, new CornerRadii(2), new BorderWidths(1))));
        }
    }

    /**
     * Affiche un petit message d'information pour dire que le champ
     * est correctement (ou pas) rempli
     */
    private void displayInputValidity(TextField field, Label checker, String message, boolean validity) {
        checker.setText(message);
        if (validity) {
            validateTextField(field, true);
            checker.setStyle("-fx-text-fill: green;" +
                    "-fx-font-size: 10px;");
        }
        else {
            validateTextField(field, false);
            checker.setStyle("-fx-text-fill: red;" +
                    "-fx-font-size: 10px;");
        }
    }

    /**
     * Switch entre signIn et signUp
     */
    @FXML private void switchPane() {
        signInArea.setVisible(!signInArea.isVisible());
        signUpArea.setVisible(!signUpArea.isVisible());
    }

    /**
     * Lorsque le bouton Sign In est cliqué, le nom d'utilisateur et le mot de passe
     * introduits par l'utilisateur sont verifiés.
     * Si OK -> la fenêtre principale de l'application est affichée
     * Sinon, la fenêtre d'erreur est affichée
     */
    public void signInButtonClicked() {
        String password = passwordLoginId.getText();
        String username = emailLoginId.getText();

        try {
            listener.signIn(username,password);
            handleCloseButtonAction();
        } catch (UserNotFoundException e) {
            ErrorWindow error = new ErrorWindow();
            error.display(e);
        }
    }

    /**
     * lorsque le bouton Sign Up est cliqué, les données introduites par l'utilisateur sont verifiées.
     * Si OK -> le nouvel utilisateur est enregistré dans la base de données
     * Sinon, un displayInputValidity est affiché
     */
    public void signUpButtonClicked() {
        String username = usernameId.getText();
        String email = emailId.getText();
        String password = passwordId.getText();
        String confirmPassword = confirmPasswordId.getText();
        if (!password.equals(confirmPassword)) return;

        try {
            listener.signUp(username, email, password);
            switchPane();
        } catch (UserExistsException e) {
            ErrorWindow error = new ErrorWindow();
            error.display(e);
            displayInputValidity(usernameId, usernameChecker, "Account already exist", false);
        }
    }

    public void termCondClicked() {
        listener.displayTermCond();
    }

    /**
     * Appele le contrôleur principal pour vérifier que le mot de passe
     * est conforme au fur et à mesure que l'utilisateur le tape et
     * modifie l'affichage en conséquence.
     */
    public void validatePassword() {
        passOK = listener.validatePassword(usernameId.getText(),passwordId.getText());
        if (passOK) {
            displayInputValidity(passwordId, passwordChecker, "Strong password", true);
        } else {
            displayInputValidity(passwordId, passwordChecker, "Weak password", false);
        }
        checkSignUpPossible();
    }

    /**
     * Appele le contrôleur principal pour vérifier que l'adresse mail
     * correspond bien aux demandes au fur et à mesure que l'utilisateur
     * le tape et modifie l'affichage en conséquence.
     */
    public void validateEmail() {
        emailOk = listener.validateEmail(emailId.getText());
        if (emailOk) {
            displayInputValidity(emailId, emailChecker, "Valid email", true);
        } else {
            displayInputValidity(emailId, emailChecker, "Invalid email", false);
        }
        checkSignUpPossible();
    }

    /**
     * Appele le contrôleur principal pour vérifier si le mot de
     * passe est confirmé et modifie l'affichage en conséquence.
     */
    public void confirmPassword() {
        boolean confirmOk = listener.confirmPassword(passwordId.getText(), confirmPasswordId.getText());
        if (confirmOk) {
            displayInputValidity(confirmPasswordId, confirmPasswordChecker, "Password confirmed", true);
        }
        else {
            displayInputValidity(confirmPasswordId, confirmPasswordChecker, "Different password!", false);
        }
        checkSignUpPossible();
    }

    /**
     * Modifie l'affichage pour permettre à l'utilisateur de se enregistrer
     * si et seulement si toutes les informations nécessaires sont
     * bien complétées et les conditions sont acceptées.
     */
    @FXML private void checkSignUpPossible() {
        signUpButton.setDisable(!(emailOk && passOK && termsAgreeButton.isSelected() && !usernameId.getText().isEmpty()));
    }

    /**
     * Modifie l'affichage pour effacer l'erreur du nom d'utilisateur
     * existant quand l'utilisateur essaie d'enregistrer un nom déjà pris.
     */
    public void typingUsername() {
        displayInputValidity(usernameId, usernameChecker, "", true);
        checkSignUpPossible();
    }

    private void handleCloseButtonAction() {
        Stage stage = (Stage) signInButton.getScene().getWindow();
        stage.close();
    }
}