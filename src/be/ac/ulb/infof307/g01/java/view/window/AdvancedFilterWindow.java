package be.ac.ulb.infof307.g01.java.view.window;

import be.ac.ulb.infof307.g01.java.view.controller.AdvancedFilterWindowController;
import be.ac.ulb.infof307.g01.java.view.controller.DashboardWindowController;
import be.ac.ulb.infof307.g01.java.view.listener.MainListener;
import javafx.stage.Stage;

import static be.ac.ulb.infof307.g01.java.Constants.FXML_ADVANCEDFILTER_PATH;

/**
 * Fenêtre où on peut chercher plus précisement (avec + des filtres)
 */
public class AdvancedFilterWindow extends Window<MainListener> {
    private DashboardWindowController parentWindowController = null;
    private final MainListener listener;

    public AdvancedFilterWindow(MainListener listener) { this.listener = listener; }

    /**
     * Affiche la fenêtre d'importation de flux RSS
     * La fenêtre contient un comboBox pour l'url,
     * des checkBox pour les choix des informations,
     * un spinner pour le nombre d'articles,
     * un textarea pour la preview et des boutons pour
     * confirmer, submit le lien
     */
    public void display() throws Exception {
        super.display(FXML_ADVANCEDFILTER_PATH, "Advanced search", 430,350, true);
        Stage filterWindow = super.primaryStage;
        AdvancedFilterWindowController controller = super.loader.getController();
        controller.setParentWindowController(parentWindowController);
        setListener(listener);
        super.getLoader().<AdvancedFilterWindowController>getController().initializeAdavancedSearch();
        filterWindow.show();
    }

    /**
     * Permets désigner le contrôleur de la fenêtre parente
     * pour pouvoir actualiser les informations dans la
     * fenêtre principale quand la recherche est lancée,
     * sans fermer la fenêtre de recherche.
     * @param parentController est le contrôleur du parent
     */
    public void setParentWindowController(DashboardWindowController parentController) {
        parentWindowController = parentController;
    }
}
