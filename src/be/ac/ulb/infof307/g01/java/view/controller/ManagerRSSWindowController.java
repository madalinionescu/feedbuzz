package be.ac.ulb.infof307.g01.java.view.controller;

import be.ac.ulb.infof307.g01.java.model.Article;
import be.ac.ulb.infof307.g01.java.model.InformationRSS;
import be.ac.ulb.infof307.g01.java.model.ParserRSS;
import be.ac.ulb.infof307.g01.java.utils.ThreadJavaFx;
import be.ac.ulb.infof307.g01.java.view.listener.DashboardViewListener;
import be.ac.ulb.infof307.g01.java.view.listener.MainListener;
import be.ac.ulb.infof307.g01.java.view.window.ErrorWindow;
import be.ac.ulb.infof307.g01.java.view.window.ManagerTwitterWindow;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.util.*;

import static be.ac.ulb.infof307.g01.java.controller.ControllerConstants.*;


/**
 * Classe représentant le controlleur de la fenêtre de gestion des flux
 * RSS.
 */
public class ManagerRSSWindowController extends ViewController<DashboardViewListener>{
    private ParserRSS parserRSS;
    private List<Article> articles;
    private final Map<String, String> urlToXmlMap = new HashMap<>();

    @FXML private ComboBox urlField;
    @FXML private TextArea textAreaPreview;
    @FXML private CheckBox checkBoxThumbnail;
    @FXML private CheckBox checkBoxLocation;
    @FXML private CheckBox checkBoxDate;
    @FXML private Spinner spinnerArticle;
    @FXML private Button buttonPreview;
    @FXML private Button buttonSubmit;
    @FXML private Button buttonConfirm;
    @FXML private Button buttonDeleteFeed;
    @FXML private ListView<String> informationRSSListView;

    private MainListener mainListener;

    public void initializeRssWindow() {
        mainListener = listener.getMainListener();

        // récupération des informationRSS
        List<InformationRSS> informationRSS = mainListener.getInformationRSS();
        List<String> links = new ArrayList<>();

        //initialise urlToXmlMap (bbc.com lié à http://feeds.bbci.co.uk/news/rss.xml par exemple)
        //et les liens par défaut dans la combobox (bbc.com, lesoir.be, etc)
        urlField.getItems().clear();
        List<String> websites = Arrays.asList(NEWS_BBC_WEBSITE, TECHNOLOGY_TECHRADAR_WEBSITE, NEWS_LALIBRE_WEBSITE,
                SCIENCE_SCIENCEDAILY_WEBSITE, NEWS_LESOIR_WEBSITE, NEWS_HACKADAY_WEBSITE);
        List<String> xmls = Arrays.asList(NEWS_BBC, TECHNOLOGY_TECHRADAR, NEWS_LALIBRE,
                SCIENCE_SCIENCEDAILY, NEWS_LESOIR, NEWS_HACKADAY);
        for(int i = 0; i < websites.size(); ++i){
           urlToXmlMap.put(websites.get(i), xmls.get(i));
           urlToXmlMap.put(xmls.get(i), websites.get(i));
           urlField.getItems().add(websites.get(i));
        }

        // affichage des feeds dans la liste
        for (InformationRSS info : informationRSS){
            links.add(getUrlFromXml(info.getLink()));
        }

        ObservableList<String> items = FXCollections.observableArrayList (links);
        informationRSSListView.setItems(items);
    }

    /**
     * Appelé lorsque l'utilisateur clique sur le bouton Submit.
     * Le parser RSS est créé et va parser le lien RSS fourni et
     * produire une liste d'articles.
     * Une fois cela fait, l'utilisateur pour choisir ses options et preview les articles
     */
    private void disableManagerButtonsWhenLoading(boolean status) {
        spinnerArticle.setDisable(status);
        buttonPreview.setDisable(status);
        buttonSubmit.setDisable(status);
        buttonConfirm.setDisable(status);
        buttonDeleteFeed.setDisable(status);
        urlField.setDisable(status);
    }

    /**
     * @return le xml d'un website donné (ou xml lui même)
     */
    private String parseUrlField(){
        String link = (String) urlField.getValue();
        return getXmlFromUrl(link);
    }

    private String getUrlFromXml(String xml){
        if (!xml.contains("http"))
            return xml;
        return parseUrlXml(xml);
    }

    private String getXmlFromUrl(String url){
        if (url.contains("http"))
            return url;
        return parseUrlXml(url);
    }

    /**
     * @param link lien rss/website
     * @return lien website du rss
     */
    private String parseUrlXml(String link){
        if (urlToXmlMap.containsKey(link))
            return urlToXmlMap.get(link);
        return link;
    }

    /**
     * Fait partie du travail en arrière plan du thread
     * Crée un parser de flux RSS
     */
    private void submitBackgroundWork() {
        parserRSS = new ParserRSS(new InformationRSS(parseUrlField(), 0, true, true, false), false); // creation parser RSS
        mainListener.updateRSSArticles(parserRSS);
        mainListener.updateInformationRSS(parserRSS);
    }

    /**
     * Importe les articles depuis le parser de flux RSS.
     */
    private void submitFinalWork() {
        articles = parserRSS.getArticles();            // list d'articles
        if (articles.size() == 0) {
            urlField.setBorder(new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID, new CornerRadii(2), new BorderWidths(2))));
        } else {
            urlField.setBorder(new Border(new BorderStroke(Color.TRANSPARENT, BorderStrokeStyle.SOLID, new CornerRadii(2), new BorderWidths(2))));
        }
        SpinnerValueFactory<Integer> valueFactory =
                new SpinnerValueFactory.IntegerSpinnerValueFactory(0, articles.size(), articles.size());

        spinnerArticle.setValueFactory(valueFactory);   // set min et max values
        buttonSubmit.setText("Submit");
        disableManagerButtonsWhenLoading(articles.size() == 0);
        buttonSubmit.setDisable(false);
        urlField.setDisable(false);
    }


    /**
     * Crée le thread permettant de simuler le chargement lors
     * de l'importation des articles du flux RSS.
     */
    private void createThread() {
        ThreadJavaFx thread = new ThreadJavaFx() {
            @Override
            protected void backgroundWork() {
                submitBackgroundWork();
            }

            @Override
            protected void finalWork()  { submitFinalWork(); }
        };
        thread.run();
    }

    /**
     * Désactive les boutons et met en place un chargement (dans un thread).
     */
    public void buttonSubmitClicked() {
        disableManagerButtonsWhenLoading(true);
        buttonSubmit.setText("Loading...");
        createThread();
    }

    /**
     * Appelé lorsque l'utilisateur clique sur le bouton
     * preview. Celui-ci va afficher les articles et les informations
     * choisies dans le textarea
     */

    public void buttonPreviewClicked() {
        int nbArticlesWanted = Integer.valueOf((spinnerArticle.getEditor().getText()));
        if (nbArticlesWanted > articles.size())
            nbArticlesWanted = articles.size();

        textAreaPreview.setText("");

        for (int i = 0; i < nbArticlesWanted; i++) { // preview article choisi
            textAreaPreview.setText(textAreaPreview.getText().concat("----------- Article " + (i+1) + "------------\n"));
            textAreaPreview.setText(textAreaPreview.getText().concat("Title : " + articles.get(i).getName() + "\n"));
            textAreaPreview.setText(textAreaPreview.getText().concat("Content : " + articles.get(i).getContent() + "\n"));
            textAreaPreview.setText(textAreaPreview.getText().concat("Author : " + articles.get(i).getAuthor() + "\n"));
            textAreaPreview.setText(textAreaPreview.getText().concat("Link : " + articles.get(i).getLink() + "\n"));
            textAreaPreview.setText(textAreaPreview.getText().concat("Source : " + articles.get(i).getSource() + "\n"));

            if (checkBoxLocation.isSelected()) {
                textAreaPreview.setText(textAreaPreview.getText().concat("Location : " + articles.get(i).getLocation() + "\n")); }

            if (checkBoxDate.isSelected()) {
                textAreaPreview.setText(textAreaPreview.getText().concat("Date : " + articles.get(i).getDate() + "\n")); }

            textAreaPreview.setText(textAreaPreview.getText().concat("\n"));
        }

        buttonConfirm.setDisable(false);
    }

    /**
     * Appelé lorsque l'utilisateur clique sur le bouton Confirm.
     * Celui-ci va sauver les articles dans la database et
     * quitter la fenêtre.
     */
    
    public void buttonConfirmClicked() {
        int spinnerValue = Integer.valueOf(spinnerArticle.getEditor().getText());
        if (spinnerValue > articles.size()) //check que l'user fasse pas nptquoi
            spinnerValue = articles.size();

        InformationRSS informationRSS = new InformationRSS(
                parseUrlField(),
                spinnerValue,
                checkBoxLocation.isSelected(),
                checkBoxDate.isSelected(),
                checkBoxThumbnail.isSelected());

        mainListener.saveInformationRSS(informationRSS);

        initializeRssWindow();
        resetButtons();

        listener.updateArticleList();
    }

    /**
     * Reset le state des boutons à leur état initial
     */
    private void resetButtons(){
        buttonConfirm.setDisable(true);
        buttonPreview.setDisable(true);
        spinnerArticle.setDisable(true);
        textAreaPreview.setText("");
        urlField.getEditor().setText("");
    }

    /**
     * Appelé lorsque l'utilisateur clique sur le bouton Delete Feed.
     * Celui-ci va voir quel article est sélectionné dans la ListView
     * et le delete de la liste des feeds auxquels l'user est abonné
     */
    public void buttonDeleteFeedClicked(){
        String link = getXmlFromUrl(informationRSSListView.getSelectionModel().getSelectedItem());
        mainListener.deleteInformationRSS(link);
        listener.updateArticleList();
        initializeRssWindow();
    }

    /**
     * Fenêtre pour gérer twitter
     */
    public void buttonManageTwitterClicked(){
        try {
            ManagerTwitterWindow twitterWindow = new ManagerTwitterWindow(listener);
            twitterWindow.display();
        } catch (Exception e) {
            ErrorWindow error = new ErrorWindow();
            error.display("Error","Error openning the Twitter manager",
                    "An error occurred while openning the Twitter manager");
        }
    }
}
