package be.ac.ulb.infof307.g01.java.view.window;

import be.ac.ulb.infof307.g01.java.view.controller.ManagerArticleTagWindowController;
import be.ac.ulb.infof307.g01.java.view.listener.DashboardViewListener;
import be.ac.ulb.infof307.g01.java.model.Article;
import javafx.stage.Stage;

import static be.ac.ulb.infof307.g01.java.Constants.*;


/**
 * fene^tre pour ajouter/supprimer des tags dans un article sélectionné
 */
public class ManagerArticleTagWindow extends Window<DashboardViewListener> {
    private final DashboardViewListener listener;

    public ManagerArticleTagWindow(DashboardViewListener listener) {
        this.listener = listener;
    }

    /**

     */
    public void display(Article article) throws Exception {
        super.display(FXML_ARTICLETAG_PATH, "Article Tag Manager", 600, 400, false);
        Stage articleTagWindow = super.primaryStage;
        setListener(listener);
        super.getLoader().<ManagerArticleTagWindowController>getController().initializeArticleTagWindow(article);
        articleTagWindow.showAndWait();
    }
}
