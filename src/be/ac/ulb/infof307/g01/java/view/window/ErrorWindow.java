package be.ac.ulb.infof307.g01.java.view.window;

import javafx.scene.control.Alert;

/**
 * Fenêtre d'erreur
 */
public class ErrorWindow extends AlertWindow{

    public ErrorWindow() {
        alert = new Alert(Alert.AlertType.ERROR);
    }
}
