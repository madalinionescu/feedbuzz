package be.ac.ulb.infof307.g01.java.view.window;

import java.io.IOException;

import static be.ac.ulb.infof307.g01.java.Constants.HTML_ABOUT_PATH;

/**
 * Cette classe représente la fenêtre du "à propos".
 * Elle contient du texte informatif de l'application.
 */

public class AboutWindow extends PopUpWindow{

    public static void display() throws IOException {
        display("About", HTML_ABOUT_PATH);
    }

}
