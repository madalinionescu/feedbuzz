package be.ac.ulb.infof307.g01.java.view.listener;

import be.ac.ulb.infof307.g01.java.model.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Listener pour les fonctions liées aux dashboard
 */
public interface DashboardListener {
    ArrayList<Article> getArticles();

    List<Article> getRecycleBinArticles();

    void addToSearchHistory(String search);

    ArrayList<String> getSearchHistory();

    void setupDefaultInterestsDB();

    void deleteArticle(Article article);

    Article getArticle(String title);

    void deleteArticleFromTrash(Article article);

    void changeArticlePath(Directory directory, Article article);

    void refreshRSSInBackground();

    void refreshTwitterInBackground();

    void updateReadingStatus(Article article, boolean reading);

    void markAsRecommended(Article article, boolean status);

    void updateInformationRSS(ParserRSS parser);

    List<InformationRSS> getInformationRSS();

    void deleteInformationRSS(String infoRssLink);

    void updateRSSArticles(ParserRSS parser);

    Interest getInterest(String name);

    void addInterest(Interest interest);
    
    void updateArticle(Article article);

    void updateUser(User user);

    void createIfNotExist(InformationRSS informationRSS);

    void addRSS(String link);

    void createDirectory(String dirName, Directory father);

    void createRootDirectory(Directory root);

    void deleteDirectory(String directoryID);

    Directory getDirectory(String dirName);

    void saveInformationRSS(InformationRSS infRss);

    List<String> getTags(Article article);

    List<SearchFilter> getSearchFilters();

    void addSearchFilter(SearchFilter searchFilter);

    SearchFilter getSearchFilterByName(String name);

    void deleteFilter(String name);

    void saveInformationTwitter(InformationTwitter informationTwitter);

    void deleteTwitterHashtag(String hashtagName);
    void deleteTwitterAccount(String accountName);

    List<InformationTwitter> getInformationTwitterAccounts();
    List<InformationTwitter> getInformationTwitterHashtags();
}

