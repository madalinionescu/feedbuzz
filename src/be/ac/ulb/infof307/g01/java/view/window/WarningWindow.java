package be.ac.ulb.infof307.g01.java.view.window;

import javafx.scene.control.Alert;

public class WarningWindow extends AlertWindow{

    public WarningWindow() {
        alert = new Alert(Alert.AlertType.WARNING);
    }
}
