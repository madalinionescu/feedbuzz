package be.ac.ulb.infof307.g01.java.view.listener;

/**
 * Listener pour les fonctions liées au dashboard view
 */
public interface DashboardViewListener {
    void updateArticleList();

    MainListener getMainListener();
}
