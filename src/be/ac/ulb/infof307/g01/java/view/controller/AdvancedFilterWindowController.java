package be.ac.ulb.infof307.g01.java.view.controller;

import be.ac.ulb.infof307.g01.java.controller.MainController;
import be.ac.ulb.infof307.g01.java.model.LocalUser;
import be.ac.ulb.infof307.g01.java.model.Search;
import be.ac.ulb.infof307.g01.java.model.SearchFilter;
import be.ac.ulb.infof307.g01.java.model.Tag;
import be.ac.ulb.infof307.g01.java.model.builder.SearchBuilder;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static be.ac.ulb.infof307.g01.java.Constants.*;

/**
 * Controller pour la fenêtre de recherche avancée
 */
public class AdvancedFilterWindowController extends ViewController<MainController> {
    @FXML private ComboBox filtersComboBox;
    @FXML private ComboBox twitterComboBox;

    @FXML private TextField advancedTextField;

    @FXML private CheckBox dateCheckBox;
    @FXML private CheckBox authorCheckBox;
    @FXML private CheckBox titleCheckBox;
    @FXML private CheckBox contentCheckBox;
    @FXML private CheckBox sourceCheckBox;
    @FXML private CheckBox locationCheckBox;
    @FXML private CheckBox twitterCheckBox;

    @FXML private VBox datePropertiesBox;
    @FXML private DatePicker datePicker;
    @FXML private TextField authorTextbox;
    @FXML private TextField sourceTextbox;
    @FXML private TextField locationTextbox;
    @FXML private TextField twitterTextbox;
    @FXML private ChoiceBox<String> tagsChoicebox;
    @FXML private TextField filterName;

    @FXML private RadioButton dateBeforeRadio;
    @FXML private RadioButton dateOnRadio;
    @FXML private RadioButton dateAfterRadio;
    @FXML private ToggleGroup radioButtons;

    /**
     * Le contrôleur de la fenêtre parente.
     */
    private DashboardWindowController parentWindowController;
    private static final int YEAR = 2; // constantes (indices dans les listes (de 3 éléments) contenant les éléments des dates)
    private static final int MONTH = 1;
    private static final int DAY = 0;
    private static final String TWITTER = "twitter";
    private static final String[] TWITTERCHOICE = {"All", "@Account", "#Hashtag"};

    /**
     * Appelée à l'initialisation de la fenêtre
     */
    public void initializeAdavancedSearch() {
        tagsChoicebox.getItems().addAll(TAG_NAME_NOT_READ, TAG_NAME_READING, TAG_NAME_FINISHED);
        List<Tag> tags = LocalUser.singleton.getUser().getUserTags();
        for (Tag tag : tags){
            tagsChoicebox.getItems().addAll(tag.getName());
        }

        twitterComboBox.getItems().addAll(TWITTERCHOICE);

        updateFilterList();
    }

    private void updateFilterList() {
        List<String> existentFilters = new ArrayList<>();
        for (SearchFilter filter : listener.getSearchFilters()) {
            existentFilters.add(filter.getName());
        }

        filtersComboBox.getItems().clear();
        filtersComboBox.getItems().addAll(existentFilters);
    }

    /**
     * Permets de désigner le contrôleur de la fenêtre parente.
     * @param parent est le contrôleur du parent
     */
    public void setParentWindowController(DashboardWindowController parent) {
        parentWindowController = parent;
    }

    /**
     * Lorsque le bouton de recherche est cliqué, la liste des
     * articles est nettoyée et les articles correspondant à la
     * recherche y sont affichés dans la fenêtre parente.
     */
    public void searchButtonClicked() {
        Search search = readAdvancedSearchWindow();
        search.setId(LocalUser.singleton.getUserId());

        // replace with calls to the listener
        parentWindowController.clearPreview();
        //Ajoute la recherche à l'historique
        parentWindowController.addToSearchHistory(advancedTextField.getText());
        parentWindowController.setSearchedArticles(search.advancedSearch());
        parentWindowController.setSearch(search);
        parentWindowController.clearArticlesFromListView();
        parentWindowController.putArticlesInListView(parentWindowController.searchedArticles);
        parentWindowController.sortComboBox.setValue(ORDER_MOST_RECENT);
    }

    /**
     * @return si on veut chercher avant/après/à une certaine
     * date en fonction de la selection faite par l'user
     */
    private String getDateComparison(){
        String dateComparison;
        if (dateBeforeRadio.isSelected())
            dateComparison = Search.BEFORE;
        else if (dateAfterRadio.isSelected())
            dateComparison = Search.AFTER;
        else
            dateComparison = Search.ON_DATE;
        return dateComparison;
    }

    /**
     * Récupère les paramètres de la recherche avancée, depuis la
     * fenêtre, et créer un Search ayant pour attributs les paramètres lus
     * @return Un Search selon les paramètres de la recherche avancée
     */
    private Search readAdvancedSearchWindow(){
        String searchText = advancedTextField.getText();
        String title, content, author, date, source, location, tag;
        title = content = author = date = source = location = null;
        String dateComparison = null; //La recherche se fait sans considérer la date
        List<String> hashtags = new ArrayList<>();

        if (titleCheckBox.isSelected()) title = searchText;
        if (sourceCheckBox.isSelected()) source = sourceTextbox.getText();
        if (contentCheckBox.isSelected()) content = searchText;
        if (authorCheckBox.isSelected()) author = authorTextbox.getText();
        if (locationCheckBox.isSelected()) location = locationTextbox.getText();
        if (dateCheckBox.isSelected()) {
            date = datePicker.getEditor().getText();
            if (!(date.equals(""))) {                                   // check si on a une date
                dateComparison = getDateComparison();
                date = convertDate(date);
            }
        }
        if (twitterCheckBox.isSelected()) {
            source = TWITTER;
            title = getSourceTwitterTitle();
        }

        tag = tagsChoicebox.getValue();

        return new SearchBuilder()
                .setTitle(title)
                .setContent(content)
                .setAuthor(author)
                .setDate(date)
                .setDateComparison(dateComparison)
                .setLocation(location)
                .setSource(source)
                .setHashtag(hashtags)
                .setTag(tag)
                .build();
    }


    /**
     * Convertit une date passée en entrée sous le format DD/MM/YYYY en une date
     * dans le format YYYY-MM-DD
     * @param date La date sous forle de String
     * @return Un string (la date sous le nouveau format)
     */
    private String convertDate(String date){
        String[] dateValues = date.split("/");
        return String.format("%04d-%02d-%02d",Integer.valueOf(dateValues[YEAR]),
                Integer.valueOf(dateValues[DAY]),
                Integer.valueOf(dateValues[MONTH]));
    }

    /**
     * Affiche les champs permettant de changer les propriétés
     * de la recherche relatives à la date seulement quand le
     * checkbox "Date" est croché.
     */
    @FXML private void displayDateProp() {
        datePropertiesBox.setVisible(dateCheckBox.isSelected());
    }

    /**
     * Affiche les champs permettant de changer les propriétés
     * de la recherche relatives au l'auteur seulement quand le
     * checkbox "Author" est croché.
     */
    @FXML private void displayAuthorProp() {
        authorTextbox.setVisible(authorCheckBox.isSelected());
    }

    /**
     * Affiche les champs permettant de changer les propriétés
     * de la recherche relatives à la localisation des articles
     * seulement quand le checkbox "Location" est croché.
     */
    @FXML private void displayLocationProp() {
        locationTextbox.setVisible(locationCheckBox.isSelected());
    }

    /**
     * Affiche les champs permettant de changer les propriétés
     * de la recherche relatives à la source des articles quand le
     * checkbox "Source" est croché.
     */
    @FXML private void displaySourceProp() {
        sourceTextbox.setVisible(sourceCheckBox.isSelected());
        displayTwitterLikeSource(false);
    }

    /**
     * Affiche les champs permettant de changer les propriétés
     * de la recherche relatives aux réseaux sociaux
     * seulement quand le checkbox "Twitter" est croché.
     */
    @FXML private void displayTwitterProp() {
        displayTwitterLikeSource(twitterCheckBox.isSelected());
    }

    /***
     * Configure l'affichage si twitter est utilisé comme source
     * @param show
     */
    private void displayTwitterLikeSource(boolean show){
        twitterComboBox.setVisible(show);
        twitterCheckBox.setSelected(show);
        if(show){
            sourceCheckBox.setSelected(false);
            sourceTextbox.setVisible(false);
            int selectIndex = twitterComboBox.getSelectionModel().getSelectedIndex();
            twitterTextbox.setVisible(selectIndex > 0);
        }
        else twitterTextbox.setVisible(false);
    }

    /***
     * Appellé quand le combobox de twitter change de value, elle permet
     * d'afficher ou non le textBox et de changer le prompt à l'intérieur
     */
    public void loadTwitterCombobox() {
        int selectIndex = twitterComboBox.getSelectionModel().getSelectedIndex();
        twitterTextbox.setVisible(selectIndex > 0);
        if(selectIndex <= 0) return;
        twitterTextbox.setPromptText(TWITTERCHOICE[selectIndex]);
    }

    /***
     * Quand le twitter est activer comme source, le combobox est
     * configuré en fonction du titre
     * @param title titre
     */
    private void setSourceTwitterTitle(String title){
        if(title == null) title = "";

        twitterTextbox.setText(title);

        if(title.startsWith("#")){
            twitterComboBox.getSelectionModel().select(2);
        }
        else if(title.startsWith("@")){
            twitterComboBox.getSelectionModel().select(1);
        }
        else twitterComboBox.getSelectionModel().select(0);
    }

    /***
     * Créer un titre de recherche pour le feed twitter.
     * @return title titre
     */
    private String getSourceTwitterTitle(){
        int selectIndex = twitterComboBox.getSelectionModel().getSelectedIndex();
        String text = twitterTextbox.getText().trim();
        if(selectIndex<=0 || text.length() == 0) return "";
        if(selectIndex == 1 && !text.startsWith("@"))
            text = "@" + text;
        else if(selectIndex == 2 && !text.startsWith("#"))
            text = "#" + text;

        return text;
    }

    /**
     * Sauvegarde les filtres pour être réutilisés plus tard
     * dans le cas d'une recherche
     */
    public void saveFilter() {
        if (!filterName.getText().isEmpty()) {
            SearchFilter filter = new SearchFilter(readAdvancedSearchWindow(), filterName.getText());

            listener.addSearchFilter(filter);

            filterName.setText("");

            updateFilterList();
        } else validateTextField(filterName, false);
    }

    /**
     * Charge un filtre sauvegardé préalablement par un user
     */
    public void loadFilter() {
        filtersComboBox.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(2), new BorderWidths(1))));

        Object selectedFilter = filtersComboBox.getSelectionModel().getSelectedItem();
        if (selectedFilter == null) return;

        SearchFilter filter = listener.getSearchFilterByName(selectedFilter.toString());

        setCriteria(filter.getTitle(), advancedTextField, titleCheckBox);
        setCriteria(filter.getContent(), advancedTextField, contentCheckBox);

        setCriteria(filter.getLocation(), locationTextbox, locationCheckBox);
        displayLocationProp();

        setCriteria(filter.getAuthor(), authorTextbox, authorCheckBox);
        displayAuthorProp();

        String source = filter.getSource();
        if(source != null && source.equals(TWITTER)){
            twitterCheckBox.setSelected(true);
            setSourceTwitterTitle(filter.getTitle());
            displayTwitterProp();
        }
        else{
            setCriteria(source, sourceTextbox, sourceCheckBox);
            displaySourceProp();
        }

        setDateCriteria(filter);
        displayDateProp();
    }

    /**
     * @param text text du critère a definir
     * @param textField field dans lequel on le definit
     * @param checkBox checkbox jouant sur l'affichage du criète
     */
    private void setCriteria(String text, TextField textField, CheckBox checkBox) {
        if (!(text == null)) {
            textField.setText(text);
            checkBox.setSelected(true);
        } else {
            textField.setText("");
            checkBox.setSelected(false);
        }
    }

    private void clearDateCriteria() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        datePicker.setValue(LocalDate.parse(formatter.format(LocalDateTime.now())));
        dateOnRadio.setSelected(true);
        dateCheckBox.setSelected(false);
    }

    private void setDateCriteria(SearchFilter filter) {
        if ((filter.getDate() == null) || (filter.getDate().isEmpty())){
            clearDateCriteria();
            return;
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(filter.getDate(), formatter);
        datePicker.setValue(localDate);
        setDateComparisonSelected(filter);
        dateCheckBox.setSelected(true);
    }

    /**
     * selectionne le bon datecomparison
     * @param filter filtre de recherche
     */
    private void setDateComparisonSelected(SearchFilter filter){
        switch (filter.getDateComparison()) {
            case Search.BEFORE :
                dateBeforeRadio.setSelected(true); break;
            case Search.ON_DATE :
                dateOnRadio.setSelected(true); break;
            case Search.AFTER:
                dateAfterRadio.setSelected(true); break;
            default : break;
        }
    }

    public void unsetFilter() {
        filtersComboBox.getSelectionModel().clearSelection();
    }

    public void validateFilerName() {
        if (filterName.getText().isEmpty()) {
            validateTextField(filterName, true);
        }
    }

    private void validateTextField(TextField textField, boolean valid) {
        if (valid) {
            textField.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(2), new BorderWidths(0))));
        } else {
            textField.setBorder(new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID, new CornerRadii(2), new BorderWidths(1))));
        }
    }

    public void deleteFilter() {
        if (filtersComboBox.getSelectionModel().getSelectedItem() == null) {
            filtersComboBox.setBorder(new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID, new CornerRadii(2), new BorderWidths(1))));
        } else {
            listener.deleteFilter(filtersComboBox.getSelectionModel().getSelectedItem().toString());
            filtersComboBox.getSelectionModel().clearSelection();
            updateFilterList();
        }
    }
}
