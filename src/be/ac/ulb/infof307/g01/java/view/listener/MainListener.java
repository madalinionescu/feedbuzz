package be.ac.ulb.infof307.g01.java.view.listener;

/**
 * Listener liant LoginListener et DashBoarDListener (les deux se 'parlant')
 */
public interface MainListener extends LoginListener, DashboardListener {
}
