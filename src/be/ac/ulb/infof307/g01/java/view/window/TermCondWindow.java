package be.ac.ulb.infof307.g01.java.view.window;

import java.io.IOException;

import static be.ac.ulb.infof307.g01.java.Constants.HTML_TERMS_PATH;

/**
 * Cette classe représente la fenêtre des termes et conditions.
 * Elle contient les termes conditions générales définies par
 * l'administrateur système.
 */
public class TermCondWindow extends PopUpWindow {

    public static void display() throws IOException {
        display("Terms & Conditions", HTML_TERMS_PATH);
    }
}
