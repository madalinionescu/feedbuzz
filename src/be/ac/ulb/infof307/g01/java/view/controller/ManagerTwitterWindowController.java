package be.ac.ulb.infof307.g01.java.view.controller;
import be.ac.ulb.infof307.g01.java.model.dao.DAOFactory;
import be.ac.ulb.infof307.g01.java.model.*;
import be.ac.ulb.infof307.g01.java.utils.ThreadJavaFx;
import be.ac.ulb.infof307.g01.java.view.listener.DashboardViewListener;
import be.ac.ulb.infof307.g01.java.view.listener.MainListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import static be.ac.ulb.infof307.g01.java.Constants.HASHTAG;
import static be.ac.ulb.infof307.g01.java.Constants.ACCOUNT;


import java.util.ArrayList;
import java.util.List;

/**
 * Classe représentant le controlleur de la fenêtre de gestion des abonnements Twitter
 */
public class ManagerTwitterWindowController extends ViewController<DashboardViewListener> {


    private MainListener mainListener;

    @FXML private ListView<String> userHashtagsListView;
    @FXML private ListView<String> userAccountsListView;
    @FXML private TextField hashtagNameTextField;
    @FXML private TextField accountNameTextField;
    @FXML private Spinner<Integer> numberPostsSpinner;
    @FXML private Button confirmButton;


    public void initializeTwitterWindow() {
        mainListener = listener.getMainListener();
        SpinnerValueFactory<Integer> valueFactory =
                new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 20, 3);
        numberPostsSpinner.setValueFactory(valueFactory);

        numberPostsSpinner.setDisable(true);

        handleTextFieldInput();
        refreshHashtagsListView();
        refreshAccountsListView();

        handleWindowCloseAction();
    }



    private void handleTextFieldInput() {
        hashtagNameTextField.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
            if (event.getCode() == KeyCode.SPACE) {
                event.consume();
            }
        });

    }

    /**
     * Met à jour la liste d'articles lorsque l'utilisateur ferme la fenêtre
     */
    private void handleWindowCloseAction() {
        Stage primaryStage = (Stage) confirmButton.getScene().getWindow();
        primaryStage.setOnCloseRequest(t -> {
            listener.updateArticleList();
            handleConfirmButtonAction();
        });
    }


    /**
     * Rafraîchit la ListView en mettant tous les hashtags auxquels l'utilisateur
     * est abonné
     */
    private void refreshHashtagsListView(){
        ObservableList<String> items = FXCollections.observableArrayList (getHashtags());
        userHashtagsListView.setItems(items);
    }

    private List<String> getHashtags(){
        List<InformationTwitter> userHashtags = mainListener.getInformationTwitterHashtags();
        List<String> userHashtagsAsString = new ArrayList<>();
            for (InformationTwitter hashtag: userHashtags){
                userHashtagsAsString.add("#"+hashtag.getName());
        }
        return userHashtagsAsString;
    }

    private void refreshAccountsListView(){
        ObservableList<String> items = FXCollections.observableArrayList (getAccounts());
        userAccountsListView.setItems(items);
    }

    /**
     * Fonction qui recupere une liste d'hashtags auxquels l'utilisateur est abonné.
     * Met un # au debut de tous les noms et renvoit la liste
     * @return
     */
    private List<String> getAccounts(){
        List<InformationTwitter> userAccounts = mainListener.getInformationTwitterAccounts();
        List<String> userAccountsAsString = new ArrayList<>();
        for (InformationTwitter account: userAccounts){
            userAccountsAsString.add("@"+account.getName());
        }
        return userAccountsAsString;
    }


    /**
     * Fonction qui ajoute dans la base de données le hashtag introduit par l'utilisateur
     * @param : le nom du hashtag
     */

    private void addHashtag(String hashtagName){
        if (!hashtagName.trim().isEmpty()) {
            if(hashtagName.startsWith("#")){ hashtagName = hashtagName.substring(1); }

            InformationTwitter hashtag = new InformationTwitter(
                    hashtagName,
                    numberPostsSpinner.getValue(),
                    HASHTAG
            );
            mainListener.saveInformationTwitter(hashtag);
        }
    }


    private void createHashtagThread() {
            ThreadJavaFx thread = new ThreadJavaFx() {
                @Override
                protected void backgroundWork() {
                    submitHashtagBackgroundWork();
                }

                @Override
                protected void finalWork() {
                    submitHashtagFinalWork();
                }
            };
            thread.run();
    }


    private void submitHashtagFinalWork(){
        refreshHashtagsListView();
    }


    private void submitHashtagBackgroundWork(){

        mainListener.deleteTwitterHashtag(userHashtagsListView
                .getSelectionModel()
                .getSelectedItem()
                .substring(1)); // retire le # au debut du nom du hashtag
    }

    private void createAccountThread() {
        ThreadJavaFx thread = new ThreadJavaFx() {
            @Override
            protected void backgroundWork() {
                submitAccountBackgroundWork();
            }

            @Override
            protected void finalWork()  { submitAccountFinalWork(); }
        };
        thread.run();
    }

    /**
     * Fait partie du travail en arrière plan du thread
     * Supprime le hashtag de la base de données ainsi que tous les tweets liés
     * à ce hashtag.
     * Met à jour la liste d'articles dans le Dashboard
     */
    private void submitAccountBackgroundWork(){
        mainListener.deleteTwitterAccount(userAccountsListView
                .getSelectionModel()
                .getSelectedItem()
                .substring(1)); // retire le # au debut du nom du compte
        listener.updateArticleList();
    }

    /**
     *
     */
    private void submitAccountFinalWork(){
        refreshAccountsListView();
    }


    public void followHashtagButtonClick(){
        String hashtagName = hashtagNameTextField.getText();
            addHashtag(hashtagName);
            refreshHashtagsListView();
            hashtagNameTextField.clear();

        refreshHashtagsListView();
//
    }

    /**
     * Fonction qui met à jour la liste d'articles dans le Dashboard lorsque
     * l'utilisateur clique sur le button Confirm
     * Ferme la fenetre
     */
    public void confirmButtonClicked(){
        listener.updateArticleList();
        handleConfirmButtonAction();
    }

    public void hashtagslistViewItemSelected(){
        hashtagNameTextField.clear();
        numberPostsSpinner.setDisable(true);
    }

    public void hashtagNameTextFieldClicked(){
        userHashtagsListView.getSelectionModel().select(null);
        numberPostsSpinner.setDisable(false);
    }

    public void accountNameTextFieldClicked(){
        userAccountsListView.getSelectionModel().select(null);
        numberPostsSpinner.setDisable(false);
    }

    /**
     * Ferme la fenetre
     */
    private void handleConfirmButtonAction() {
        Stage stage = (Stage) confirmButton.getScene().getWindow();
        stage.close();
    }

    public void followAccountButtonClicked() {
        String accountName = accountNameTextField.getText();
        if (!accountName.trim().isEmpty()) {
            addAccount(accountName);
            refreshAccountsListView();
            accountNameTextField.clear();
        }
        refreshAccountsListView();
    }

    private void addAccount(String accountName) {
            if(accountName.startsWith("@")){ accountName = accountName.substring(1); }

            InformationTwitter account = new InformationTwitter(
                    accountName,
                    numberPostsSpinner.getValue(),
                    ACCOUNT
            );
            mainListener.saveInformationTwitter(account);
    }

    public void unfollowAccountButtonClicked(){
        ObservableList<String> readOnlyItems = userAccountsListView.getSelectionModel().getSelectedItems();
        readOnlyItems.forEach((item) -> DAOFactory.getInformationTwitterDAO().deleteByName(item.substring(1), ACCOUNT));
        refreshAccountsListView();
    }

    public void unfollowHashtagButtonClicked(){
        ObservableList<String> readOnlyItems = userHashtagsListView.getSelectionModel().getSelectedItems();
        readOnlyItems.forEach((item) -> DAOFactory.getInformationTwitterDAO().deleteByName(item.substring(1), HASHTAG));
        refreshHashtagsListView();
    }
}
