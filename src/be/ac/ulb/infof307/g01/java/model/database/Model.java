package be.ac.ulb.infof307.g01.java.model.database;

/**
 * Le model est la class de base qui compose chaques objects
 * de la database.
 */
public abstract class Model {

    //Identifiant unique
    public abstract String getId();

    public abstract void setId(String id);

    public abstract boolean canGetNewId();

    @Override
    public boolean equals(Object o){

        if(o==null) return false;

        Model other = (Model) o;
        String currentUniqueId = getId();
        String otherUniqueId = other.getId();

        if(currentUniqueId == null)
            return(otherUniqueId == null);

        return currentUniqueId.equals(otherUniqueId);
    }
}
