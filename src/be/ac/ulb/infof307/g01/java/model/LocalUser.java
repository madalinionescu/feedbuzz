package be.ac.ulb.infof307.g01.java.model;

import be.ac.ulb.infof307.g01.java.model.dao.DAOFactory;

/**
 * Cette class possède l'instance de l'utilisateur local.
 */
public class LocalUser {

    public static final LocalUser singleton = new LocalUser();

    private User user;

    /**
     * Constructeur privé
     */
    private LocalUser() {
    }

    /**
     * Permet de créer un utilisateur local via le DAO
     *
     * @param username
     * @param password
     * @param mail
     * @return Boolean
     */
    public Boolean create(String username, String password, String mail) {

        return DAOFactory.getUserDAO().create(username, password, mail) != null;
    }

    /**
     * Permet de se connecter à l'utilisateur local via le DAO
     * @param username
     * @param password
     * @return Boolean
     */
    public Boolean connect(String username, String password) {

        user = DAOFactory.getUserDAO().find(username, password);
        return user != null;
    }

    /**
     * Permet de vérifier que l'utilisateur local est connecté
     * @return Boolean
     */
    Boolean isConnect() {
        return user != null;
    }

    /**
     * Permet de déconnecter l'utilisateur local
     */
    public void disconnect() {
        user = null;
    }

    /**
     * Return l'utilisateur local
     * @return user
     */
    public User getUser() {
        return user;
    }

    /**
     * retourne l'identifiant de l'utilisateur
     * @return String
     */
    public String getUserId() {
        return user.getId();
    }
}
