package be.ac.ulb.infof307.g01.java.model.dao;

import be.ac.ulb.infof307.g01.java.model.InformationRSS;
import be.ac.ulb.infof307.g01.java.model.User;
import be.ac.ulb.infof307.g01.java.model.database.Database;

import java.util.ArrayList;
import java.util.List;

public class InformationRSSDAO extends OwnedDAO<InformationRSS>{


    /**
     * constructeur du DAO
     *
     * @param db database
     */
    InformationRSSDAO(Database db, User user) {
        super(db,user);
    }

    /**
     * Creer ou mets à jour un article avec reference sont link qui doit être unique
     * @param link lien
     */
    public void createIfNotExist(InformationRSS link){
        List<InformationRSS> links = findAll();
        List<InformationRSS> matchArticle = new ArrayList<>();
        for (InformationRSS p : links) {
            if (p.getLink().equals(link.getLink())) {
                matchArticle.add(p);
            }
        }
        if(matchArticle.size() == 0){
           create(link);
        }
    }

    @Override
    public Class<InformationRSS> getModelClass() {
        return InformationRSS.class;
    }
}
