package be.ac.ulb.infof307.g01.java.model;

import be.ac.ulb.infof307.g01.java.model.database.Model;
import io.jsondb.annotation.Document;
import io.jsondb.annotation.Id;

/**
 * Représente un tag que l'utilisateur peut créer
 * en plus de ceux présents par défaut (unread, reading, finished)
 */
@Document(collection="Tag", schemaVersion="1.0")
public class Tag extends Model {

    @Id
    private String name;

    public Tag() {}

    public Tag(String name){
        this.name = name;
    }

    public String getName() {
        return this.name;
    }


    @Override
    public void setId(String name) {
        this.name = name;
    }

    @Override
    public boolean canGetNewId() {
        return false;
    }

    @Override
    public String getId() {
        return this.name;
    }
}
