package be.ac.ulb.infof307.g01.java.model.dao;
import be.ac.ulb.infof307.g01.java.model.LocalUser;
import be.ac.ulb.infof307.g01.java.model.Tag;
import be.ac.ulb.infof307.g01.java.model.User;
import be.ac.ulb.infof307.g01.java.model.database.Database;
import be.ac.ulb.infof307.g01.java.view.window.ErrorWindow;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.util.ArrayList;
import java.util.List;

public class UserDAO extends DAO<User> {

    /**
     * constructeur du DAO
     *
     * @param db database
     */
    UserDAO(Database db) {
        super(db);
    }

    /**
     * crée l'object dans la database
     * @param model le model
     */
    @Override
    public boolean create(User model){
        return db.create(model);
    }

    /**
     * récupère l'object de la database
     * @param id l'id du model
     */
    @Override
    public User findById(String id){
        return db.findById(id,getModelClass());
    }

    /**
     * Créer un user qui possèdera un mot de passe hash
     */
    public User create(String username, String password, String mail) {
        if(username == null || password == null || mail == null) return null;

        String salt = BCrypt.gensalt();
        String hash = BCrypt.hashpw(password,salt);

        User user = new User(username, hash, mail, new ArrayList<>());
        if(DAOFactory.getUserDAO().create(user))
            return user;
        return null;
    }

    /**
     * récupère l'utilisateur de la database
     * @param username username
     * @param password password
     */
    public User find(String username,String password){
        if(username == null || password == null) return null;
        User user = findById(username);

        try {
            if(user != null && BCrypt.checkpw(password, user.getHash())){
                return user;
            }
        }
        catch (IllegalArgumentException e){
            ErrorWindow error = new ErrorWindow();
            error.display("Error","Error in Database!", e.toString());
        }

        return null;
    }

    void addTag(String tag){
        Tag tmpTag = new Tag(tag);
        User user = LocalUser.singleton.getUser();
        List<Tag> tags = user.getUserTags();
        tags.add(tmpTag);
        user.setUserTags(tags);
        DAOFactory.getUserDAO().update(user);
    }

    void deleteTag(String tag){
        User user = LocalUser.singleton.getUser();
        List<Tag> tags = user.getUserTags();
        tags.remove(new Tag(tag));
        user.setUserTags(tags);
        DAOFactory.getUserDAO().update(user);
    }

    @Override
    public Class<User> getModelClass() {
        return User.class;
    }


}
