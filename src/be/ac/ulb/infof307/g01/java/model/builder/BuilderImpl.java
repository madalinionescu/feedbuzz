package be.ac.ulb.infof307.g01.java.model.builder;

import be.ac.ulb.infof307.g01.java.model.database.Model;

/**
 * Interface pour les Builders
 * @param <T> Model dont on souhaite créer un Builder
 */
public interface BuilderImpl <T extends Model> {
    T build();
}
