package be.ac.ulb.infof307.g01.java.model;

import be.ac.ulb.infof307.g01.java.model.database.OwnedModel;

public abstract class Node extends OwnedModel {

    protected String name;

    public Node(){}

    public Node(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public boolean nodeIsFolder(){
        return false;
    }

    @Override
    public String toString() {
        return name;
    }
}
