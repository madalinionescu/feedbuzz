package be.ac.ulb.infof307.g01.java.model.builder;

import be.ac.ulb.infof307.g01.java.model.dao.DAOFactory;
import be.ac.ulb.infof307.g01.java.model.Article;
import be.ac.ulb.infof307.g01.java.model.Tag;

import java.util.List;

import static be.ac.ulb.infof307.g01.java.Constants.TAG_NAME_FINISHED;
import static be.ac.ulb.infof307.g01.java.Constants.TAG_NAME_READING;


/**
 * Builder pour Article qui contient plein de fonctions pour set
 * plusieurs attributs possibles d'un article
 */
public class ArticleBuilder implements BuilderImpl {
    private final Article articleTmp;
    
    public ArticleBuilder(){
        articleTmp = new Article();
    }

    public Article build() {
        Article article = new Article();
        article.setName(articleTmp.getName());
        article.setContent(articleTmp.getContent());
        article.setAuthor(articleTmp.getAuthor());
        article.setSource(articleTmp.getSource());
        article.setLink(articleTmp.getLink());
        article.setLocation(articleTmp.getLocation());
        article.setDate(articleTmp.getDate());
        article.setHashtags(articleTmp.getHashtags());
        article.setThumbnail(articleTmp.getThumbnail());
        article.setTrash(articleTmp.getTrash());
        article.setRssLink(articleTmp.getRssLink());
        article.setTags(articleTmp.getTags());
        article.setHTMLPath(articleTmp.getHTMLPath());
        article.setTwitterHashtag(articleTmp.getTwitterHashtag());
        article.setTweet(articleTmp.isTweet());
        return article;
    }

    
    public ArticleBuilder setHTMLPath(String source){
        articleTmp.setHTMLPath(source);
        return this;
    }

    
    public ArticleBuilder setTitle(String title) {
        articleTmp.setName(title);
        return this;
    }

    
    public ArticleBuilder setContent(String content) {
        articleTmp.setContent(content);
        return this;
    }

    
    public ArticleBuilder setAuthor(String author) {
        articleTmp.setAuthor(author);
        return this;
    }

    
    public ArticleBuilder setSource(String source) {
        articleTmp.setSource(source);
        return this;
    }

    
    public ArticleBuilder setLink(String link) {
        articleTmp.setLink(link);
        return this;
    }

    
    public ArticleBuilder setLocation(String location) {
        articleTmp.setLocation(location);
        return this;
    }

    
    public ArticleBuilder setDate(String date) {
        articleTmp.setDate(date);
        return this;
    }

    
    public ArticleBuilder setHashtags(List<String> hashtags){
        articleTmp.setHashtags(hashtags);
        return this;
    }


    public ArticleBuilder setThumbnail(String thumbnail) {
        articleTmp.setThumbnail(thumbnail);
        return this;
    }


    public ArticleBuilder setRssLink(String rssLink) {
        articleTmp.setRssLink(rssLink);
        return this;
    }

    
    public ArticleBuilder setTags(List<Tag> tags) {
        articleTmp.setTags(tags);
        return this;
    }

    
    public ArticleBuilder addTag(String name) {
        if (DAOFactory.getArticleDAO().hasTagInArticle(TAG_NAME_FINISHED, articleTmp) ||
            DAOFactory.getArticleDAO().hasTagInArticle(TAG_NAME_READING, articleTmp))
            return this;
        articleTmp.addTag(name);
        return this;
    }

    
    public ArticleBuilder setTweet(boolean tweet) {
        articleTmp.setTweet(tweet);
        return this;
    }

    
    public ArticleBuilder setTwitterHashtag(String twitterHashtag) {
        articleTmp.setTwitterHashtag(twitterHashtag);
        return this;
    }
}
