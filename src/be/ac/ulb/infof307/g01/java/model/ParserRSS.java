package be.ac.ulb.infof307.g01.java.model;

import be.ac.ulb.infof307.g01.java.model.dao.DAOFactory;
import be.ac.ulb.infof307.g01.java.model.builder.ArticleBuilder;
import be.ac.ulb.infof307.g01.java.utils.DateUtils;
import be.ac.ulb.infof307.g01.java.view.window.ErrorWindow;
import javafx.scene.image.Image;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static be.ac.ulb.infof307.g01.java.Constants.*;

/**
 * ParserRSS est une classe qui va gérer le parsing d'un flux RSS.
 * Elle va gérer les flux locaux (dans un fichier par exemple) ainsi
 * que les flux sur internet (d'un site web par exemple). Pour cela,
 * il suffit de lui donner en paramètre un String avec soit le chemin
 * du fichier, soit l'adresse internet du site (avec le http). Il
 * distinguera tout seul s'il s'agit d'un flux local ou internet.
 * Ensuite, il parsera le RSS (format XML) afin de créer un objet
 * Article avec les différentes données (titre, description, lien, etc.)
 * On stocke tous ces articles dans une liste.
 * @see Article
 */
public class ParserRSS extends Parser {
    private final InformationRSS informationRSS;
    private final boolean downloadOffline;
    private DocumentBuilder builder;
    private Element racine;
    private NodeList racineChildren;
    private Date lastBuildDate = new Date();
    private final List<Article> parsedArticles = new ArrayList<>();

    /**
     * Le constructeur crée un Document qui nous permettra
     * d'accéder plus facilement à la racine et ses éléments.
     * @param informationRSS représente l'ensemble des données du flux (lien, nombre d'articles, etc)
     * @see InformationRSS
     */
    public ParserRSS(final InformationRSS informationRSS, boolean downloadOffline) {
        this.informationRSS = informationRSS;
        this.downloadOffline = downloadOffline;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            builder = factory.newDocumentBuilder();
            Document tmpDoc;
            tmpDoc = getDocument(informationRSS.getLink());
            if (tmpDoc != null) {
                racine = tmpDoc.getDocumentElement();
                racineChildren = getRootChildren();

                updateLastBuildDate();
                updateArticles();
            }
        } catch (Exception e) {
            // getArticles() va avoir simplement 0 article. Pas d'erreur à gérer.
        }
    }

    public InformationRSS getInformationRSS() {
        return informationRSS;
    }


    /**
     * Update le lastBuildDate en fonction de celui du RSS
     */
    private void updateLastBuildDate(){
        for (int i = 0; i < racineChildren.getLength(); ++i){
            Node currentNode = racineChildren.item(i);
            if (currentNode.getNodeName().equals("lastBuildDate")){
                lastBuildDate = DateUtils.stringToDate(currentNode.getTextContent());
                break;
            }
        }
    }

    /**
     * Lie un website pour récupérer son code XML et le renvoie
     * sous forme de String
     * @param requestURL l'adresse du flux XML RSS
     * @return string contenant le XML
     */
    private static String readStringFromURL(String requestURL) {
        try (Scanner scanner = new Scanner(new URL(requestURL).openStream(),
                StandardCharsets.UTF_8.toString())) {
            scanner.useDelimiter("\\A");
            return scanner.hasNext() ? scanner.next() : "";
        }
        catch (Exception e){
            return "";
        }
    }

    /**
     * Gère le XML (format RSS) et le transforme en Document Java
     * @param source Lien d'un website xml (RSS)
     * @return Document objet Java pour gérer les noeuds plus tard
     * @throws Exception si problème de parsage par le Builder de Document.
     */
    private Document getDocument(String source) throws Exception {
        if (source == null|| !source.contains("http"))
            return null;
        String s = readStringFromURL(source);
        InputStream stream = new ByteArrayInputStream(s.getBytes(StandardCharsets.UTF_8));
        return builder.parse(stream);
    }

    /**
     * Récupère les enfants de la racine. On a donc une liste de noeuds,
     * chaque noeud représentant donc un article à part.
     * @return liste de noeuds contenant les informations d'un article.
     */
    private NodeList getRootChildren(){
        return racine.getElementsByTagName("channel").item(0).getChildNodes(); // channel
    }

    /**
     * Cycle à travers les enfants de la racine qui sont des noeuds
     * représentant les articles. A chaque noeud, on crée un article
     * qu'on ajoute à la liste des articles.
     */
    private void createArticles(){
        int nbArticles = informationRSS.getNbArticles();
        if (nbArticles == 0) //si on veut "0" article, cela veut dire qu'on les veut tous
            nbArticles = racineChildren.getLength();
        for (int i = 0; i < racineChildren.getLength(); ++i){

            try{
                if (racineChildren.item(i).getNodeName().equals("item")) {
                    Node item = racineChildren.item(i);
                    Article article = createArticleFromNode( (Element) item);
                    parsedArticles.add(article);
                }
            } catch (Exception e){
                //Pas un item, on peut skip.
            }
            if (parsedArticles.size() >= nbArticles)
                break;
        }
    }

    /**
     * Crée un Article à partir d'un noeud XML. Le noeud contient
     * différentes informations sur celui-ci qui seront utilisé par
     * l'Article.
     * @see Article
     * @param item noeud duquel on doit récupérer les informations
     * @return un article contenant les informations du noeud fourni en paramètre
     */
    private Article createArticleFromNode(Element item){
        String title, content, author, source, link, sourceHtml = "",
                location = STRING_UNKNOWN, date = STRING_UNKNOWN, thumbnail = STRING_UNKNOWN;

        title = getElementInArticle(item, "title");
        content = getElementInArticle(item, "description");
        author = getElementInArticle(item, "author");
        source = getElementInArticle(item, "source");
        link = getElementInArticle(item, "link");

        if (informationRSS.isLocationSelected())
            location = getElementInArticle(item, "location");
        if (informationRSS.isDateSelected())
            date = formatDate(item, date);
        if (downloadOffline)
           sourceHtml = getHTMLInArticle(link);
        if (informationRSS.isThumbnailSelected())
            thumbnail = getThumbnailInArticle(getThumbnailURL(item), link);

        List<String> hashtags = new ArrayList<>();
        return new ArticleBuilder()
                                .setTitle(title)
                                .setContent(content)
                                .setAuthor(author)
                                .setSource(source)
                                .setLink(link)
                                .setLocation(location)
                                .setDate(date)
                                .setHashtags(hashtags)
                                .setThumbnail(thumbnail)
                                .setHTMLPath(sourceHtml)
                                .setRssLink(informationRSS.getLink())
                                .addTag(TAG_NAME_NOT_READ)
                                .setTweet(false)
                                .setTwitterHashtag("")
                                .build();
    }

    private String formatDate(Element item, String newDate) {
        String date;
        date = getElementInArticle(item, "pubDate");
        SimpleDateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH);
        formatter.setTimeZone(TimeZone.getTimeZone("Europe/Brussels"));
        try {
            newDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(formatter.parse(date));

        } catch (ParseException e) {
            ErrorWindow error = new ErrorWindow();
            error.display("Error","Error creating article !", "Couldn't parse the date !");
        }
        return newDate;
    }

    /**
     * Récupère une information d'un noeud. On peut donc lire par exemple
     * le "titre" d'un noeud. Si l'élément demandée n'existe pas,
     * alors on l'information est Unknown.
     * @param item noeud duquel on doit récupérer les informations
     * @param element élément qu'on veut récupérer (titre, lien, source, date, etc...)
     * @return l'information souhaitée du noeud
     */
    private String getElementInArticle(Element item, String element){
        // Dans item, on prend le contenu du premier tag "element" :
        String res = STRING_UNKNOWN;
        try{
           res = item.getElementsByTagName(element).item(0).getTextContent();
        } catch (Exception e){
            //Ne rien faire. C'est un élément dont nous avons pas besoin (#text)
        }
        return res;
    }

    /**
     * @param item Item dans lequel on cherche l'url de la thumbnail
     * @return l'url de la thumbnail
     */
    private String getThumbnailURL(Element item){
        String url = "";
        for (int i  = 0; i < item.getChildNodes().getLength(); i++){
            try {
                url = item.getChildNodes().item(i).getAttributes().getNamedItem("url").getTextContent();
            }
            catch (Exception e){
                //Pas une image, on peut skip
            }
        }
        return url;
    }

    /**
     * @return la liste de tous les articles qu'on a parsé
     */
    public List<Article> getArticles(){
        return parsedArticles;
    }


    /**
     * Convertit la vignette sous forme de string en base 64 en une image.
     * @param path la vignette
     * @return une image correspondant a la vignette
     */
    public static Image thumbnailStringToImage(String path){
        if (path.equals(STRING_UNKNOWN)) return new Image(ParserRSS.class.getResource(IMG_UNAVAILABLE_PATH).toString());
        try {
            return new Image(new FileInputStream(path));
        } catch (FileNotFoundException e) {
            return new Image(ParserRSS.class.getResource(IMG_UNAVAILABLE_PATH).toString());
        }
    }

    /**
     * Si la lastBuildDate du RSS est plus récente que celle
     * stockée dans l'informationRSS, c'est que le feed a été
     * updated depuis la dernière fois donc on retrieve les
     * articles à nouveau et on update la lastBuildDate de l'informationRSS
     */
    private void updateArticles(){
        if (lastBuildDate.compareTo(informationRSS.getLastBuildDate()) <= 0) {
            return;
        }
        createArticles();
        informationRSS.setLastBuildDate(lastBuildDate);
        DAOFactory.getInformationRSSDAO().update(informationRSS);
    }
}
