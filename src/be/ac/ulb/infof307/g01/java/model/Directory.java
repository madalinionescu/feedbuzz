package be.ac.ulb.infof307.g01.java.model;

import be.ac.ulb.infof307.g01.java.model.dao.DAOFactory;
import io.jsondb.annotation.Document;
import io.jsondb.annotation.Id;

import java.util.ArrayList;

/**
 * Représente un dossier dans le TreeView. On pourra ranger des articles
 * et sous-dossiers dans un dossier.
 */
@Document(collection="Directory", schemaVersion="1.0")

public class Directory extends Node{

    @Id
    private String id;

    private ArrayList<String> nodes = new ArrayList<>();

    public Directory(){
        super();
    }

    public Directory(String name){
        super(name);
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Pour jsonDB
     */

    public ArrayList<String> getNodes() {
        return nodes;
    }

    /**
     * pour jsonDB
     * @param nodes
     */
    public void setNodes(ArrayList<String> nodes) {this.nodes = nodes;}

    public ArrayList<Node> nodeList(){
        ArrayList<Node> res = new ArrayList<>();

        for (String node: nodes){
            Node tmpNode = DAOFactory.getDirectoryDAO().findById(node);
            if (tmpNode == null) {
                tmpNode = DAOFactory.getArticleDAO().findById(node);
            }

            if (tmpNode != null) {
                res.add(tmpNode);
            }
        }

        return res;
    }

    public void addNode(Node node){
        String nodeId = node.getId();
        if(!nodes.contains(nodeId))
            nodes.add(nodeId);
    }

    @Override
    public boolean nodeIsFolder(){
        return true;
    }


    public void deteleNode(Node node){
        for (String nodeId: nodes){
            if (nodeId.equals(node.getId())) {
                nodes.remove(node.getId());
                break;
            }
            else{
                Node tmpNode = DAOFactory.getDirectoryDAO().findById(node.getId());
                if (tmpNode != null) {
                    if (tmpNode.nodeIsFolder()) deteleNode(node);
                }
            }
        }
    }

}
