package be.ac.ulb.infof307.g01.java.model;

import be.ac.ulb.infof307.g01.java.model.database.OwnedModel;
import io.jsondb.annotation.Document;
import io.jsondb.annotation.Id;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Cette class definit un interet de l'utilisateur
 */
@Document(collection="Interest", schemaVersion="1.0")
public class Interest extends OwnedModel {
    private ArrayList<String> relatedRSSLinks;
    private ArrayList<Tag> relatedTags;

    @Id
    private String id;

    public Interest() {}

    public Interest(String name, ArrayList<String> rssList, ArrayList<Tag> relatedTags){
        this.id = name;
        this.relatedRSSLinks = rssList;
        this.relatedTags = relatedTags;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void setId(String id) {
        this.id=id;
    }

    /**
     * Fonction qui renvoie la liste des links rss qui definissent l'interet
     * @return
     */
    public  ArrayList<String> getRelatedRSSLinks() { return this.relatedRSSLinks; }

    public void setRelatedRSSLinks(ArrayList<String> rssLinks) {this.relatedRSSLinks = rssLinks; }

    /**
     * Fonction qui renvoie la liste des tags qui definissent l'interet
     * @return
     */
    public ArrayList<Tag> getRelatedTags() {
        return relatedTags;
    }

    public void setRelatedTags(ArrayList<Tag> relatedTags) {
        this.relatedTags = relatedTags;
    }

    @Override
    public boolean canGetNewId() {
        return false;
    }

    /**
     * Fonction qui detecte si le rss definit l'interet ou pas
     * @param otherRss: un rss de type InformationRSS
     * @return true si le rss definit l'interet, false sinon
     */
    public boolean isRelatedToRSS(InformationRSS otherRss){
        for(String rss: this.relatedRSSLinks){
            if (Objects.equals(rss, otherRss.getLink())){
                return true;
            }
        }
        return false;
    }

    /**
     * Fonction qui detecte si le rss definit l'interet ou pas
     * @param otherTag: un tag de type Tag
     * @return true si le tag definit l'interet, false sinon
     */
    public boolean isRelatedToTag(Tag otherTag){
        for(Tag tag: this.relatedTags ){
            if (tag == otherTag){
                return true;
            }
        }
        return false;
    }

    /**
     * Fonction qui ajoute un link rss dans la liste des rss
     * @param rssLink: un link qui doit etre ajouté dans la liste
     *               des links rss qui definissent l'interet
     */
    public void addRSS(String rssLink){
        this.relatedRSSLinks.add(rssLink);
    }

    /**
     * Fonction qui ajoute un tag dans la liste des tags
     * @param tag: un tag qui doit etre ajouté dans la liste
     *               des tags qui definissent l'interet
     */
    public void addTag(Tag tag){
        this.relatedTags.add(tag);
    }


}
