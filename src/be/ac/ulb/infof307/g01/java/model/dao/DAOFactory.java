package be.ac.ulb.infof307.g01.java.model.dao;

import be.ac.ulb.infof307.g01.java.model.LocalUser;
import be.ac.ulb.infof307.g01.java.model.User;
import be.ac.ulb.infof307.g01.java.model.database.Database;
import be.ac.ulb.infof307.g01.java.view.window.ErrorWindow;

/**
 * Cette classe permet de donner accès aux DAO à partir de
 * methode static. Elle est correspond au design pattern 'factory'
 * https://fr.wikipedia.org/wiki/Fabrique_(patron_de_conception)
 */
public class DAOFactory {
    private static final String PATH = "./db";
    private static Database db;

    static {
        try{
            db = new Database(PATH);
        }
        catch (Exception e){
            ErrorWindow error = new ErrorWindow();
            error.display("Error","Error in Database !", e.toString());
        }
    }

    /**
     * Change le fichier de la database
     * @param path path de la db
     */
    public static void changeDatabasePath(String path){
        db = new Database(path);
    }

    /**
     * @return le DAO qui gère les articles
     */
    public static ArticleDAO getArticleDAO(){
        User user = LocalUser.singleton.getUser();
        if(user == null){
            throw new NullPointerException("The user must be connected !");
        }
        return new ArticleDAO(db,user);
    }

    /**
     * @return Retourne le DAO qui gère les articles
     */
    public static InformationRSSDAO getInformationRSSDAO(){
        User user = LocalUser.singleton.getUser();
        if(user == null){
            throw new NullPointerException("The user must be connected !");
        }
        return new InformationRSSDAO(db,user);
    }

    /**
     * @return le DAO qui gère les users
     */
    public static UserDAO getUserDAO(){

        return new UserDAO(db);
    }

    /**
     * @return le DAO qui gère les directory
     */
    public static DirectoryDAO getDirectoryDAO(){
        User user = LocalUser.singleton.getUser();
        if(user == null){
            throw new NullPointerException("The user must be connected !");
        }

        return new DirectoryDAO(db,user);
    }

    /**
     * @return le DAO qui gère les interests
     */
    public static InterestDAO getInterestDAO(){
        User user = LocalUser.singleton.getUser();
        if(user == null){
            throw new NullPointerException("The user must be connected !");
        }

        return new InterestDAO(db,user);
    }

    /**
     * @return le DAO qui gère les filtres de recherche
     */
    public static SearchFilterDAO getSearchFilterDAO(){
        User user = LocalUser.singleton.getUser();
        if(user == null){
            throw new NullPointerException("The user must be connected !");
        }

        return new SearchFilterDAO(db,user);
    }

    /**
     * @return le DAO qui gère les hashtags Twitter
     */
    public static InformationTwitterDAO getInformationTwitterDAO() {
        User user = LocalUser.singleton.getUser();
        if(user == null){
            throw new NullPointerException("The user must be connected !");
        }
        return new InformationTwitterDAO(db,user);
    }

    public static void clearAll(){
        LocalUser.singleton.disconnect();
        db.clearAll();
    }


}

