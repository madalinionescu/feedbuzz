package be.ac.ulb.infof307.g01.java.model.dao;

import be.ac.ulb.infof307.g01.java.model.SearchFilter;
import be.ac.ulb.infof307.g01.java.model.User;
import be.ac.ulb.infof307.g01.java.model.database.Database;

import java.util.List;

public class SearchFilterDAO extends OwnedDAO<SearchFilter> {
    SearchFilterDAO(Database db, User user) { super(db, user); }

    public List<SearchFilter> findAll() {
        return db.findAll(getModelClass());
    }

    @Override
    public boolean create(SearchFilter filter) {
        List<SearchFilter> existentFilters = findAll();
        for (SearchFilter existentFilter : existentFilters) {
            if (existentFilter.getName().equals(filter.getName())) {
                filter.setName(findNextAvailableName(filter));
                break;
            }
        }
        return super.create(filter);
    }

    @Override
    public Class<SearchFilter> getModelClass() {
        return SearchFilter.class;
    }

    public String findNextAvailableName(SearchFilter filter) {
        boolean found = true;
        int counter = 0;

        while (found) {
            counter++;
            found = false;
            List<SearchFilter> existentFilters = findAll();
            for (SearchFilter existentFilter : existentFilters) {
                found = found || (existentFilter.getName().equals(filter.getName() + " " + counter));
            }
        }

        return filter.getName() + " " + counter;
    }

    public SearchFilter findByString(String name){
        return db.findById(name,getModelClass());
    }
}


