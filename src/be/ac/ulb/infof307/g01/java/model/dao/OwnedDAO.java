package be.ac.ulb.infof307.g01.java.model.dao;

import be.ac.ulb.infof307.g01.java.model.User;
import be.ac.ulb.infof307.g01.java.model.database.Database;
import be.ac.ulb.infof307.g01.java.model.database.Model;
import be.ac.ulb.infof307.g01.java.model.database.OwnedModel;

import java.util.ArrayList;
import java.util.List;

/**
 * OwnedDAO est le DAO pour tous les données qui appartiennent à
 * des utilisateurs.
 *
 * @param <T>
 */

public abstract class OwnedDAO<T extends OwnedModel> extends DAO<T> {

    private final User user;

    /**
     * constructeur
     *
     * @param db database
     */
    OwnedDAO(Database db, User user) {
        super(db);
        this.user = user;
    }

    /**
     * crée l'object dans la database avec l'user associé
     *
     * @param model le model
     */
    @Override
    public boolean create(T model) {
        if (user == null || user.getId() == null || user.getId().equals("") || model == null) {
            return false;
        }

        model.setUserID(user.getId());
        return db.create(model);
    }

    /**
     * récupère l'object de la database
     * @param id l'id du model
     */
    @Override
    public T findById(String id){
        T model = db.findById(id, getModelClass());
        if(model == null || user == null || !user.getId().equals(model.getUserID())){
            return null;
        }
        return model;
    }

    /**
     * récupère tous les object de la database du models
     */
    public List<T> findAll() {
        if(user ==null) return null;
        List<T> list = new ArrayList<>();
        for (T item : db.findAll(getModelClass())) {
            if (item.getUserID().equals(user.getId())) {
                list.add(item);
            }
        }
        return list;
    }

    /**
     * supprime toute les objets du models de l'utilisateur
     */
    public void clear() {

        for (T item : db.findAll(getModelClass())) {
            if (item.getUserID().equals(user.getId()))
                delete(item);
        }
    }

    public boolean isFound(Model model){
        return db.isFound(model);
    }
}
