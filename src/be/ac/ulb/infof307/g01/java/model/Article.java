package be.ac.ulb.infof307.g01.java.model;

import be.ac.ulb.infof307.g01.java.model.database.OwnedModel;
import io.jsondb.annotation.Document;
import io.jsondb.annotation.Id;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * Classe article représentant un article provennant d'un flux RSS ou
 * d'un site web.
 * Il contient les informations suivantes :
 * Le titre, le contenu, l'auteur, la source, le lien, la localisation, la date, ainsi
 * qu'un hashtag
 */
@Document(collection="Article", schemaVersion="1.0")
public class Article extends Node {
    private String title;
    private String content;
    private String author;
    private String source;
    private String link;
    private String location;
    private String date;
    private List<String> hashtags;
    private String thumbnail;
    private boolean trash;
    private String rssLink;
    private List<Tag> tags;
    private String HTMLPath;
    private boolean tweet;
    private String twitterHashtag;

    @Id
    private String id;

    /**
     * Constructor pour jsonDB
     */
    public Article(){
        this.tags = new ArrayList<>();
    }

    @Override
    public String getId(){return id;}
    @Override
    public  void setId(String value){ this.id = value;}

    @Override
    public String getName() {
        return title;
    }
    @Override
    public void setName(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }

    public String getLink() {
        return link;
    }
    public void setLink(String link) {
        this.link = link;
    }

    public Boolean getTrash() {
        return trash;
    }
    public void setTrash(Boolean isDelete) {
        this.trash = isDelete;
    }

    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }

    public List<String> getHashtags() {
        return hashtags;
    }
    public void setHashtags(List<String> hashtags) {
        this.hashtags = hashtags;
    }
    public void addHashtag(String tag) {
        hashtags.add(tag);
    }

    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }

    public String getThumbnail() { return thumbnail; }
    public void setThumbnail(String thumbnail){this.thumbnail = thumbnail;}

    public List<Tag> getTags() {
        return tags;
    }
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }
    public void addTag(String name){
        this.tags.add(new Tag(name));
    }
    public void removeTag(String name) {
        Tag toRemove = null;
        for (Tag tag: this.tags){
            if(Objects.equals(tag.getName(), name)){
                toRemove = tag;
            }
        }
        this.tags.remove(toRemove);
    }

    public String getRssLink() {
        return rssLink;
    }
    public void setRssLink(String rssLink) {
        this.rssLink = rssLink;
    }

    public String getHTMLPath() { return HTMLPath; }
    public void setHTMLPath(String sourceHtml) {
        this.HTMLPath = sourceHtml;
    }

    public boolean isTweet() { return tweet; }

    public void setTweet(boolean tweet) { this.tweet = tweet; }

    public void setTwitterHashtag(String twitterHashtag) { this.twitterHashtag = twitterHashtag; }

    public String getTwitterHashtag() { return twitterHashtag; }

    @Override
    public String toString() {
        return this.title;
    }

    /**
     * Permet de comparer l'égalité entre 2 objets articles
     * @param o un autre objet article
     * @return true si les articles sont identiques, false sinon
     */
    private boolean isEqualsModel(Article o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        if (trash != o.trash) return false;
        if (!Objects.equals(title, o.title)) return false;
        if (!Objects.equals(content, o.content)) return false;
        if (!Objects.equals(author, o.author)) return false;
        if (!Objects.equals(source, o.source)) return false;
        if (!Objects.equals(link, o.link)) return false;
        if (!Objects.equals(location, o.location)) return false;
        if (!Objects.equals(date, o.date)) return false;
        if (!Objects.equals(hashtags, o.hashtags)) return false;
        if (!Objects.equals(thumbnail, o.thumbnail)) return false;
        if (!Objects.equals(rssLink, o.rssLink)) return false;
        if (!Objects.equals(twitterHashtag, o.twitterHashtag)) return false;
        if ( !(tweet == o.tweet) ) return false;
        return Objects.equals(tags, o.tags);
    }

    /**
     * Permet de comparer l'égalité entre 2 objets articles
     * @param o un autre objet article
     * @return true si les articles sont identiques, false sinon
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Article article = (Article) o;
        if(!isEqualsModel(article)){
            return false;
        }
        return Objects.equals(id, article.id);
    }
}
