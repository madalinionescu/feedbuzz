package be.ac.ulb.infof307.g01.java.model;


import be.ac.ulb.infof307.g01.java.model.database.OwnedModel;
import io.jsondb.annotation.Document;
import io.jsondb.annotation.Id;

import java.util.Date;

/**
 * Cette classe gère les données liées à un flux RSS. On a ainsi son lien,
 * ainsi que le nombre d'articles à récupérer de ce RSS. On a aussi des
 * booléens représentant si oui ou non on doit récupérer une donnée (titre, date, etc.)
 */
@Document(collection="InformationRSS", schemaVersion="1.0")
public class InformationRSS extends OwnedModel {
    @Id
    private String id;
    private String link;
    private int nbArticles = 0;
    private boolean titleSelected = true;
    private boolean contentSelected = true;
    private boolean authorSelected = true;
    private boolean linkSelected = true;
    private boolean sourceSelected = true;
    private boolean locationSelected;
    private boolean dateSelected;
    private boolean thumbnailSelected;
    private Date lastBuildDate = new Date("Wed, 27 Mar 2000 17:35:10 +0100");

    /**
     * Constructor par default pour jsonDB
     */
    public InformationRSS(){}

    /**
     * @param link
     */
    public InformationRSS(String link){
        this(link, 3, true, true, true);
    }

    public InformationRSS(String link, int nbArticles){
        this(link, nbArticles, true, true, true);
    }

    /**
     * @param link lien du RSS
     * @param nbArticles nbre d'articles à récup
     * @param locationSelected récup la localisation ?
     * @param dateSelected récup la date ?
     */
    public InformationRSS(String link, int nbArticles, boolean locationSelected, boolean dateSelected, boolean thumbnailSelected) {
        this.link = link;
        this.nbArticles = nbArticles;
        this.locationSelected = locationSelected;
        this.dateSelected = dateSelected;
        this.thumbnailSelected = thumbnailSelected;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getNbArticles() {
        return nbArticles;
    }

    public void setNbArticles(int nbArticles) {
        this.nbArticles = nbArticles;
    }

    public boolean isTitleSelected() {
        return titleSelected;
    }

    public void setTitleSelected(boolean titleSelected) {
        this.titleSelected = titleSelected;
    }

    public boolean isContentSelected() {
        return contentSelected;
    }

    public void setContentSelected(boolean contentSelected) {
        this.contentSelected = contentSelected;
    }

    public boolean isAuthorSelected() {
        return authorSelected;
    }

    public void setAuthorSelected(boolean authorSelected) {
        this.authorSelected = authorSelected;
    }

    public boolean isLinkSelected() {
        return linkSelected;
    }

    public void setLinkSelected(boolean linkSelected) {
        this.linkSelected = linkSelected;
    }

    public boolean isSourceSelected() {
        return sourceSelected;
    }

    public void setSourceSelected(boolean sourceSelected) {
        this.sourceSelected = sourceSelected;
    }

    public boolean isLocationSelected() {
        return locationSelected;
    }

    public void setLocationSelected(boolean locationSelected) {
        this.locationSelected = locationSelected;
    }

    public boolean isDateSelected() {
        return dateSelected;
    }

    public void setDateSelected(boolean dateSelected) {
        this.dateSelected = dateSelected;
    }

    public boolean isThumbnailSelected() {
        return thumbnailSelected;
    }

    public void setThumbnailSelected(boolean thumbnailSelected) {
        this.thumbnailSelected = thumbnailSelected;
    }

    /**
     * @return id l'id sous forme d'entier
     */
    @Override
    public String getId(){return id;}

    /**
     * @param value une valeur sous forme d'entier
     */
    @Override
    public  void setId(String value){ this.id = value;}

    public Date getLastBuildDate() {
        return lastBuildDate;
    }

    public void setLastBuildDate(Date lastBuildDate) {
        this.lastBuildDate = lastBuildDate;
    }
}

