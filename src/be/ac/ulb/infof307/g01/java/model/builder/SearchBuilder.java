package be.ac.ulb.infof307.g01.java.model.builder;

import be.ac.ulb.infof307.g01.java.model.Search;
import be.ac.ulb.infof307.g01.java.model.SearchFilter;

import java.util.List;

/**
 * Builder pour Search qui contient plein de fonctions pour set
 * plusieurs attributs possibles d'une recherche
 */
public class SearchBuilder implements BuilderImpl {

    private final Search searchTmp;

    public SearchBuilder() {searchTmp = new SearchFilter(); }

    public Search build(){
        Search tmp = new Search();
        tmp.setDateComparison(searchTmp.getDateComparison());
        tmp.setDate(searchTmp.getDate());
        tmp.setAuthor(searchTmp.getAuthor());
        tmp.setContent(searchTmp.getContent());
        tmp.setLocation(searchTmp.getLocation());
        tmp.setSource(searchTmp.getSource());
        tmp.setTag(searchTmp.getTag());
        tmp.setTitle(searchTmp.getTitle());
        tmp.setHashtags(searchTmp.getHashtags());
        tmp.setLanguage(searchTmp.getLanguage());
        return tmp;
    }

    public SearchBuilder setHashtag(List<String> hashtags) {
        searchTmp.setHashtags(hashtags);
        return this;
    }

    public SearchBuilder setAuthor(String author) {
        searchTmp.setAuthor(author);
        return this;
    }

    public SearchBuilder setTitle(String title) {
        searchTmp.setTitle(title);
        return this;
    }

    public SearchBuilder setContent(String content) {
        searchTmp.setContent(content);
        return this;
    }

    public SearchBuilder setDate(String date) {
        searchTmp.setDate(date);
        return this;
    }

    public SearchBuilder setLocation(String location) {
        searchTmp.setLocation(location);
        return this;
    }


    public SearchBuilder setSource(String source) {
        searchTmp.setSource(source);
        return this;
    }

    public SearchBuilder setDateComparison(String dateComparison) {
        searchTmp.setDateComparison(dateComparison);
        return this;
    }

    public SearchBuilder setTag(String tag) {
        searchTmp.setTag(tag);
        return this;
    }

}
