package be.ac.ulb.infof307.g01.java.model.dao;

import be.ac.ulb.infof307.g01.java.model.Interest;
import be.ac.ulb.infof307.g01.java.model.User;
import be.ac.ulb.infof307.g01.java.model.database.Database;

import java.util.List;

public class InterestDAO extends OwnedDAO<Interest> {
    /**
     * constructeur du DAO
     *
     * @param db database
     */
    InterestDAO(Database db, User user) {
        super(db,user);
    }

    /**
     * Recherche de l'interet dans la basse de données, basée sur
     * le nom de l'interet
     * @param name nom de l'interet
     * @return un objet de type Interest
     */
    public Interest findByString(String name){
        return db.findById(name,getModelClass());
    }

    /**
     * Cherche tous les interets dans la base de données
     * @return une liste d'objets de type Interest
     */
    public List<Interest> findAll() {
        return db.findAll(getModelClass());
    }

    /**
     * Crée un interet dans la base de données
     */
    @Override
    public boolean create(Interest model){
        return db.create(model);
    }

    @Override
    public Class<Interest> getModelClass() {
        return Interest.class;
    }


}
