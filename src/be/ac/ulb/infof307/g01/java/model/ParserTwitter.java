package be.ac.ulb.infof307.g01.java.model;

import be.ac.ulb.infof307.g01.java.model.dao.DAOFactory;
import be.ac.ulb.infof307.g01.java.model.builder.ArticleBuilder;
import be.ac.ulb.infof307.g01.java.view.window.ErrorWindow;
import org.apache.commons.io.FileUtils;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

import static be.ac.ulb.infof307.g01.java.Constants.STRING_UNKNOWN;
import static be.ac.ulb.infof307.g01.java.Constants.TAG_NAME_NOT_READ;


/**
 * ParserTwitter est une classe qui va gérer le parsing des tweets pour un hashtag donné.
 * Il faut lui donner un hashtag. Ensuite, il parsera le hashtag afin de créer un objet
 * Article avec les différentes données (titre, description, author, lien, etc.)
 * On stocke tous ces articles dans une liste.
 * @see Article
 */

public class ParserTwitter extends Parser {
    private final Twitter twitter;
    private final InformationTwitter infoTwitter;
    private final List<Article> tweetsList = new ArrayList<>();


    /**
     * A la construction, un générateur est utilisé pour configurer twitter4j
     * avec les paramètres souhaités.
     * Twitter4J est une librairie Java, qui fournit une API pour accéder à l'API Twitter.
     *
     * @param infoTwitter: contient les informations necessaires pour
     *                           extraire des tweets pour un hashtag donné
     */
    public ParserTwitter(final InformationTwitter infoTwitter){
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.setDebugEnabled(true)
                .setOAuthConsumerKey("d0zwpBGnaVLPfSAa4F2F2ht5v")
                .setOAuthConsumerSecret("cK3umhanTk1VDuNaXeU7hQtXvQovjrshEcntZbpVxuw5m2JU9L")
                .setOAuthAccessToken("73688266-l0Km87SpyDeFFttVN5B1Oyd4d9tcDS8rEodNOGMux")
                .setOAuthAccessTokenSecret("AbDe4hLlNqPxLAjCVoXrLzrJdPwPpNeiopgvuzRx4nyBi");
        TwitterFactory twitterFactory = new TwitterFactory(configurationBuilder.build());
        twitter = twitterFactory.getInstance();

        this.infoTwitter = infoTwitter;
        createTweets(retrieveTweets());
    }

    private List<Status> getAccountTweets(String account) {
        Paging p = new Paging();
        p.setCount(infoTwitter.getNumberPosts());
        List<Status> feed;
        try {
            feed = twitter.getUserTimeline(account,p);
        } catch (TwitterException e) {
            feed = new ArrayList<>();
            //Account Doesn't exist;
        }
        return feed;
    }

    /**
     * @return la requête cherchant les tweets les plus recents et les plus populaires
     */
    private Query getTopQuery(){
        Query query = new Query("#"+ infoTwitter.getName());
        query.setResultType(Query.POPULAR); // searches for most popular tweets with a given keyword
        return query;
    }

    /**
     * @return la requête cherchant les tweets les plus recents
     */
    private Query getSimpleQuery(){
        return new Query("#" + infoTwitter.getName());
    }

    /**
     * Cherche les tweets les plus recents et les plus populaires
     * @return les resulats de la recherche
     */
    private QueryResult searchRecentTopTweets(){
        Query query = getTopQuery();
        return search(query);
    }

    /**
     * Cherche les tweets les plus recents
     * @return les resulats de la recherche
     */
    private QueryResult searchRecentTweets(){
        Query query = getSimpleQuery();
        return search(query);
    }

    /**
     * Recherche de tweets basée sur une requête donnée en parametre
     * @param query: la requête pour la recherche
     * @return les resulats de la recherche
     */
    private QueryResult search(Query query){
        QueryResult result = null;
        try {
            result = twitter.search(query);
        } catch (TwitterException e) {
            ErrorWindow error = new ErrorWindow();
            error.display("Error","Connection failed!", "An error occurred while " +
                    "searching for tweets");
        }
        return result;
    }

    /**
     * Recupere tous les tweets les plus recents et les plus populaires pour l'instant.
     * @return une liste de status twitter
     */
    private ArrayList<Status> retrieveRecentTopTweets() {
        ArrayList<Status> tweets = new ArrayList<>();
        QueryResult result = searchRecentTopTweets();
        handleRateLimit(result.getRateLimitStatus());
        long lastID = Long.MAX_VALUE;
        int flag = 0;

        Query query = getTopQuery();

        /* Tant que le nombre de tweets trouvés ne satisfait pas
           le nombre de tweets souhaités par l'utilisateur,
           on continue la recherche.
           Si le nombre de tweets trouvé reste toujours le meme,
           on arrete la boucle.
         */

        if (DAOFactory.getInformationTwitterDAO().isAccount(this.infoTwitter)) {
            return (ArrayList<Status>) this.getAccountTweets(infoTwitter.getName());
        }

        while (tweets.size () < infoTwitter.getNumberPosts()) {
            query.setCount(infoTwitter.getNumberPosts()- tweets.size());
            int size = tweets.size();
            result = search(query);
            handleRateLimit(result.getRateLimitStatus());
            tweets.addAll(result.getTweets());
            for (Status status: tweets){
                if(status.getId() < lastID)
                    lastID = status.getId();
            }
            query.setMaxId(lastID-1);
            if(size == tweets.size())
                flag += 1;
            if (flag == 3) //si le nombre de tweets trouvés reste le meme 3 fois de suite
                break;
        }
        return tweets;
    }

    /**
     * Extrait les tweets pour un hashtag donné.
     * D'abord elle cherche des tweets qui sont les plus populaires et les plus
     * recents. Si le nombre des tweets n'atteint pas le nombre de
     * tweets attendu (nombre de tweets choisis par l'utilisateur), alors
     * on fait la recherche des tweets qui sont restés:
     * le plus recents (mais pas les plus populaires).
     * @return les tweets trouvés
     */
    private ArrayList<Status> retrieveTweets(){
        ArrayList<Status> tweets = retrieveRecentTopTweets();
        int numberOfPosts;
        if (this.infoTwitter != null) {numberOfPosts = infoTwitter.getNumberPosts();}
        else {numberOfPosts = infoTwitter.getNumberPosts();}

        if (numberOfPosts > tweets.size()){
            QueryResult result = searchRecentTweets();
            if (result != null) {
                for (Status tweet : result.getTweets()) {
                    if (!tweets.contains(tweet) && tweets.size()<numberOfPosts){
                        tweets.add(tweet);
                    }
                }
            }
        }
        return tweets;
    }

    /**
     * Sauvegarde dans une liste les tweets trouvés suite à une recherche
     * basée sur un hashtag donné.
     * Ils sont sauvegardés sous forme d'objets de type Article
     * @param tweets : la liste de tweets de type Article
     */
    private void createTweets(ArrayList<Status> tweets){
        for (Status tweet: tweets) {
            tweetsList.add(createTweet(tweet));
        }
    }

    private String formatUrl(String url){
        List<String> extension = new ArrayList<>(Arrays.asList(".png", ".jpg", ".jpeg"));
        for (String elem : extension){
            if (url.contains(elem)){
                return url.substring(0, url.indexOf(elem)).concat(elem);
            }
        }
        return url;
    }

    /**
     * Extrait les informations du tweet de type Status et
     * crée un nouvel objet de type Article contenat les informations
     * importantes du tweet: le contenu, l'auteur, la date (etc.)
     *
     * @param tweet: objet de type Status qui contient toutes
     *             les données d'un tweet
     * @return l'objet de type Article contenant juste les
     * informations du tweet qui nous interessent.
     */
    private Article createTweet(Status tweet){
        String title, content, author, source,
            link, location = "Unknown", date, thumbnail,sourceHtml;

        content = tweet.getText();
        author = tweet.getUser().getScreenName();
        if (DAOFactory.getInformationTwitterDAO().isAccount(this.infoTwitter)) {
            title = tweet.getUser().getName() + " " + "(@" + author + ") tweeted ";
        }
        else {
            title = tweet.getUser().getName() + " " + "(@"+author+") "+ "tweeted with #"+ infoTwitter.getName();
        }
        source = "Twitter";
        link =  "https://twitter.com/" + author + "/status/" + tweet.getId();
        date = formatDate(tweet.getCreatedAt()); // format (yyyy-MM-dd HH:mm:ss) + converti temps local
        thumbnail = getThumbnailInArticle(tweet,link);
        sourceHtml = getHTMLInArticle(link);

        List<String> hashtags = new ArrayList<>();
        ArticleBuilder articleBuilder = new ArticleBuilder()
                .setTitle(title)
                .setContent(content)
                .setAuthor(author)
                .setSource(source)
                .setLink(link)
                .setLocation(location)
                .setDate(date)
                .setHashtags(hashtags)
                .setHTMLPath(sourceHtml)
                .setThumbnail(thumbnail)
                .setRssLink("")
                .addTag(TAG_NAME_NOT_READ)
                .setTweet(true);
        if (DAOFactory.getInformationTwitterDAO().isHashtag(this.infoTwitter)) {
            articleBuilder.setTwitterHashtag(this.infoTwitter.getName());
        }
        return articleBuilder.build();
    }

    /**
     * Récupère les vignettes des articles sous l'encodage base 64.
     * @param tweet objet de type Status représentant le tweet
     * @param link le link vers le tweet
     * @return le path où on a enregistré l'image dans la db
     */
    private String getThumbnailInArticle(Status tweet,String link){
        // recupere le lien vers l'image de profil de l'utilisateur qui a posté ce tweet
        String url = tweet.getUser().getOriginalProfileImageURL();
        try {
            java.net.URL imageUrl = new java.net.URL(formatUrl(url));
            InputStream inputStream = imageUrl.openStream();

            String path = "./db/thumbnails/" + link.replace("/", "_")
                    .replace(":", "")
                    .replace(".", "") + ".jpg";
            File targetFile = new File( path);
            FileUtils.copyInputStreamToFile(inputStream, targetFile);
            return path;

        } catch (Exception e) {
            return STRING_UNKNOWN;
        }
    }

    private String formatDate(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    }

    public InformationTwitter getInfoTwitter(){
        return this.infoTwitter;
    }

    public List<Article> getTweetsList(){
        return tweetsList;
    }

    /**
     * Fonction qui gere la limite de demandes qu'on peut faire dans
     * un intervalle de temps.
     * Si la limite est atteinte, l'execution du thread en cours est suspendue
     * pendant une durée spécifiée en millisecondes.
     * @param rateLimitStatus : est un objet de type RateLimitStatus qui
     *                        représente le statut de limite de débit de l'API.
     */
    private void handleRateLimit(RateLimitStatus rateLimitStatus) {
        if(rateLimitStatus!=null) {
            if (rateLimitStatus.getRemaining() == 0) {
                int resetTime = rateLimitStatus.getSecondsUntilReset() + 5;
                int sleep = (resetTime * 1000);
                try {
                    if (sleep > 0)
                        Thread.sleep(sleep);
                    else
                        Thread.sleep(0);
                } catch (InterruptedException e) {
                    ErrorWindow error = new ErrorWindow();
                    error.display("Error",e.getMessage(),e.getMessage());
                }
            }
        }
    }
}
