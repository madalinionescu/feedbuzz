package be.ac.ulb.infof307.g01.java.model;

import be.ac.ulb.infof307.g01.java.model.database.OwnedModel;
import io.jsondb.annotation.Document;
import io.jsondb.annotation.Id;

import static be.ac.ulb.infof307.g01.java.Constants.ACCOUNT;
import static be.ac.ulb.infof307.g01.java.Constants.HASHTAG;

/**
 * Cette classe gère les abonnements aux hashtags de Twitter.
 * On a le nom du hashtag donné, le nombre d'articles à récupérer pour ce hashtag.
 */
@Document(collection="InformationTwitter", schemaVersion="1.0")
public class InformationTwitter extends OwnedModel {


    @Id
    private String id;

    private String name;
    private String type;
    private int numberPosts;



    public InformationTwitter() {}

    public InformationTwitter(String name, int numberPosts, String type){
        this.name = name;
        this.type = type;
        this.numberPosts = numberPosts;
    }

    @Override
    public String getId() { return id;}

    public String getType() {return this.type;}

    public void setType(String newType) {
        if (newType.equals(ACCOUNT) || newType.equals(HASHTAG)) {
            this.type = newType;
        }
    }

    @Override
    public void setId(String id) { this.id = id;}

    public int getNumberPosts() {
        return numberPosts;
    }

    public void setNumberPosts(int numberPosts) {
        this.numberPosts = numberPosts;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
