package be.ac.ulb.infof307.g01.java.model;

import io.jsondb.annotation.Document;
import io.jsondb.annotation.Id;

/**
 * Représente un filtre de recherche qu'on peut sauvegarder
 * pour être utilisé plus tard
 */
@Document(collection="SearchFilter", schemaVersion="1.0")
public class SearchFilter extends Search {

    @Id //Clé pour la base de donnée
    private String id;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id=id;
    }

    @Override
    public boolean canGetNewId() {
        return false;
    }

    public SearchFilter() {
        super();
    }

    public SearchFilter(Search search, String name) {
        super(search);
        this.id = name;
    }

    public String getName() {
        return getId();
    }

    public void setName(String name) {
        setId(name);
    }
}
