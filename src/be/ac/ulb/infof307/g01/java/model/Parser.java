package be.ac.ulb.infof307.g01.java.model;

import be.ac.ulb.infof307.g01.java.utils.FileUtils;
import be.ac.ulb.infof307.g01.java.utils.HtmlParser;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static be.ac.ulb.infof307.g01.java.Constants.STRING_UNKNOWN;

abstract class Parser {
    /**
     * Récupère les vignettes des articles sous l'encodage base 64.
     * @param urlImage url de l'image du tweet
     * @param link le link vers le tweet
     * @return le path où on a enregistré l'image dans la db
     */

    String getThumbnailInArticle(String urlImage, String link){
        String path = "./db/thumbnails/" + link.replace("/", "_")
                .replace(":", "")
                .replace(".", "") + ".jpg";
        if (FileUtils.fileExists(path)) return path;
        try {
            java.net.URL imageUrl = new java.net.URL(formatUrl(urlImage));
            InputStream is = imageUrl.openStream();
            FileUtils.writeInputStreamToFile(path, is);
            return path;
        } catch (Exception e) {
            return STRING_UNKNOWN;
        }
    }

    /**
     * Sauvgarde l'html de l'article dans un fichier txt
     * @param link Lien de l'article
     * @return Path dans lequel est stocké l'html de l'article
     */
    public String getHTMLInArticle(String link){
        String html = HtmlParser.htmlWithSource(link,true,true);
        String path = "./db/htmls/" + link.replace("/", "_")
                .replace(":", "")
                .replace(".", "") + ".txt";

        if (FileUtils.fileExists(path)) return path;

        InputStream is = new ByteArrayInputStream(html.getBytes(StandardCharsets.UTF_8));
        FileUtils.writeInputStreamToFile(path, is);
        return path;
    }

    private String formatUrl(String url){
        List<String> extension = new ArrayList<>(Arrays.asList(".png", ".jpg", ".jpeg"));
        for (String elem : extension){
            if (url.contains(elem)){
                return url;
            }
        }
        return url;
    }
}
