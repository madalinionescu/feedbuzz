package be.ac.ulb.infof307.g01.java.model;

import be.ac.ulb.infof307.g01.java.model.database.Model;
import io.jsondb.annotation.Document;
import io.jsondb.annotation.Id;
import io.jsondb.annotation.Secret;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Date;

/**
 * Représente un user et tout ce qui lui est lié
 * (tags, password, interet, search history, etc)
 */
// Définir le fichier database
@Document(collection="User", schemaVersion="1.0")
public class User extends Model {
    @Secret
    private String hash;
    private String mail;

    //Clé d'accès au user
    @Id
    private String username;

    private ArrayList<String> searchHistory;
    private List<Tag> userTags;
    private Integer loginCount;
    private Date lockedUntil;
    private ArrayList<String> userInterests;

    /**
     * Constructor par default pour jsonDB
     */
    public User(){}

    /**
     * @param username
     * @param hash
     * @param mail
     * @param searchHistory
     */
    public User(String username, String hash, String mail, ArrayList<String> searchHistory) {
        this.username = username;
        this.hash = hash;
        this.mail = mail;
        this.lockedUntil = new Date();
        this.loginCount = 0;
        this.searchHistory = searchHistory;
        this.userTags = new ArrayList<>();
    }


    public String getUsername() {
        return username;
    }

    /**
     * @return hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @return le mail
     */
    public String getMail() {
        return mail;
    }

    /**
     * @return le loginCount
     */
    public Integer getLoginCount() { return loginCount; }

    /**
     * @return le loginCount
     */
    public Date getLockedUntil() { return lockedUntil; }

    /**
     * @param newUsername
     */
    public void setUsername(String newUsername) {
        this.username = newUsername;
    }

    /**
     * @param newHash
     */
    public void setHash(String newHash) {
        this.hash = newHash;
    }

    /**
     * @param newMail
     */
    public void setMail(String newMail) {
        this.mail = newMail;
    }

    public List<Tag> getUserTags() {
        return userTags;
    }

    public void setUserTags(List<Tag> userTags) {
        this.userTags = userTags;
    }

    /**
     * @param newLoginCount
     */
    public void setLoginCount(Integer newLoginCount) { this.loginCount = newLoginCount; }
    /**
     * @param newLockedUntil
     */
    public void setLockedUntil(Date newLockedUntil) { this.lockedUntil = newLockedUntil; }

    /**
     * Indique si il peut recevoir un nouvelle id
     * @return bool
     */
    public boolean canGetNewId(){return false;}

    @Override
    public void setId(String id){ username = id; }

    @Override
    public String getId() { return username; }


    public ArrayList<String> getSearchHistory() {
        return searchHistory;
    }

    public void setSearchHistory(ArrayList<String> searchHistory) {
        this.searchHistory = searchHistory;
    }

    public void addSearchHistory(String search) {
        if (!search.equals("") && !searchHistory.contains(search)) {
            this.searchHistory.add(search);
        }
    }

    public ArrayList<String> getUserInterests() {
        return userInterests;
    }

    public void setUserInterests(ArrayList<String> userInterests) {
        this.userInterests = userInterests;
    }

    public void addUserInterest(String interest){
        if(userInterests==null){
            this.userInterests = new ArrayList<>();
        }
        if(!userInterests.contains(interest)){
            this.userInterests.add(interest);
        }
    }

    public void deleteUserInterest(String name){
        if(userInterests!=null){
            userInterests.remove(name);
        }
    }



    /**
     * Compare deux utilisateurs sur base de leur username
     * @param o Autres utilisateur auquel User est comparé
     * @return true si les utilisateurs ont le même username
     */
    @Override
    public boolean equals(Object o){
        if(!super.equals(o)){
            return false;
        }
        User other = (User) o;
        return this.username.equals(other.getUsername());
    }

    /**
     * Vérifie si le mot passe correspond aux critères de sécurité:
     * contient entre 8 et 16 caractères, parmi lesquelles au moins
     * une majuscule, une minuscule, un chiffre et un symbole
     * @param password est le chaine de caractères (l'input de l'utilisateur)
     * @return true si le chaine de caractères correspond aux critères
     * de sécurité pour representer un mot de passe, false sinon
     */
    public static boolean validatePassword(String password) {
        Pattern passwordRequirements = Pattern.compile("((?=.*\\p{Digit})(?=.*\\p{Lower})(?=.*\\p{Upper})(?=.*\\p{Punct}).{8,16})");
        Matcher matcher = passwordRequirements.matcher(password);
        return matcher.matches();
    }

    /**
     * Utilise la méthode en polymorphisme pour vérifier si le mot
     * de passe respecte les demandes de sécurité et vérifie de plus
     * si le nom d'utilisateur est different du mot de passe.
     * @param password le mot de passe à verifier
     * @param username le nom d'utilisateur à verifier
     * @return true si le mot passe correspond aux critères de sécurité
     * et est diffrenet du nom d'utilisateur, false sinon
     */
    public static boolean validatePassword(String username, String password) {
        return (validatePassword(password) && !password.equals(username));
    }

    /**
     * Vérifie que le chaine de caractères donne en paramètre
     * correspond aux critères pour être considérée adresse d'email.
     * @param email est le chaine de caractères (l'input de l'utilisateur)
     * @return true si l'input respecte la structure d'une adresse
     * email, false sinon
     */
    public static boolean validateEmail(String email) {
        Pattern emailRequirements = Pattern.compile("([^@ ]*@[\\p{Lower}\\p{Upper}\\p{Digit}]+([.][\\p{Lower}\\p{Upper}]+)+)");
        Matcher matcher = emailRequirements.matcher(email);
        return matcher.matches();
    }

    /**
     * Vérifie que l'utilisateur a bien introduit le même mot
     * de passe deux fois.
     * @param password premier input
     * @param confirmPassword deuxième input
     * @return true si les deux coïncident, false sinon§
     */
    public static boolean confirmPassword(String password, String confirmPassword) {
        return (password.equals(confirmPassword) && confirmPassword.length() != 0);
    }

}
