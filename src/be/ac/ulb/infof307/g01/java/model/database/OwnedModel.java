package be.ac.ulb.infof307.g01.java.model.database;

/**
 * OwnedModel est un dérivé de la class model qu'on utilise
 * pour pouvoir associer un utilisateur.
 */
public abstract class OwnedModel extends Model {

    //Identifiant unique de l'utilisateur
    private String userID;

    public void setUserID(String userID){ this.userID = userID; }

    public String getUserID(){ return userID; }

    public boolean canGetNewId(){return true;}

    @Override
    public boolean equals(Object o){
        if(!super.equals(o)){
            return false;
        }
        OwnedModel other = (OwnedModel) o;

        String currentUserId = getUserID();
        String otherUserId = other.getUserID();

        if(currentUserId == null){
            return(otherUserId == null);
        }
        return currentUserId.equals(otherUserId);
    }
}