package be.ac.ulb.infof307.g01.java.model.dao;

import be.ac.ulb.infof307.g01.java.model.Article;
import be.ac.ulb.infof307.g01.java.model.Directory;
import be.ac.ulb.infof307.g01.java.model.Node;
import be.ac.ulb.infof307.g01.java.model.User;
import be.ac.ulb.infof307.g01.java.model.database.Database;

import java.util.ArrayList;
import java.util.List;

import static be.ac.ulb.infof307.g01.java.Constants.ROOT_DIRECTORY_NAME;

public class DirectoryDAO extends OwnedDAO<Directory> {

    /**
     * constructeur
     *
     * @param db   database
     * @param user
     */
    DirectoryDAO(Database db, User user) {
        super(db, user);
    }

    public Directory findByName(String name){
        Directory res =  null;
        List<Directory> dirs = findAll();
        for(Directory dir: dirs){
            if (dir.getName().equals(name)) {
                res = dir;
                break;
            }
        }
        return res;
    }

    @Override
    public Class<Directory> getModelClass() {
        return Directory.class;
    }

    public void deleteDirectory(String id){
        Directory parent;
        Directory toDelete = findById(id);
        List<Directory> allDirs = findAll();

        for (Directory tmp: allDirs){
            if(tmp.getNodes().contains(toDelete.getId())){
                parent = tmp;
                for(Node node: toDelete.nodeList()){
                    parent.addNode(node);
                }
                update(parent);
                break;
            }
        }

        delete(toDelete);
    }

    /**
     * Change le directory d'un node
     * @param parentId id du dossier parent
     * @param node
     */
    public void changeNodePath(String parentId, Node node){
        // Delete dans l'ancien directory
        for (Directory tmpDir : findAll()) {
            if(tmpDir.getNodes().contains(node.getId())) {
                tmpDir.deteleNode(node);
                update(tmpDir);
                break;
            }
        }

        Directory newDir = findById(parentId);
        changeNodePath(newDir, node);
    }

    /**
     * Change le directory d'un node
     * @param newDir dossier parent
     * @param node
     */
    public void changeNodePath(Directory newDir, Node node){
        // Delete dans l'ancien directory
        for (Directory tmpDir : findAll()) {
            if(tmpDir.getNodes().contains(node.getId())) {
                tmpDir.deteleNode(node);
                update(tmpDir);
                break;
            }
        }

        newDir.addNode(node);
        update(newDir);
    }

    public void createIfNotExists(Directory newDir){
        List<Directory> allDirs = findAll();
        boolean create = true;
        for (Directory dir: allDirs){
            if (dir.getName().equals(newDir.getName())) {
                create =false;
                break;
            }
        }
        if(create) create(newDir);
    }

    public void addChildToRoot(Article article) {
        Directory rootDir = findByName(ROOT_DIRECTORY_NAME);
        changeNodePath(rootDir.getId(), article);
    }


}
