package be.ac.ulb.infof307.g01.java.model;

import be.ac.ulb.infof307.g01.java.model.dao.DAOFactory;
import be.ac.ulb.infof307.g01.java.model.database.OwnedModel;
import io.jsondb.annotation.Document;
import io.jsondb.annotation.Id;

import java.util.ArrayList;
import java.util.List;

/**
 * Cette classe représente une recherche avançée faite par l'utilisateur
 * Celle-ci peut être sauvegarer pour l'utilisateur et réutiliser par la
 * suite.
 * Pour chaque recherche on stock si la recherche se fait sur le titre et/ou le contenu,
 * le nom de l'auteur, de la source, le lieu, la date de référence pour cette recherche
 * ainsi que le rapport entre les articles et la date de référence (avant, le même jour ou après)
 * Cette classe contient les informations à rechercher. Tandis que oldSearch effectue la recherche dans
 * les articles à partir des informations de cette classe.
 */

@Document(collection="Search", schemaVersion="1.0")
public class Search extends OwnedModel {
    public static final String BEFORE = "Before";
    public static final String AFTER = "After";
    public static final String ON_DATE = "On Date";

    private String title;
    private String content;
    private String author;
    private String date;
    private String dateComparison; //valeur possible: -1 (On garde les articles sorties avant la date de référence),
                                    // 0(On garde les articles sorties le même jour que la date de référence),
                                    // 1(On garde les articles sorties après la date de référence)
    private String location;
    private String source;
    private String language;
    private List<String> hashtags = new ArrayList<>();
    private String tag;

    private boolean isAdvanced; //Attribut spécifiant si il s'agit de la recherche simple ou avançée

    @Id //Clé pour la base de donnée
    private String id;

    public Search() {}

    /**
     * Constructeur qui initialise les champs principaux à la même valeur
     * (utiliser lors de la recherche simple)
     * @param simpleSearch
     */
    public Search(String simpleSearch){
        this.author = simpleSearch;
        this.content = simpleSearch;
        this.title = simpleSearch;
        this.location = simpleSearch;
        this.source = simpleSearch;
        this.tag = simpleSearch;
    }

    /**
     * Constructeur plus avancée
     * @param title String à rechercher dans le titre
     * @param content String à rechercher dans le contenu
     * @param author Nom de l'auteur
     * @param date La date de référence
     * @param dateComparison Le lien entre la date de référence et celle des articles recherchés
     * @param location Le lieu de l'article
     * @param source La source de l'article
     * @param language La langue de publication
     * @param hashtags Les mots-clés de l'article
     * @param tag Tag à rechercher
     */
    public Search(String title, String content, String author, String date,
                  String dateComparison, String location, String source, String language, List<String> hashtags, String tag) {
        this.title = title;
        this.content = content;
        this.author = author;
        this.date = date;
        this.dateComparison = dateComparison;
        this.location = location;
        this.source = source;
        this.language = language;
        this.hashtags = hashtags;
        this.tag = tag;
    }

    public Search(Search otherSearch) {
        title = otherSearch.getTitle();
        content = otherSearch.getContent();
        author = otherSearch.getAuthor();
        date = otherSearch.getDate();
        dateComparison = otherSearch.getDateComparison();
        location = otherSearch.getLocation();
        source = otherSearch.getSource();
        language = otherSearch.getLanguage();
        hashtags = otherSearch.getHashtags();
        tag = otherSearch.getTag();
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) { this.author = author; }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getLanguage() { return language; }

    public void setLanguage(String language) { this.language = language; }

    public String getDateComparison() {
        return dateComparison;
    }

    public void setDateComparison(String dateComparison) {
        this.dateComparison = dateComparison;
    }

    public List<String> getHashtags() {
        return hashtags;
    }

    public void setHashtags(List<String> hashtags){
        this.hashtags = hashtags;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

    public String getId(){ return id;}

    public void setId(String newId){id = newId;}

    /**
     * @param container Le contenant
     * @param contained le contenu
     * @return true si le contenu est contenu dans le contenant ou si le conteneur est null. false sinon.
     */
    private boolean doesContain(String container, String contained){
        return contained == null || container.toLowerCase().contains(contained.toLowerCase());
    }

    /**
     * Permet de réaliser une recherche simple.
     * Celle-ci ne cherrche que parmi le titre de l'article, son auteur, son contenu (résumé),
     * sa source et sa localisation
     * @return Une liste d'article
     */
    public ArrayList<Article> search(){
        ArrayList<Article> resultSearch = new ArrayList<>();
        List<Article> allArticles = DAOFactory.getArticleDAO().findAll();
        for (Article article: allArticles) {
            //Pour chaque attributs, on compare le texte en miniscule (cf toLowerCase() ) afin de permettre
            // à la recherche d'être insensible à la casse.
            boolean title = doesContain(article.getName(), this.title);
            boolean author = doesContain(article.getAuthor(), this.author);
            boolean content = doesContain(article.getContent(), this.content);
            boolean source = doesContain(article.getSource(), this.source);
            boolean location = doesContain(article.getLocation(), this.location);
            boolean tag = this.tag == null || article.getTags().contains(new Tag(this.tag));
            boolean valid = (title || author || content || source || location || tag);
            // Si la recherche est dans le mode avançée, alors on veut les articles dont tous les attributes correspondent
            //(Et non pas seulement un comme pour la recherche simple)
            if (isAdvanced) valid = (title && author && content && source && location && tag);
            if (valid)resultSearch.add(article);
        }
        isAdvanced = false;
        return resultSearch;
    }

    /**
     * Recherche plus spécifique.
     * Dans cette fonction on recherche les articles dont tout les attributs correspondent à la recherche
     * (un paramètre de recherche vide signifient que ce paramètre ne rentre pas en compte)
     * @return une liste d'article
     */
    public ArrayList<Article> advancedSearch() {
        isAdvanced = true;
        ArrayList<Article> resultSearch = new ArrayList<>();
        List<Article> allArticles = this.search(); // On réalise une première recherche simple
        for (Article article: allArticles) {
            //Puis on applique les filtres plus particulier à la recherche avançée
            if ((date == null || compareDate(article.getDate())))
                resultSearch.add(article);
        }
        return resultSearch;
    }

    /**
     * Compare la date de l'article (sous format YYYY-MM-DD HH-MM-SS)
     * avec celle du Search
     * @param date Date de l'article (sous format YYYY-MM-DD HH-MM-SS)
     * @return True si les dates correspondent
     */
    private boolean compareDate(String date){
        String[] toCompare = date.split(" ");
        Integer res = toCompare[0].compareTo(this.date);
        return getDateComparator(res).equals(dateComparison);
    }

    private String getDateComparator(Integer res) {
        switch (res) {
            case -1:
                return BEFORE;
            case 1:
                return AFTER;
            default:
                return ON_DATE;
        }
    }

    /**
     * @param dir folder dans lequel on cherche
     * @return les articles présents dans le folder
     */
    public ArrayList<Article> searchInDir(Directory dir) {
        ArrayList<Article> resultSearch = new ArrayList<>();
        for(Node node: dir.nodeList()){
            if (node.nodeIsFolder()){
                resultSearch.addAll(searchInDir((Directory) node));
            }
            else{
                resultSearch.add((Article) node);
            }
        }
        return resultSearch;
    }

}
