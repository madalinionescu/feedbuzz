package be.ac.ulb.infof307.g01.java.model.dao;
import be.ac.ulb.infof307.g01.java.model.Profile;
import be.ac.ulb.infof307.g01.java.model.User;
import be.ac.ulb.infof307.g01.java.model.database.Database;

public class ProfileDAO extends OwnedDAO<Profile>{


    /**
     * constructeur
     *
     * @param db   database
     * @param user
     */
    ProfileDAO(Database db, User user) {
        super(db, user);
    }

    @Override
    public Class<Profile> getModelClass() {
        return Profile.class;
    }


}
