package be.ac.ulb.infof307.g01.java.model.dao;

import be.ac.ulb.infof307.g01.java.exception.TagExistsException;
import be.ac.ulb.infof307.g01.java.model.*;
import be.ac.ulb.infof307.g01.java.model.database.Database;

import java.util.ArrayList;
import java.util.List;

import static be.ac.ulb.infof307.g01.java.Constants.*;

public class ArticleDAO extends OwnedDAO<Article> {

    /**
     * constructeur du DAO
     *
     * @param db database
     */
    ArticleDAO(Database db, User user) {
        super(db,user);
    }

    /**
     * Creer ou mets à jour un article avec reference sont link qui doit être unique
     * @param article nouvelle article
     */
    public void createOrUpdate(Article article){
        List<Article> articles = findAll();
        List<Article> matchArticle = new ArrayList<>();
        for (Article p : articles) {
            if (p.getLink().equals(article.getLink())) {
                matchArticle.add(p);
            }
        }
        if(matchArticle.size() == 0) {
            create(article);
            DAOFactory.getDirectoryDAO().addChildToRoot(article);
        }
        else{
            update(article);
        }
    }

    /**
     * récupère tous les articles de la database qui ne sont pas dans la corbeille
     * @return tous les articles de l'utilisateur de la database.
     */
    @Override
    public List<Article> findAll(){
        return findAll(false);
    }

    /**
     * récupère tous les articles de la database qui se trouve
     * dans la corbeille si inTrash = true et qui ne sont pas dans
     * la corbeille si inTrash = false;
     * @param inTrash prendre les articles dans la corbeille
     * @return tous les articles de l'utilisateur de la database.
     */
    public List<Article> findAll(Boolean inTrash){
        List<Article> articles = super.findAll();

        int i = 0;
        while (i<articles.size()){
            Article item = articles.get(i);
            if(item.getTrash() != inTrash)
                articles.remove(i);
            else i++;
        }

        return articles;
    }

    @Override
    public Class<Article> getModelClass() {
        return Article.class;
    }


    /**
     * La fonction va delete tous les articles venant d'un feed
     * feedLink ou du Twitter (tweets liées à un hashtag)qui
     * sont contenus dans la database
     * @param feedLink link rss des articles qu'on doit delete
     */
    public static void deleteArticlesFromFeed(String feedLink){
        List<Article> list = DAOFactory.getArticleDAO().findAll();
        for (Article article : list){
            if ( ((!article.isTweet()) && article.getRssLink().equals(feedLink)) ||
                    ( article.isTweet() && article.getTwitterHashtag().equals(feedLink)) ) {
                DAOFactory.getArticleDAO().delete(article);
            }
        }
    }

    /**
     * @param tagName nom du tag
     * @param article article auquel on supprime le tagName
     * @throws TagExistsException si le tag est déjà ajouté
     */
    void addTagToArticle(String tagName, Article article) throws TagExistsException{
        List<Tag> tmpTags = article.getTags();
        Tag tmpTag = new Tag(tagName);
        if (tmpTags.contains(tmpTag)){
            throw new TagExistsException();
        }
        else{
            tmpTags.add(tmpTag);
            article.setTags(tmpTags);
        }
        update(article);
    }

    /**
     * @param tagName nom du tag
     * @param article article auquel on supprime le tagName
     */
    void deleteTagFromArticle(String tagName, Article article){
        List<Tag> tmpTags = article.getTags();
        Tag tmpTag = new Tag(tagName);
        tmpTags.remove(tmpTag);
        article.setTags(tmpTags);
        update(article);
    }

    /**
     * @param tagName nom du tag
     * @param article article à vérifier
     * @return true si tagName dans article
     */
    public boolean hasTagInArticle(String tagName, Article article){
        List<Tag> tags = article.getTags();
        return tags.contains(new Tag(tagName));
    }


    /**
     * @param article Article qu'on marque comme 'En cours/Reading'
     */
    public void tagAsReading(Article article){
        if (hasTagInArticle(TAG_NAME_FINISHED, article))
            return;
        deleteTagFromArticle(TAG_NAME_NOT_READ, article);
        try {
            addTagToArticle(TAG_NAME_READING, article);
        } catch (TagExistsException e) {
            // Tag déjà là, n'arrivera jamais
        }
        update(article);
    }

    /**
     * @param article Article qu'on marque comme 'Lu/Finished'
     */
    public void tagAsFinished(Article article){
        deleteTagFromArticle(TAG_NAME_NOT_READ, article);
        deleteTagFromArticle(TAG_NAME_READING, article);
        try {
            addTagToArticle(TAG_NAME_FINISHED, article);
        } catch (TagExistsException e) {
            // Tag déjà là, n'arrivera jamais
        }
        update(article);
    }

    /**
     * Ajoute le tag "recommended" à l'article
     * @param article article
     */
    public void tagAsRecommended(Article article){
        try {
            addTagToArticle(TAG_NAME_RECOMMENDED, article);
        }
        catch (TagExistsException e) {
            // n'arrivera surement jamais

        }
        update(article);
    }

    /**
     * @param article Article dont on veut avoir les tags en Strings
     * @return Liste de Strings (les noms des tags) plutôt que l'objet Tag
     */
    public List<String> getTagsAsStrings(Article article){
        List<String> res = new ArrayList<>();
        for (Tag tag : article.getTags()){
            res.add(tag.getName());
        }
        return res;
    }

    public Article findByName(String articleName) {
        Article res =  new Article();
        List<Article> articles = findAll();
        for(Article art: articles){
            if (art.getName().equals(articleName)) {
                res = art;
                break;
            }
        }
        return res;
    }
}
