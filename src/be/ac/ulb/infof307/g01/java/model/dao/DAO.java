package be.ac.ulb.infof307.g01.java.model.dao;

import be.ac.ulb.infof307.g01.java.model.database.Database;
import be.ac.ulb.infof307.g01.java.model.database.Model;

/**
 * Direct Access Object Pattern
 * https://www.tutorialspoint.com/design_pattern/data_access_object_pattern.htm
 *
 * A utiliser avec le design pattern Factory
 */
public abstract class DAO< T extends Model> {

    protected final Database db;

    /**
     * constructeur du DAO
     * @param db database
     */
    DAO(Database db){
        this.db = db;
    }

    /**
     * crée l'object dans la database
     * @param model le model
     */
    public abstract boolean create(T model);

    /**
     * suprime l'object de la database
     * @param model le model
     */
    public void delete(T model) {
        db.delete(model);
    }

    /**
     * met à jour le model dans la database
     * @param model le model
     */
    public void update(T model){
        db.update(model);
    }

    /**
     * récupère l'object de la database
     * @param id l'id du model
     */
    public abstract T findById(String id);

    /**
     * récupère la class du models
     */
    protected abstract Class<T> getModelClass();

}
