package be.ac.ulb.infof307.g01.java.model.dao;

import be.ac.ulb.infof307.g01.java.model.InformationTwitter;
import be.ac.ulb.infof307.g01.java.model.User;
import be.ac.ulb.infof307.g01.java.model.database.Database;

import static be.ac.ulb.infof307.g01.java.Constants.ACCOUNT;
import static be.ac.ulb.infof307.g01.java.Constants.HASHTAG;

import java.util.ArrayList;
import java.util.List;

public class InformationTwitterDAO extends OwnedDAO<InformationTwitter> {
    /**
     * constructeur
     *
     * @param db   database
     * @param user
     */
    InformationTwitterDAO(Database db, User user) {
        super(db, user);
    }

    public void createIfNotExist(InformationTwitter informationTwitter) {
        List<InformationTwitter> informationsTwitter = findAll();
        List<InformationTwitter> matchInfoTwitter = new ArrayList<>();
        for (InformationTwitter p : informationsTwitter) {
            if (p.getName().equals(informationTwitter.getName())) {
                matchInfoTwitter.add(p);
            }
        }
        if (matchInfoTwitter.size() == 0) {
            create(informationTwitter);
        }
    }

    public List<InformationTwitter> getHashtags() {
        List<InformationTwitter> informationsTwitter = findAll();
        List<InformationTwitter> hashtags = new ArrayList<>();
        for (InformationTwitter hashtag : informationsTwitter) {
            if (isHashtag(hashtag)) {
                hashtags.add(hashtag);
            }
        }
        return hashtags;
    }

    public List<InformationTwitter> getAccounts() {
        List<InformationTwitter> informationsTwitter = findAll();
        List<InformationTwitter> accounts = new ArrayList<>();
        for (InformationTwitter account : informationsTwitter) {
            if (isAccount(account)) {
                accounts.add(account);
            }
        }
        return accounts;
    }

    public void deleteByName(String name, String type){
        List<InformationTwitter> allInformation = findAll();
        for (InformationTwitter infoTwit: allInformation){

            if (infoTwit.getName().equals(name) && infoTwit.getType().equals(type)){
                delete(infoTwit);
            }
        }


    }

    public boolean isAccount(InformationTwitter infoTwit){
        return infoTwit.getType().equals(ACCOUNT);
    }

    public boolean isHashtag(InformationTwitter infoTwit){
        return infoTwit.getType().equals(HASHTAG);
    }

    @Override
    public Class<InformationTwitter> getModelClass() {
        return InformationTwitter.class;
    }

}
