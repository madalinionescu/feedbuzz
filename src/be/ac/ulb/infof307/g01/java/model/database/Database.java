package be.ac.ulb.infof307.g01.java.model.database;


import io.jsondb.JsonDBTemplate;

import java.util.ArrayList;
import java.util.List;


public class Database {
    final static String EMPTY_KEY = "";
    private final JsonDBTemplate jsonDBTemplate;

    public Database(String path) {
        jsonDBTemplate = new JsonDBTemplate(path, "be.ac.ulb.infof307.g01.java.model");
    }

    /**
     * Crée un model dans la database
     *
     * @param model main.java.model à ajouter
     * @return true si le model a bien été ajouté de la database
     */
    public boolean create(Model model) {
        try {
            if (!jsonDBTemplate.collectionExists(model.getClass())) {
                jsonDBTemplate.createCollection(model.getClass());
            }

            if (model.canGetNewId()) {
                generateUniqueId(model);
            } else {
                String key = model.getId();
                if (key.equals(EMPTY_KEY) || key == null || findById(key, model.getClass()) != null) {
                    return false;
                }
            }

            jsonDBTemplate.insert(model);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Mets à jour le model de la database
     *
     * @param model
     * @return true si le model a bien été mis à jour dans la database
     */
    public void update(Model model) {
        try {
            String key = model.getId();
            jsonDBTemplate.save(model, model.getClass());
            if (!key.equals(EMPTY_KEY) && jsonDBTemplate.collectionExists(model.getClass())) {
                jsonDBTemplate.save(model, model.getClass());
            }
        } catch (Exception e) {
            //Pas de fenêtre erreur car static est pas possible avec les tests.
        }
    }

    /**
     * Supprime le model de la database
     *
     * @param model
     * @return true si le model a bien été supprimé de la database false sinon
     */
    public void delete(Model model) {
        try {
            String key = model.getId();
            if (!key.equals(EMPTY_KEY) && jsonDBTemplate.collectionExists(model.getClass())) {
                jsonDBTemplate.remove(model, model.getClass());
            }
        } catch (Exception e) {
            //Pas de fenêtre erreur car static est pas possible avec les tests.
        }
    }

    /**
     * cherche un model par rapport à son id et collection
     *
     * @param uniqueId        unique Id
     * @param collectionClass class
     * @return true si le model a été trouvé
     */
    public <T extends Model> T findById(String uniqueId, Class<T> collectionClass) {
        if (!jsonDBTemplate.collectionExists(collectionClass)) {
            return null;
        }
        return jsonDBTemplate.findById(uniqueId, collectionClass);
    }

    /**
     * Récupère tous les models d'une collection
     *
     * @param collectionClass model class
     * @return true si l'utilisateur a bien été supprimé de la database false sinon
     */
    public <T extends Model> List<T> findAll(Class<T> collectionClass) {
        if (!jsonDBTemplate.collectionExists(collectionClass)) {
            return new ArrayList<>();
        }
        return jsonDBTemplate.findAll(collectionClass);
    }

    /**
     * recherche si le model existe
     *
     * @param model model
     * @return true si le model a bien été trouvé dans la database
     */
    public <T extends Model> boolean isFound(Model model) {
        if (!jsonDBTemplate.collectionExists(model.getClass())) {
            return false;
        }
        return jsonDBTemplate.findById(model.getId(), model.getClass()) != null;
    }

    /**
     * réinitialise toutes une collection
     *
     * @param collectionClass model class
     */
    public <T extends Model> void clear(Class<T> collectionClass) {
        if (!jsonDBTemplate.collectionExists(collectionClass)) {
            return;
        }
        jsonDBTemplate.dropCollection(collectionClass);
    }

    /**
     * réinitialise toutes les collections
     */
    public void clearAll() {
        for (String collection : jsonDBTemplate.getCollectionNames()) {
            jsonDBTemplate.dropCollection(collection);
        }
    }

    /**
     * Génère un Id non utilisé dans la database
     *
     * @param model model
     */
    public void generateUniqueId(Model model) {
        String id;
        do {
            id = "" + System.nanoTime();
        }
        while (jsonDBTemplate.findById(id, model.getClass()) != null);
        model.setId(id);
    }
}
