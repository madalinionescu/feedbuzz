package be.ac.ulb.infof307.g01.java.model;

import be.ac.ulb.infof307.g01.java.model.database.OwnedModel;
import io.jsondb.annotation.Document;
import io.jsondb.annotation.Id;

import java.util.List;

@Document(collection="Profile", schemaVersion="1.0")
public class Profile extends OwnedModel {
    @Id
    private String id;

    private List<String> interests;

    public List<String> getInterests() {
        return interests;
    }

    public void setInterests(List<String> interests) {
        this.interests = interests;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }
}
