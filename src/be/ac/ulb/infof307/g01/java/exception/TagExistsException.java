package be.ac.ulb.infof307.g01.java.exception;

public class TagExistsException extends Exception implements DisplayableException {
    protected final String title;

    public TagExistsException() {
        super("Tag is already added");
        this.title = "Tag is already added";
    }

    @Override
    public String getTitle() {
        return title;
    }
}
