package be.ac.ulb.infof307.g01.java.exception;

public class UserNotFoundException extends Exception implements DisplayableException {
    protected final String title;

    public UserNotFoundException() {
        super("Please check the username/ password");
        this.title = "User not found";
    }

    @Override
    public String getTitle() {
        return title;
    }
}
