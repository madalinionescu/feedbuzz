package be.ac.ulb.infof307.g01.java.exception;

public class InvalidOperationException extends  Exception implements  DisplayableException{

    protected final String title;

    public InvalidOperationException() {
        super("An error occured during the operation");
        this.title = "Operation failed";
    }

    @Override
    public String getTitle() {
        return title;
    }
}
