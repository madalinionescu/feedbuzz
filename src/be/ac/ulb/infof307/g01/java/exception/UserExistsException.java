package be.ac.ulb.infof307.g01.java.exception;

public class UserExistsException extends Exception implements DisplayableException {
    protected final String title;

    public UserExistsException() {
        super("Username already registered");
        this.title = "User already registered";
    }

    @Override
    public String getTitle() {
        return title;
    }
}
