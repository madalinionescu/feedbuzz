package be.ac.ulb.infof307.g01.java.exception;

public interface DisplayableException {
    String getTitle();
    String getMessage();
}
