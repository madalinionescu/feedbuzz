# FeedBuzz : Projet de génie logiciel et gestion de projet (INFO-F-307)

L'application est un agrégateur de flux RSS qui offre à l'utilisateur la possibilité de visionner ses articles 
au sein de l'application ou bien dans une webview, celui-ci est aussi capable de gérer ses articles en les assignant à des dossiers
ou en y associant des tags qu'il aura lui-même créé. En plus de pouvoir gérer ses propres flux RSS l'utilisateur est aussi
capable de suivre des comptes/hashtags twitter qui peuvent être géré de la même manière que les articles, tout cela associé à
un système de recherche avancé permettant à l'utilisateur de facilement accéder aux articles qui l'intéresse.

Pour plus d'informations sur comment utiliser l'application naviguer vers la section "Help" de celle-ci


# Utilisation

Ces instructions vous permettrons d'obtenir une copie du projet executable sur votre machine local
à des fins de develepoment ou de testing. 

## Prérequis

* Intellij IDEA Ultimate - 2018.2
* Java JDK 1.8

## Compilation
Le projet est builder via Maven

1.  Ouvrez le projet dans Intellij via le fichier *FeedBuzz.iml*
2.  Naviguer vers la Maven view
3.  Builder le projet avec Maven goals via la commande `package`

Ou bien
1. Naviguer vers la racine du projet
2. Dans un terminal éxecuter la commande `mvn package`

## Démarrage 

Exécuter le fichier *FeedBuzz-4.0-jar-with-dependencies.jar*

# Tests

Les fichiers se trouvent dans le dossier test. Un clic droit sur le dossier, dans IntelliJ, permet de tous les lancer.

Les tests sont appliqué sur différente partie du projet

**1. Modèles**

    Test que ceux-ci sont corrèctement créé/détruit et que leurs méthodes remplisse bien leur rôle 

**2. DAO**

    Test que celui-ci remplis bien sa fonction d'écriture/lecture/suppression de données dans la BDD 

**3. Utils**

    Test toutes les méthodes de nos classes utils 


# Développement
## Librairies utilisé
Toutes les libraires sont gérer grâce a [Maven](https://maven.apache.org/)
*  jsonDB
*  twitter4j
*  jsoup
*  springframework.security

## Structure du projet
### MVC

**Modèle**

**Contrôleur principal (MainController):** il est responsable de la communication entre les différents modèles. 
Son interface (MainListener) agrège les deux interfaces nécessaires pour la fenêtre de démarrage et la fenêtre du tableau de 
bord pour communiquer avec les modèles. Toutes les communication mentionnées (c.f. tous les appels à n’importe quel objet DAO)
devrait uniquement utiliser le contrôleur principal. 
    
**Contrôleur de vue (ViewController):** ils ont la responsabilité de mettre à jour les différentes fenêtres. 
Ces mises à jour contiennent par exemple l’affiche de l’aperçu des articles, le tri des articles basé sur les critères
de l’utilisateur, etc. (Cette liste est loin d’être exhaustive). Une autre responsabilité du contrôleur de vue est d’ouvrir
les différentes sous-vue nécessaires, mais l’accès au modèle doit exclusivement être fait grâce à l’interface du contrôleur 
principal, en utilisant le listener fourni. Certains contrôleurs de vue peuvent fournir leur propre interface, qui sont
utilisée pour partiellement mettre à jour une vue qu’ils contrôlent, grâce au contrôleur de sous-vue.
    
**Contrôleur de sous-vue:** une sorte spéciale de contrôleur de vue, qui à part mettre à jour une fenêtre par laquelle
ils ont été instanciés, peuvent mettre à jour une partie d’une autre fenêtre. (Ex. ajouter un flux RSS dans la fenêtre
d’administration des flux RSS va également modifier la fenêtre du tableau de bord pour y afficher le fait que la liste 
d’article est entrain d’être mise à jour). Le listener fourni est utilisé pour donner accès à l’interface d’un contrôleur 
de vue spécifique, s’ils ont besoin de mettre à jour, mise à part les fenêtres, les modèles ou simplement pour y accéder, 
vu que toutes les communications avec les modèles devraient passé par le contrôleur principal.

Selon l’importance des exceptions rencontrée, elles pourraient être catch dans le contrôleur principal, ou dans l’un des
contrôleurs de vue. En général, les erreurs critiques (comme celle envoyé lors de l’absence d’un fichier FXML requis lors 
de la connexion, une absence qui empêche l’utilisateur de continuer à utiliser l’application) devraient être catch par le contrôleur 
le plus haut dans l’arborescence (MainController) et en fermant celles-ci l’utilisateur accepte de fermer/redémarrer l’application, 
sachant que continuer à l’utiliser dans son état est impossible. Les exceptions moins sévères (qui par exemple peuvent être 
lié à l’impossibilité d’ouvrir la fenêtre d’aide ou tout autre fonctionnalité moins critique) peuvent aussi être catch dans 
un contrôleur de vue, et dans ce cas l’user n’est pas forcé à rouvrir l’application, sachant qu’elle est toujours partiellement 
utilisable. Dernièrement, les exceptions les moins importantes, comme celle envoyé par l’impossibilité de correctement analyser
une date lors du tri des articles, sont traitées dans le contrôleur de vue exclusivement et un message d’alerte (au lieu d’une erreur) 
sera affiché, vu que c’est erreur peuvent être maitriser (dans le cas des erreurs d’analyse de date, les articles avec une 
date non déterminée sont simplement ajouter à la fin de la liste, en d’autre terme ils sont considérer comme les plus vieux 
articles disponible)

### DAO

Nous utilisons JsonDB comme moyen pour stocker les données.
Tous les objets nécessitent un attribut `@Document` qui donne les informations
à JsonDB sur le nom de la collection auxquelles l’objet appartient et un attribut
`@Id` sur la variable qui sera utiliser comme identifiant unique.
L’avantage : Très facile d’ajouter et mettre à jour des éléments dans la
database sans être obliger de définir ou redéfinir des requêtes comme sql.
Désavantage : Moins performant ?

![Screenshot UML Modèle](https://i.gyazo.com/f98052876e02b5f4991bef4374cbed62.png)

Les modèles sont des données qu’on va stocker dans la database. Etant donné
que chaque modèles de jsonDB doit avoir son propre attribut `@Id`, on a défini
sur la class abstrait « Model » les méthodes abstrait getId() et setId() pour
récupérer l’information.
Il y a aussi l’avantage de pouvoir personnalisé notre `@Id` sur chaque model
(« User » a comme unique id l’username).

La méthode abstrait « canGetNewId() » permet d’indiquer à la database si en
l’absence d’un id, elle a le droit de lui générer un nouvelle id. (Pour les objets
comme les articles, c’est la database qui génère l’id. La class « User » possède
sont id (username) directement à la création donc sa méthode renverra un
false).
Nous possédons 2 types de modèles. « OwnedModel » qui est destiné à
appartenir à un utilisateur, elle encapsule toutes les données et méthodes
nécessaire pour pouvoir déterminer quel objet appartient à quelle utilisateur et
« Model » qui permet de stocker toutes les autres données.

![Screenshot UML DAO](https://i.gyazo.com/7cd868346a3fafe01212e8cfe1edd77f.png)

**Pourquoi le DAO (Data Access Object)**

C’est un design pattern qui permet de faire une abstraction sur l’accès aux
données de la database. Il est beaucoup plus facile de changer de database
avec un DAO
Ce pattern est également très adapté au MVC et permet aussi de forcer à avoir
une structure répétitive quand on ajouter des nouveaux modèles.
Chaque model doit implémenter son propre DAO, ils possèderont ainsi tous
toutes les méthodes de base à savoir le create, delete, update et find. On peut
également ajouter des méthodes personnalisées.
Comme pour les 2 types de modèles qui peuvent être implémenté
« OwnedModel » et « Model », nous avons aussi 2 types de DAO qui peuvent 
TEAM INFORMATION
être implémenté « OwnedDAO » et « DAO ». La logique reste la même, si le
model appartient à un utilisateur, on utilisera OwnedDAO sinon DAO.
L’existence de « OwnedModel » et « OwnedDAO » est justifier uniquement par
le fait de limiter le code répétitif concernant l’utilisateur grâce à un abstraction
supplémentaire.

**Pourquoi le DAOFactory**

Le DAOFactory est une usine qui permet de créer et donner accès à tous les
DAO de la database, à partir d’un endroit unique. Le DAOFactory est en
communication directe avec LocalUser pour tous ce qui concerne donnée
utilisateur.

**Guide d’utilisation**

Pour créer model dans la database, on va passer par DAOFactory
```
DAOFactory.get{DAO}.create(Model)
DAOFactory.getArticleDAO().create(Article)
```

Pour mettre à jour un model
```
DAOFactory.get{DAO}.update(Model)
DAOFactory.getArticleDAO().update(Article)
```


Pour supprimer un model
```
DAOFactory.get{DAO}.delete(Model)
DAOFactory.getArticleDAO().delete(Article)
```

Pour récupérer les models
```
DAOFactory.get{DAO}.findAll()
DAOFactory.getArticleDAO().findAll()
```

Attention : Pour tous les OwnedModels, il est important d’avoir appelé utiliser
`LocalUser.singleton.connect()` avant de manipuler la database.

**Comment ajouter un nouveau modèle à la database**
1. Déterminer s’il faut utiliser un « OwnedModel » ou un « modèle » et
extends dans le nouveau models
2. Déclarer et implémenter l’attribut `@Document` dans l’entête de la classe
pour JsonDB
3. Déclarer une variable avec l’attribut `@Id`. Pour les objets qui
appartiennent à « OwnedModel » on doit juste déclarer une variable id
qui ne sera manipulé que par la database. A par pour le setter et le
getter vous ne devrez donc jamais les modifier manuellement !
4. Maintenant que le model est créé, il faut créer une class DAO
« OwnedDAO » ou « DAO » pour pouvoir le manipuler dans la database.
Aucune méthode n’est à ajouter, le model est déjà utilisable mais vous
pouvez ajouter des méthodes personnalisées.
5. Ajouter l’entrée du DAO dans la DAOFactory.
Attention : Il faut le plus possible éviter de mettre des méthodes dans les
modèles parce que déjà ce n’est pas la bonne place et ensuite JsonDB peut
provoquer des erreurs.

## Design pattern

Dans cette partie vous trouverez les design pattern que nous avons mis en place
lors du projet ainsi qu'une brève explication sur leur utilité dans notre cas.

### Builder

Certains modèles possèdent de nombreux attributs, comme par exemple les 
recherches avancées ou les articles. Or, tous les attributs ne sont pas toujours
utiles ou importants à préciser selon le contexte. Et faire un constructeur 
pour chacun de ces modèles pour chaque combinaison d'attributs est fastidieux. 
C'est pourquoi nous avons choisi d'utiliser le design pattern du `Builder`. 
Ce dernier nous permet de créer différentes représentation d'un même modèle
(cfr. [Wikipédia](https://fr.wikipedia.org/wiki/Monteur_(patron_de_conception))).

![Screenshot UML BUILDER](https://en.proft.me/media/java/dp_builder.png)

(Image d'un diagramme uml du design pattern **Builder**,
https://en.proft.me/media/java/dp_builder.png)


### Composite

Afin de représenter une structure arborescente comme celle de dossiers, nous 
avons fais le choix d'implémenter le design pattern du `Composite`. Ce dernier
se base sur le principe de tout-partie: c'est-à-dire que l'on travaille sur des
éléments qui peuvent être une partie/ une feuille/ un noeud finale et d'autres 
un tout (composer de ces parties).
(cfr. [Wikipédia](https://fr.wikipedia.org/wiki/Objet_composite)).

![Screenshot UML COMPOSITE](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Composite_UML_class_diagram_%28fixed%29.svg/600px-Composite_UML_class_diagram_%28fixed%29.svg.png)

(Image d'un diagramme uml du design pattern **Composite**,
https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Composite_UML_class_diagram_%28fixed%29.svg/600px-Composite_UML_class_diagram_%28fixed%29.svg.png)


## Screenshots
![Screenshot Sign In](https://i.gyazo.com/4ed3e194b44c037ae89d8dd5c15c6d93.png)

![Screenshot Sign Up](https://i.gyazo.com/9a27a501d26ae35585a7903637d699ce.png)

![Screenshot Dashboard](https://i.gyazo.com/73c50b063bc8a3a90d0a0e4790339a29.png)

![Screenshot Interests](https://i.gyazo.com/55bf857888db128d2a26124e4ef0f4b9.png)

![Screenshot RSS Manager](https://i.gyazo.com/358d6188d6d6efa85e2fdeef1affc910.png)

![Screenshot Twitter Manager](https://i.gyazo.com/abfd9188bfa3a389a98361cadcba2930.png)

![Screenshot Advanced Search](https://i.gyazo.com/15f4b3086f4b0ba56ef7716d6a122f46.png)

![Screenshot Article Tag Manager](https://i.gyazo.com/bdae9721736f262cfc527a2c2af29df8.png)

![Screenshot User Tag Manager](https://i.gyazo.com/8c91bb16340e8288582662417252b965.png)

![Screenshot Profile](https://i.gyazo.com/c27c5b50e3096b01a80f3ca8e23eeea3.png)

![Screenshot Help](https://i.gyazo.com/8b22c0ca3b3b052ef298bffabbe14298.png)


## Bugs
1. Certaines fenêtres sont trop petites ce qui cause des problèmes d'affichage.
2. Lors de la suppression d'un article, celui-ci persiste dans la structure arborescente ce qui cause des bugs si l'on clique dessus

## Auteurs
* **Ricardo Gomes Rodrigues** - [RicGR98](https://gitlab.com/RicGR98)
* **Alexandre Heneffe** - [AlexandreHnf](https://gitlab.com/AlexandreHnf)
* **Madalin Ionescu** - [madalinionescu](https://gitlab.com/madalinionescu)
* **Nicolas Jonas** - [nijonas](https://gitlab.com/nijonas)
* **Simao Morais Da Cunha** - [smoraisd](https://gitlab.com/smoraisd)
* **Alexis Naud** - [alexisnaud](https://gitlab.com/alexisnaud)
* **Thomas Rouaux** - [trouaux](https://gitlab.com/trouaux)
* **Adriana Sirbu** - [sirbuadriana](https://gitlab.com/sirbuadriana)
